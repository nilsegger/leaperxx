CREATE TABLE password_reset_verification(
    uuid UUID PRIMARY KEY,
    confirmation_code TEXT NOT NULL,
    confirmation_expires TIMESTAMP NOT NULL,
    foreign key (uuid) references logins(uuid) ON DELETE CASCADE
);