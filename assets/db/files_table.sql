CREATE TABLE files(
    uuid UUID PRIMARY KEY,
    owner UUID NOT NULL,
    public BOOLEAN NOT NULL,
    path TEXT NOT NULL,
    mime TEXT NOT NULL,
    FOREIGN KEY (owner) REFERENCES logins(uuid) ON DELETE CASCADE
);