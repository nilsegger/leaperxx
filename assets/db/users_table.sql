CREATE TABLE users(
    uuid UUID PRIMARY KEY,
    address INT,
    profile_picture UUID,
    display_name TEXT NOT NULL,
    birthday TIMESTAMP,
    gender TEXT,
    FOREIGN KEY (uuid) REFERENCES logins(uuid) ON DELETE CASCADE,
    FOREIGN KEY (address) REFERENCES addresses(id),
    FOREIGN KEY (profile_picture) REFERENCES files(uuid)
);