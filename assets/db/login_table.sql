CREATE TABLE logins (
    uuid UUID PRIMARY KEY,
    role TEXT NOT NULL,
    email TEXT NOT NULL,
    email_activated BOOLEAN DEFAULT false,
    email_activation_code TEXT,
    email_activation_expires TIMESTAMP,
    username TEXT,
    password TEXT,
    refresh_token TEXT,
    refresh_token_expires TIMESTAMP,
    refresh_token_revoked BOOLEAN
);