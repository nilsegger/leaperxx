CREATE TABLE email_update_verification(
    uuid UUID PRIMARY KEY,
    email TEXT NOT NULL,
    confirmation_code TEXT NOT NULL,
    confirmation_expires TIMESTAMP NOT NULL,
    foreign key (uuid) references logins(uuid) ON DELETE CASCADE
);