CREATE TABLE friends(
    id SERIAL PRIMARY KEY,
    sender UUID NOT NULL,
    receiver UUID NOT NULL,
    confirmed BOOLEAN DEFAULT false,
    confirmed_timestamp TIMESTAMP,
    FOREIGN KEY (sender) REFERENCES users(uuid) ON DELETE CASCADE,
    FOREIGN KEY (receiver) REFERENCES users(uuid) ON DELETE CASCADE
);