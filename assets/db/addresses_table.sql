CREATE TABLE addresses(
    id SERIAL PRIMARY KEY,
    street TEXT NOT NULL,
    location TEXT NOT NULL,
    canton TEXT NOT NULL,
    country TEXT NOT NULL,
    postal_code TEXT NOT NULL
);