#ifndef LEAPER_LIBS_ERRORS_INCLUDE_IO_ERRORS_HPP
#define LEAPER_LIBS_ERRORS_INCLUDE_IO_ERRORS_HPP

#include <system_error>

enum class IOErrors {
    SUCCESS = 0,

    //  Internal Server Error
    ARGON2_ERROR,
    OPENSSL_RAND_BYTES_ERROR,
    RSA_KEYS_NOT_FOUND,
    ERROR_REMOVING_FILE,
    FILE_SAVE_ERROR,
    MAIL_INITIALIZE,
    MAIL_SEND,

    // Unauthorized = 1000

    // Bad Request = 2000
   BASE64_ENCODING_ERROR,
    BASE64_DECODING_ERROR,
    UUID_INVALID,
    UUID_EMPTY,
    MODEL_VALIDATION_FAILED,
    // Not Found
    FILE_NOT_FOUND=3000,
    // Conflict 4000
    // Forbidden 5000
    // Unsupported media 6000
};

namespace std {
    template<>
    struct is_error_code_enum<IOErrors> : true_type {};
}

namespace {
    struct IOErrorsCategory : std::error_category {
        const char* name() const noexcept override {
            return "IOErrors";
        }

        std::string message(int e) const override {
            switch (static_cast<IOErrors>(e)) {
                case IOErrors::SUCCESS:
                    return "There was no error but it was handled???";
                case IOErrors::RSA_KEYS_NOT_FOUND:
                    return "Unable to find rsa keys.";
                case IOErrors::OPENSSL_RAND_BYTES_ERROR:
                    return "Unable to generate random bytes.";
                case IOErrors::FILE_NOT_FOUND:
                    return "Unable to find file.";
                case IOErrors::FILE_SAVE_ERROR:
                    return "Unable to save file.";
                case IOErrors::ERROR_REMOVING_FILE:
                    return "Unable to remove file.";
                case IOErrors::ARGON2_ERROR:
                    return "ARGON2 operation resulted in an error.";
                case IOErrors::BASE64_ENCODING_ERROR:
                    return "Error encountered encoding string to base64.";
                case IOErrors::BASE64_DECODING_ERROR:
                    return "Error encountered decoding base64 to string.";
                case IOErrors::MAIL_INITIALIZE:
                    return "Failed to initialize mail sending.";
                case IOErrors::MAIL_SEND:
                    return "Failed to send mail.";
                case IOErrors::UUID_INVALID:
			return "Uuid is invalid.";
                case IOErrors::UUID_EMPTY:
			return "UUID is empty.";
                case IOErrors::MODEL_VALIDATION_FAILED:
			return "Validaiton of model failed.";
                default:
                    return "Unknown error code received. '" + std::to_string(e) + "'";
            }
        }
    };

    const IOErrorsCategory io_codes_categories {};

    void fail(std::error_code& e, IOErrors c){
        e = c;
    }

}

inline std::error_code make_error_code(IOErrors e) {
    return {static_cast<int>(e), io_codes_categories};
}

#endif //LEAPER_LIBS_ERRORS_INCLUDE_IO_ERRORS_HPP
