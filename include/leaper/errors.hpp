#ifndef LEAPER_LIBS_ERRORS_INCLUDE_ERRORS_HPP
#define LEAPER_LIBS_ERRORS_INCLUDE_ERRORS_HPP

#include <system_error>

enum class error_codes {
    SUCCESS = 0,
    //  Internal Server Error
    // Unauthorized = 1000
    // Bad Request = 2000
    MODEL_FAILED_VALIDATION,
    ADDRESS_INVALID_COUNTRY,
    USER_INVALID_GENDER,
    EXISTING_FRIENDS,
    INVALID_SELF_REQUEST,
    // Not Found
    MODEL_NOT_FOUND=3000,
    MISSING_FRIEND_REQUEST,
    FRIEND_NOT_FOUND,
    // Conflict 4000
    DUPLICATE_FRIEND_REQUEST=4000,
    // Forbidden 5000
    ROLE_FORBIDDEN=5000,
    MODEL_VIEW_FORBIDDEN,
    // Unsupported media 6000
};

namespace std {
    template<>
    struct is_error_code_enum<error_codes> : true_type {};
}

namespace {
    struct error_codes_category : std::error_category {
        const char* name() const noexcept override {
            return "error_codes";
        }

        std::string message(int e) const override {
            switch (static_cast<error_codes>(e)) {
                case error_codes::SUCCESS:
                    return "There was no error but it was handled???";
                case error_codes::MODEL_NOT_FOUND:
                    return "Model was not found.";
                case error_codes::MODEL_FAILED_VALIDATION:
                    return "Model validation failed.";
                case error_codes::ADDRESS_INVALID_COUNTRY:
                    return "Invalid country.";
                case error_codes::USER_INVALID_GENDER:
                    return "Given gender is not recognised by system.";
                case error_codes::MODEL_VIEW_FORBIDDEN:
                    return "Not allowed to view model.";
                case error_codes::EXISTING_FRIENDS:
                    return "Users are already friends.";
                case error_codes::DUPLICATE_FRIEND_REQUEST:
                    return "Duplicate friend request.";
                case error_codes::INVALID_SELF_REQUEST:
                    return "Cant self request.";
                case error_codes::MISSING_FRIEND_REQUEST:
                    return "Missing friend request.";
                case error_codes::FRIEND_NOT_FOUND:
                    return "Friend not found.";
                case error_codes::ROLE_FORBIDDEN:
                    return "Role not sufficient.";
                default:
                    return "Unknown error code received. '" + std::to_string(e) + "'";
            }
        }
    };

    const error_codes_category codes_categories {};

    void fail(std::error_code& e, error_codes c){
        e = c;
    }

}

inline std::error_code make_error_code(error_codes e) {
    return {static_cast<int>(e), codes_categories};
}

#endif //LEAPER_LIBS_ERRORS_INCLUDE_ERRORS_HPP


