//
// Created by nils on 07/06/2020.
//

#ifndef LEAPER_SESSION_HPP
#define LEAPER_SESSION_HPP

#include <boost/beast/http.hpp>

#include "leaper/definitions.hpp"
#include "leaper/http/listener.hpp"
#include "leaper/http/sessions/ssl_session.hpp"
#include "leaper/http/sessions/plain_session.hpp"
#include "leaper/http/path_matcher.hpp"
#include "leaper/http/url.hpp"

#include "leaper/resources/test/user_info_resource.hpp"
#include "leaper/resources/controllers/user/user_controller_resource.hpp"
#include "leaper/resources/controllers/user/user_list_controller_resource.hpp"
#include "leaper/resources/controllers/user/user_profile_picture_controller_resource.hpp"
#include "leaper/resources/controllers/friends/friends_controller_resource.hpp"
#include "leaper/resources/controllers/friends/friends_list_controller_resource.hpp"
#include "leaper/resources/auth/user_register_resource.hpp"
#include "leaper/auth/auth_resource.hpp"
#include "leaper/auth/register_resource.hpp"
#include "leaper/auth/email_update_resource.hpp"
#include "leaper/auth/password_reset_resource.hpp"
#include "leaper/auth/username_resource.hpp"

namespace http = boost::beast::http;

namespace leaper {
	enum class Routes {
	    NOT_FOUND,
	    AUTH,
	    USER_REGISTER,
	    ACCOUNT_EMAIL_UPDATE,
	    PASSWORD_RESET,
	    USERNAME_UPDATE,
	    USER,
	    USERS,
	    USERS_FRIEND,
	    USERS_FRIENDS,
	    USERS_PROFILE_PICTURE,
	    USERS_PROFILE_PICTURE_CACHE,
	    DEV_USER,
	};

	template<class Session>
	void route_to_resource(Session* session, std::pair<string_t, http::Parameters>& url, Routes route, http::Parameters& pathParameters) {
		switch(route) {
			case Routes::DEV_USER:
			    session->template instantiate<resources::user_resource>(pathParameters, url.second);
			    break;
			case Routes::AUTH:
			    session->template instantiate<auth::auth_resource>(pathParameters, url.second);
			    break;
			case Routes::USER_REGISTER:
			    session->template instantiate<resources::UserRegisterResource>(pathParameters, url.second);
			    break;
			case Routes::ACCOUNT_EMAIL_UPDATE:
			    session->template instantiate<auth::EmailUpdateResource>(pathParameters, url.second);
			    break;
			case Routes::PASSWORD_RESET:
			    session->template instantiate<auth::PasswordResetResource>(pathParameters, url.second);
			    break;
			case Routes::USERNAME_UPDATE:
			    session->template instantiate<auth::UsernameResource>(pathParameters, url.second);
			    break;
			case Routes::USERS:
			    session->template instantiate<resources::UserListControllerResource>(pathParameters, url.second);
			    break;
			case Routes::USER:
			    session->template instantiate<resources::UserControllerResource>(pathParameters, url.second);
			    break;
			case Routes::USERS_FRIENDS:
			    session->template instantiate<resources::FriendsListControllerResource>(pathParameters, url.second);
			    break;
			case Routes::USERS_FRIEND:
			    session->template instantiate<resources::FriendsControllerResource>(pathParameters, url.second);
			    break;
			case Routes::USERS_PROFILE_PICTURE:
			    session->template instantiate<resources::UserProfilePictureControllerResource>(pathParameters, url.second);
			    break;
			case Routes::USERS_PROFILE_PICTURE_CACHE:
				{
				    auto resource = session->template instantiate<resources::UserProfilePictureControllerResource>(pathParameters, url.second);
				    resource->set_cache_control("public, max-age=31536000, immutable");
				    break;
				}
			default:
			    session->send_error_response(beast::http::status::not_found, "resource not found.");
	    }

	}

	class LeaperPlainSession : public http::PlainSession {

	    http::PathMatcher<Routes> * matcher_;

	protected:
	    void route_request() override;

	public:

	    LeaperPlainSession(net::io_context &ioc, beast::tcp_stream &&stream, beast::flat_buffer&& buffer, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port, http::PathMatcher<Routes>* router);
	};

	class LeaperSSLSession : public http::SSLSession {

	    http::PathMatcher<Routes> * matcher_;

	protected:
	    void route_request() override;

	public:

	    LeaperSSLSession(net::io_context &ioc, net::ssl::context& ctx, beast::tcp_stream &&stream, beast::flat_buffer&& buffer, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port, http::PathMatcher<Routes>* router);
	};

	class LeaperDetectSession : public http::DetectSession {

	    http::PathMatcher<Routes> * matcher_;
	public:
		LeaperDetectSession(net::io_context &ioc, net::ssl::context& ctx, tcp::socket &&socket, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port, http::PathMatcher<Routes>* router);

		void create_ssl_session() override;

		void create_plain_session() override;;
	};

	class LeaperListener : public http::Listener {

	    http::PathMatcher<Routes> * matcher_;

	protected:
	    std::shared_ptr<http::DetectSession> create_session(tcp::socket &socket) override;

	public:

	    LeaperListener(beast::error_code &ec, net::ssl::context &ctx, net::io_context &ioc, const tcp::endpoint &endpoint,
			   connection_pool_t *pool, http::PathMatcher<Routes> *matcher);
	};

} // namespace leaper

#endif //LEAPER_SESSION_HPP
