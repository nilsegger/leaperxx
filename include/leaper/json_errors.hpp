#ifndef LEAPER_LIBS_ERRORS_INCLUDE_JSON_ERRORS_HPP
#define LEAPER_LIBS_ERRORS_INCLUDE_JSON_ERRORS_HPP

#include <system_error>

enum class JSONErrors {
    SUCCESS = 0,

    // Bad Request
    JSON_INVALID=2000,
    JSON_INVALID_VALUE,
    JSON_KEY_MISSING,
    JSON_WRONG_TYPE,
};

namespace std {
    template<>
    struct is_error_code_enum<JSONErrors> : true_type {};
}

namespace {
    struct JSONErrorsCategory : std::error_category {
        const char* name() const noexcept override {
            return "JSONErrors";
        }

        std::string message(int e) const override {
            switch (static_cast<JSONErrors>(e)) {
                case JSONErrors::SUCCESS:
                    return "There was no error but it was handled???";
               case JSONErrors::JSON_INVALID:
                    return "Unable to decode JSON.";
                case JSONErrors::JSON_INVALID_VALUE:
                    return "Actual JSON value does not match expected.";
                case JSONErrors::JSON_KEY_MISSING:
                    return "Expected key in JSON not found.";
		case JSONErrors::JSON_WRONG_TYPE:
			return "Value is not of expected type.";
                default:
                    return "Unknown error code received. '" + std::to_string(e) + "'";
            }
        }
    };

    const JSONErrorsCategory json_codes_categories {};

    void fail(std::error_code& e, JSONErrors c){
        e = c;
    }

}

inline std::error_code make_error_code(JSONErrors e) {
    return {static_cast<int>(e), json_codes_categories};
}

#endif //LEAPER_LIBS_ERRORS_INCLUDE_JSON_ERRORS_HPP

