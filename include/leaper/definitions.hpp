//
// Created by nils on 08/05/2020.
//

#ifndef LEAPER_LIBS_DEFINITIONS_INCLUDE_DEFINITIONS_HPP
#define LEAPER_LIBS_DEFINITIONS_INCLUDE_DEFINITIONS_HPP

#include <boost/json.hpp>
#include <boost/uuid/uuid.hpp>

typedef boost::json::string string_t;
typedef boost::uuids::uuid uuid_t;

#endif //LEAPER_LIBS_DEFINITIONS_INCLUDE_DEFINITIONS_HPP
