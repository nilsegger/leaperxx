//
// Created by nils on 05/06/2020.
//

#ifndef LEAPER_LIBS_CONFIG_INCLUDE_CONFIG_HPP
#define LEAPER_LIBS_CONFIG_INCLUDE_CONFIG_HPP

#include <map>

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"

namespace leaper {
	class Config {

	    static std::optional<Config> kInstance;

	    std::map<string_t, string_t> map_;

	    string_t file_;

	    void read_file(std::error_code& ec);

	    Config();
	    Config(string_t& file);

	public:

	    static void initialize(std::error_code& ec, string_t file);

	    static string_t get(string_t key);

	};
}

#endif //LEAPER_LIBS_CONFIG_INCLUDE_CONFIG_HPP
