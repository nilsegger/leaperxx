//
// Created by nils on 05/06/2020.
//

#include <fstream>
#include <iostream>

#include "leaper/config.hpp"

std::optional<leaper::Config> leaper::Config::kInstance;

leaper::Config::Config() {

}

leaper::Config::Config(string_t& file) : file_(file) {

}

void leaper::Config::read_file(std::error_code& ec) {

    std::ifstream file(file_.c_str(), std::ios_base::in);

    if(!file.is_open()) {
        return fail(ec, IOErrors::FILE_NOT_FOUND);
    }

    std::string key, value;
    while(file >> key >> value) {
        map_[string_t(key.c_str(), key.size())] = string_t(value.c_str(), value.size());
    }

}

void leaper::Config::initialize(std::error_code& ec, string_t file) {
    kInstance = Config(file);
    kInstance->read_file(ec);
}

string_t leaper::Config::get(string_t key) {
    return kInstance->map_[key];
}




