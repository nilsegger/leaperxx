#include <cstdlib>
#include <iostream>
#include <thread>
#include <vector>

#include <boost/beast/core.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/json.hpp>

#include <ozo/connection_info.h>
#include <ozo/connection_pool.h>

#include "leaper/auth/jwt.hpp"
#include "leaper/http/listener.hpp"
#include "leaper/http/ssl.hpp"
#include "leaper/config.hpp"
#include "leaper/session.hpp"
#include "leaper/util/logger.hpp"

#include "jwtpp/jwtpp.hh"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>
using namespace ozo::literals;

int main(int argc, char *argv[]) {


    // Check command line arguments.
    if (argc != 2) {
        std::cerr <<
                  "Usage: leaper++ <path_to_config>\n" <<
                  "Example:\n" <<
                  "\tleaper++ path/to/config\n";
        return EXIT_FAILURE;
    }

    std::error_code configEc;
    leaper::Config::initialize(configEc, string_t(argv[1]));

    if(configEc) {
        std::cerr << "Failed to load config." << std::endl;
        return EXIT_FAILURE;
    }

    if(leaper::Config::get("logInfo") == "true") leaper::util::logger::info.set_active(true);
    if(leaper::Config::get("logDebug") == "true") leaper::util::logger::debug.set_active(true);
    if(leaper::Config::get("logError") == "true") leaper::util::logger::error.set_active(true);

    INFO << "Loaded configuration." << std::endl;

    auto const address = net::ip::make_address(leaper::Config::get("address").c_str());
    auto const port = static_cast<unsigned short>(std::atoi(leaper::Config::get("port").c_str()));
    auto const threads = std::max<int>(1, std::atoi(leaper::Config::get("threads").c_str()));

    // The io_context is required for all I/O
    net::io_context ioc{threads};

    // The SSL context is required, and holds certificates
    net::ssl::context ctx{net::ssl::context::tlsv12};

    beast::error_code ec;
    // This holds the self-signed certificate used by the server
    leaper::http::load_server_certificate(ec, ctx, leaper::Config::get("sslPassword"), leaper::Config::get("sslCertificate"), leaper::Config::get("sslKey"), leaper::Config::get("sslDh"));

    if(ec) {
    	ERROR << "Failed to load ssl certificates." << std::endl;
        return EXIT_FAILURE;
    }


    ozo::connection_pool_config config;
    ozo::connection_info conn_info("host=localhost port=5432 dbname=leaper++ user=postgres password=postgres");
    auto *pool = new ozo::connection_pool<ozo::connection_info<>>(conn_info, config);

    leaper::http::PathMatcher<leaper::Routes> matcher(leaper::Routes::NOT_FOUND);

    matcher.add_path("/auth", leaper::Routes::AUTH);
    matcher.add_path("/accounts/email", leaper::Routes::ACCOUNT_EMAIL_UPDATE);
    matcher.add_path("/accounts/password", leaper::Routes::PASSWORD_RESET);
    matcher.add_path("/accounts/{uuid}/email", leaper::Routes::ACCOUNT_EMAIL_UPDATE);
    matcher.add_path("/accounts/username", leaper::Routes::USERNAME_UPDATE);
    matcher.add_path("/accounts/{uuid}/username", leaper::Routes::USERNAME_UPDATE);
    matcher.add_path("/user", leaper::Routes::DEV_USER);
    matcher.add_path("/users", leaper::Routes::USERS);
    matcher.add_path("/users/register", leaper::Routes::USER_REGISTER);
    matcher.add_path("/users/{uuid}", leaper::Routes::USER);
    matcher.add_path("/users/{uuid}/profile_picture", leaper::Routes::USERS_PROFILE_PICTURE);
    matcher.add_path("/users/{uuid}/profile_picture/{_}", leaper::Routes::USERS_PROFILE_PICTURE_CACHE);
    matcher.add_path("/users/{uuid}/friends", leaper::Routes::USERS_FRIENDS);
    matcher.add_path("/users/{uuid}/friends/{friend}", leaper::Routes::USERS_FRIEND);

    auto listener = std::make_shared<leaper::LeaperListener>(
        ec,
        ctx,
        ioc,
        tcp::endpoint{address, port},
        pool,
        &matcher
    );

    if (ec) {
    	ERROR << "Failed to initialize listener " << ec.message() << std::endl;
        return EXIT_FAILURE;
    }

    listener->run();

    // Capture SIGINT and SIGTERM to perform a clean shutdown
    net::signal_set signals(ioc, SIGINT, SIGTERM);
    signals.async_wait(
            [&](beast::error_code const &, int) {
                // Stop the `io_context`. This will cause `run()`
                // to return immediately, eventually destroying the
                // `io_context` and all of the sockets in it.
                ioc.stop();
            });


    // Run the I/O service on the requested number of threads
    std::vector<std::thread> v;
    v.reserve(threads - 1);
    for (auto i = threads - 1; i > 0; --i)
        v.emplace_back(
                [&ioc] {
                    ioc.run();
                });

	INFO << "Listening on " <<  leaper::Config::get("address") << ":" << leaper::Config::get("port") << std::endl;
    ioc.run();

    // (If we get here, it means we got a SIGINT or SIGTERM)
    // Block until all the threads exit
    for(auto& t : v)
        t.join();

    delete pool;

    return EXIT_SUCCESS;
}
