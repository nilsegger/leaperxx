//
// Created by nils on 07/06/2020.
//

#include "leaper/session.hpp"

leaper::LeaperPlainSession::LeaperPlainSession(net::io_context &ioc, beast::tcp_stream&& stream, beast::flat_buffer&& buffer, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port, http::PathMatcher<Routes>* matcher)
	: http::PlainSession(ioc, std::move(stream), std::move(buffer), pool, remote_ip, remote_port), matcher_(matcher) {}

void leaper::LeaperPlainSession::route_request() {

    std::pair<string_t, http::Parameters> url = http::parse_url(this->parser_->get().target());
    http::Parameters pathParameters;
    Routes route = matcher_->match(url.first, pathParameters);

	route_to_resource(this, url, route, pathParameters);
}

leaper::LeaperSSLSession::LeaperSSLSession(net::io_context &ioc, net::ssl::context& ctx, beast::tcp_stream&& stream, beast::flat_buffer&& buffer, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port, http::PathMatcher<Routes>* matcher)
	: SSLSession(ioc, ctx, std::move(stream), std::move(buffer), pool, remote_ip, remote_port), matcher_(matcher) {}

void leaper::LeaperSSLSession::route_request() {

    std::pair<string_t, http::Parameters> url = http::parse_url(this->parser_->get().target());
    http::Parameters pathParameters;
    Routes route = matcher_->match(url.first, pathParameters);
	route_to_resource(this, url, route, pathParameters);
}

leaper::LeaperDetectSession::LeaperDetectSession(net::io_context &ioc, net::ssl::context& ctx, tcp::socket &&socket, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port, http::PathMatcher<Routes>* matcher)
	:DetectSession(ioc, ctx, std::move(socket), pool, remote_ip, remote_port), matcher_(matcher)
{

}

void leaper::LeaperDetectSession::create_ssl_session() {
	std::make_shared<LeaperSSLSession>(ioc_, ctx_, std::move(stream_), std::move(buffer_), pool_, remote_ip_, remote_port_, matcher_)->run();
}

void leaper::LeaperDetectSession::create_plain_session() {
	std::make_shared<LeaperPlainSession>(ioc_, std::move(stream_), std::move(buffer_), pool_, remote_ip_, remote_port_, matcher_)->run();
}

leaper::LeaperListener::LeaperListener(beast::error_code &ec, net::ssl::context &ctx, net::io_context &ioc,
                               const tcp::endpoint &endpoint, connection_pool_t *pool, http::PathMatcher<Routes> *matcher)
        : Listener(ec, ctx, ioc, endpoint, pool), matcher_(matcher) {}

std::shared_ptr<leaper::http::DetectSession> leaper::LeaperListener::create_session(tcp::socket &socket) {
    return std::make_shared<LeaperDetectSession>(this->ioc_, this->ctx_, std::move(socket), this->pool_, socket.remote_endpoint().address(), socket.remote_endpoint().port(), matcher_);
}

