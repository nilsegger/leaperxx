//
// Created by nils on 07/06/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_FILE_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_FILE_CONTROLLER_HPP

#include "leaper/controllers/controller.hpp"
#include "leaper/definitions.hpp"
#include "leaper/config.hpp"
#include "leaper/util/uuid.hpp"

namespace leaper::controllers {
struct FileControllerAddResult {
	uuid_t file_uuid;
	string_t file_path;
	FileControllerAddResult(uuid_t fileUuid, string_t filePath)
		: file_uuid(fileUuid), file_path(filePath) {}
};


template<class CALLBACK_C>
class FileController : public Controller {

protected:

    auto insert_file(uuid_t uuid, uuid_t owner, bool public_, string_t path, string_t mime) {
        return "INSERT INTO files(uuid, owner, public, path, mime) VALUES("_SQL
               + uuid + ", "_SQL
               + owner + ", "_SQL
               + public_ + ", "_SQL
               + path + ", "_SQL
               + mime + ")"_SQL;
    }

    string_t concat_absolute_path(string_t relative) {
        string_t path = Config::get("imagePath");
        assert(path[path.size() - 1] == '/');
        assert(relative[0] != '/');
        return path.append(relative);
    }

    string_t create_relative_path(string_t mime) {
        string_t path(util::uuid::to_string(util::uuid::create()));
        if (mime == "image/png") path.append(".png");
        else if (mime == "image/jpeg") path.append(".jpeg");
        else path.append(".mime-unknown.txt");
        return path;
    }

    void remove_file(string_t relative_path) {
        if (std::remove(concat_absolute_path(relative_path).c_str()) != 0) {
            // what should happen here? Is file most likely already deleted?
        }
    }

public:

    virtual void get(void (CALLBACK_C::*callback_member)(std::optional<string_t> path, std::optional<string_t> mime), CALLBACK_C *callback_instance) = 0;
	
    virtual void add(void(CALLBACK_C::* callback_member)(std::optional<FileControllerAddResult> result), CALLBACK_C *callback_instance, string_t mime) = 0;

    virtual void remove(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance) = 0;

    using Controller::Controller;

};

} // namespace leaper::controllers

#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_FILE_CONTROLLER_HPP
