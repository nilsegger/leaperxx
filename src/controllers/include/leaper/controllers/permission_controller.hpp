//
// Created by nils on 02/06/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLER_PERMISSION_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLER_PERMISSION_CONTROLLER_HPP

#include "leaper/controllers/controller.hpp"
#include "leaper/http/requester.hpp"

namespace leaper::controllers {
enum Permissions {
    NONE,
    EXISTS,
    VIEW,
    EDIT,
    EXISTS_AND_EDIT,
    SELF_EDIT_OTHER_VIEW,
    SELF_EDIT_OTHER_EXISTS
};

template<class CALLBACK_C>
class PermissionController : public Controller {

    void(CALLBACK_C::* callback_member_)();
    CALLBACK_C * callback_instance_;

protected:
    leaper::http::Requester* requester_;

    void finish() {
        (this->callback_instance_->*this->callback_member_)();
    }

public:

    PermissionController(std::error_code &ec, connection_t &connection, void (CALLBACK_C::* callbackMember)(),
                         CALLBACK_C *callbackInstance, leaper::http::Requester* requester) : Controller(ec, connection), callback_member_(callbackMember),
                                                         callback_instance_(callbackInstance), requester_(requester) {}

    virtual void check(Permissions permission) = 0; 
};
} // namespace leaper::controllers

#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLER_PERMISSION_CONTROLLER_HPP
