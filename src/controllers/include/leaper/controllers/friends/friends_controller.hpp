//
// Created by nils on 27/05/2020.
//

#ifndef LEAPER_FRIENDS_CONTROLLER_H
#define LEAPER_FRIENDS_CONTROLLER_H

#include "leaper/controllers/controller.hpp"
#include "leaper/definitions.hpp"
#include "leaper/models/user.hpp"
#include "leaper/sql/client.hpp"
#include "leaper/util/chrono.hpp"

namespace leaper::controllers {

template<class CALLBACK_C>
class FriendsController : public Controller {

    models::User* sender_;
    models::User* receiver_;

    std::unique_ptr<ozo::result> request_buffer_;
public:

    FriendsController(std::error_code& ec, connection_t& connection, models::User* sender, models::User* receiver) : sender_(sender), receiver_(receiver), Controller(ec, connection) {

    };

    void are_friends(void(CALLBACK_C::* callback_member)(std::optional<bool>), CALLBACK_C * callback_instance) {
        /*
         * If optional is empty, row does not exist, otherwise if true, sender and receiver are friends
         */
        request_buffer_ = std::make_unique<ozo::result>();
        auto stmt = "SELECT confirmed FROM friends WHERE"_SQL
                    + " (sender="_SQL + sender_->uuid + " AND receiver="_SQL + receiver_->uuid + ")"_SQL
                    + " OR (sender="_SQL + receiver_->uuid + " AND receiver="_SQL + sender_->uuid + ") LIMIT 1"_SQL;
        sql::fetch_value(this->ec_, callback_member, callback_instance, this->connection_, std::move(stmt), *request_buffer_);
    }

    void has_received_request(void(CALLBACK_C::* callback_member)(bool), CALLBACK_C * callback_instance) {
        request_buffer_ = std::make_unique<ozo::result>();
        auto stmt = "SELECT EXISTS(SELECT 1 FROM friends WHERE sender="_SQL + sender_->uuid + " AND receiver="_SQL + receiver_->uuid + ") AS exists"_SQL;
        sql::exists(this->ec_, callback_member, callback_instance, this->connection_, std::move(stmt), *request_buffer_);
    }

    void add_friends(void(CALLBACK_C::* callback_member)(), CALLBACK_C * callback_instance) {
        auto stmt = "INSERT INTO friends(sender, receiver, confirmed) VALUES("_SQL
                    + sender_->uuid + ", "_SQL
                    + receiver_->uuid + ", "_SQL
                    + false
                    + ")"_SQL;
        sql::execute(this->ec_, callback_member, callback_instance, this->connection_, std::move(stmt));
    }

    void accept_friend_request(void(CALLBACK_C::* callback_member)(), CALLBACK_C * callback_instance) {
        auto stmt = "UPDATE friends SET confirmed="_SQL + true + ", confirmed_timestamp="_SQL + util::chrono::now()
                    + " WHERE sender="_SQL + sender_->uuid + " AND receiver="_SQL + receiver_->uuid;

        sql::execute(this->ec_, callback_member, callback_instance, this->connection_, std::move(stmt));
    }

    void remove_friend(void(CALLBACK_C::* callback_member)(), CALLBACK_C * callback_instance) {
        auto stmt = "DELETE FROM friends WHERE "_SQL
                    + "(sender="_SQL + sender_->uuid + "AND receiver="_SQL + receiver_->uuid + ")"_SQL
                    + " OR (sender="_SQL + receiver_->uuid + "AND receiver="_SQL + sender_->uuid + ")"_SQL;

        sql::execute(this->ec_, callback_member, callback_instance, this->connection_, std::move(stmt));
    }

};
} // namespace leaper::controllers

#endif //LEAPER_FRIENDS_CONTROLLER_H
