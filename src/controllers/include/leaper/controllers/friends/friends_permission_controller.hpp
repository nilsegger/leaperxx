#ifndef SRC_CONTROLLERS_INCLUDE_CONTROLLERS_FRIEDS_FRIENDS_PERMISSION_CONTROLLER_HPP
#define SRC_CONTROLLERS_INCLUDE_CONTROLLERS_FRIEDS_FRIENDS_PERMISSION_CONTROLLER_HPP

#include "leaper/controllers/permission_controller.hpp"
#include "leaper/util/queue.hpp"
#include "leaper/controllers/user/user_permission_controller.hpp"
#include "leaper/models/user.hpp"

namespace leaper::controllers {
template<class CALLBACK_C>
class FriendsPermissionController : public PermissionController<CALLBACK_C> {

	using queue_type = util::queue::MemberQueue<FriendsPermissionController<CALLBACK_C>>;
	std::unique_ptr<queue_type> queue_;
	std::unique_ptr<UserPermissionController<queue_type>> sender_permission_controller_;
	std::unique_ptr<UserPermissionController<queue_type>> receiver_permission_controller_;
	models::User* sender_;
	models::User* receiver_;

public:
	/*
		Sender must not be the one who sent the friend request, but the actual api requester
	*/
    FriendsPermissionController(std::error_code &ec, connection_t &connection, void (CALLBACK_C::* callbackMember)(), CALLBACK_C *callbackInstance, http::Requester* requester, models::User* sender, models::User* receiver)
	: PermissionController<CALLBACK_C>(ec, connection, callbackMember, callbackInstance, requester), sender_(sender), receiver_(receiver)  {}

	void check(Permissions permission) override {

		switch(permission) {
			case Permissions::SELF_EDIT_OTHER_EXISTS:
				queue_ = std::make_unique<queue_type>(&FriendsPermissionController<CALLBACK_C>::finish, this, 2);	
				sender_permission_controller_ = std::make_unique<UserPermissionController<queue_type>>(this->ec_, this->connection_, &queue_type::done, queue_.get(), this->requester_, sender_);
				sender_permission_controller_->check(Permissions::EXISTS_AND_EDIT);
				receiver_permission_controller_ = std::make_unique<UserPermissionController<queue_type>>(this->ec_, this->connection_, &queue_type::done, queue_.get(), this->requester_, receiver_);
				receiver_permission_controller_->check(Permissions::EXISTS);
				break;
			default:
				fail(this->ec_, error_codes::MODEL_VIEW_FORBIDDEN);
				return this->finish();
		}
	}
};

} // namespace leaper::controllers

#endif // SRC_CONTROLLERS_INCLUDE_CONTROLLERS_FRIEDS_FRIENDS_PERMISSION_CONTROLLER_HPP
