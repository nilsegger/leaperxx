//
// Created by nils on 03/06/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_FRIENDS_LIST_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_FRIENDS_LIST_CONTROLLER_HPP

#include "leaper/controllers/list_controller.hpp"
#include "leaper/models/user.hpp"

namespace leaper::controllers {
template<class CALLBACK_C>
class FriendsListController : public ListController<CALLBACK_C> {

    models::User* user_;
protected:
    std::unique_ptr<models::Model> read_in(row_t &row) override {
        auto friend_ = std::make_unique<models::User>();
        sql::io::read(this->ec_, row[0], friend_->uuid);
        sql::io::read(this->ec_, row[1], friend_->display_name);
        return friend_;
    }

public:

    void list(void (CALLBACK_C::* callback_member)(std::vector<std::unique_ptr<models::Model>>), CALLBACK_C *callback_instance,
              unsigned int offset, unsigned int limit) override {
        auto stmt = "SELECT uuid, display_name FROM friends"_SQL
                    + " JOIN users ON CASE WHEN sender="_SQL + user_->uuid + " THEN users.uuid=friends.receiver ELSE users.uuid=friends.sender END"_SQL
                    + " WHERE confirmed=true AND (sender="_SQL + user_->uuid + " OR receiver="_SQL + user_->uuid + ")"_SQL;
        this->call_list(stmt, callback_member, callback_instance);
    }

    FriendsListController(std::error_code & ec, connection_t & connection, models::User* user) : user_(user), ListController<CALLBACK_C>(ec, connection) {}

};
} // namespace leaper::controllers

#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_FRIENDS_LIST_CONTROLLER_HPP
