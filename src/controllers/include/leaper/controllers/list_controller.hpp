//
// Created by nils on 03/06/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLER_LIST_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLER_LIST_CONTROLLER_HPP

#include "leaper/controllers/controller.hpp"
#include "leaper/models/model.hpp"
#include "leaper/sql/client.hpp"

namespace leaper::controllers {
template<class CALLBACK_C>
class ListController : public Controller {

    struct Container {
        ozo::result buffer;
        void(CALLBACK_C::* callback_member)(std::vector<std::unique_ptr<models::Model>>);
        CALLBACK_C *callback_instance;

        Container(void(CALLBACK_C::* callback_member)(std::vector<std::unique_ptr<models::Model>>), CALLBACK_C * callback_instance) : callback_member(callback_member), callback_instance(callback_instance) {}
    };

    std::unique_ptr<Container> container_;

    void list_callback() {
        std::vector<std::unique_ptr<models::Model>> result;
        for(size_t i = 0; i < container_->buffer.size(); i++) {
            row_t row = container_->buffer[i];
            result.push_back(read_in(row));
        }
        (container_->callback_instance->*container_->callback_member)(std::move(result));
    }

protected:
    virtual std::unique_ptr<models::Model> read_in(row_t &row) = 0;

    template<typename QUERY_T>
    void call_list(QUERY_T &stmt, void(CALLBACK_C::* callback_member)(std::vector<std::unique_ptr<models::Model>>),
                   CALLBACK_C *callback_instance) {
        container_ = std::make_unique<Container>(callback_member, callback_instance);
        sql::request(this->ec_, &ListController::list_callback, this, this->connection_, std::move(stmt), container_->buffer);
    }

public:

    virtual void list(void(CALLBACK_C::* callback_member)(std::vector<std::unique_ptr<models::Model>>), CALLBACK_C *callback_instance, unsigned int offset, unsigned int limit) = 0;

    using Controller::Controller;
};
} // namespace leaper::controllers

#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLER_LIST_CONTROLLER_HPP
