//
// Created by nils on 09/06/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_USER_PROFILE_PICTURE_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_USER_PROFILE_PICTURE_CONTROLLER_HPP

#include <cstdio>

#include "leaper/controllers/file_controller.hpp"
#include "leaper/sql/client.hpp"
#include "leaper/sql/transaction.hpp"
#include "leaper/config.hpp"
#include "leaper/util/logger.hpp"
#include "leaper/models/user.hpp"

namespace leaper::controllers {
template<class CALLBACK_C>
class UserProfilePictureController : public FileController<CALLBACK_C> {

    std::optional<ozo::result> buffer_;
    std::optional<string_t> new_path_;
    std::optional<string_t> old_path_;
    std::unique_ptr<ozo::rows_of<std::optional<string_t>, std::optional<string_t>>> read_buffer_;

    models::User* user_;

    template<typename CALLBACK_T>
    void on_add(CALLBACK_T callback, uuid_t uuid) {

        if (old_path_) {
            this->remove_file(old_path_.value());
        }

        callback(FileControllerAddResult(uuid, this->concat_absolute_path(new_path_.value())));
    }

public:

    UserProfilePictureController(std::error_code &ec, connection_t &connection, models::User *user) : user_(user), FileController<CALLBACK_C>(ec, connection) {}

    void get(void (CALLBACK_C::*callback_member)(std::optional<string_t> path, std::optional<string_t> mime), CALLBACK_C *callback_instance) override {

    	DEBUG << "Getting profile picture for " << user_->uuid << std::endl;

        read_buffer_ = std::make_unique<ozo::rows_of<std::optional<string_t>, std::optional<string_t>>>();
        auto stmt = "SELECT path, mime FROM users RIGHT JOIN files ON files.uuid=users.profile_picture WHERE users.uuid="_SQL + user_->uuid;
        sql::fetch_rows(this->ec_, [this, callback_member, callback_instance]() {
            if (this->ec_ || read_buffer_->empty()) {
	    	DEBUG << "Unable to find profile_picture for " << user_->uuid << std::endl;
                return (callback_instance->*callback_member)(std::nullopt, std::nullopt);
            }
            std::optional<string_t> path = this->concat_absolute_path(std::get<0>((*read_buffer_)[0]).value());
	    	DEBUG << "Found profile_picture for" << user_->uuid << " path " << path.value_or("(error no path)") << std::endl;
            return (callback_instance->*callback_member)(path, std::get<1>((*read_buffer_)[0]));
        }, this->connection_, std::move(stmt), *read_buffer_);

    }

    void add(void (CALLBACK_C::*callback_member)(std::optional<FileControllerAddResult> result), CALLBACK_C *callback_instance, string_t mime) override {
        // If existing profile_picture, old one has to be deleted and replace by new one

        sql::transaction::begin(this->ec_, [this, mime, callback = std::bind(callback_member, callback_instance, std::placeholders::_1)](auto &&conn) {

            if (this->ec_) callback(std::nullopt);

            auto stmt = "SELECT profile_picture FROM users WHERE uuid="_SQL + user_->uuid;

            buffer_.emplace();
            sql::transaction::template fetch_value<uuid_t>(this->ec_, [this, mime, callback](auto &&conn, std::optional<uuid_t> profile_picture) {
                if (this->ec_) return sql::transaction::rollback(this->ec_, std::bind(callback, std::nullopt), conn);

                // Create new uuid, new path and create new db entry
                // uuid should be changed to create new unique url for browsers to cache
                uuid_t uuid = util::uuid::create();
                new_path_ = this->create_relative_path(mime);
                auto stmt = this->insert_file(uuid, user_->uuid, false, new_path_.value(), mime);
                sql::transaction::execute(this->ec_, [this, uuid, profile_picture, callback](auto &&conn) {
                    if (this->ec_) return sql::transaction::rollback(this->ec_, std::bind(callback, std::nullopt), conn);

                    auto stmt = "UPDATE users SET profile_picture="_SQL + uuid + " WHERE uuid="_SQL + user_->uuid;
                    sql::transaction::execute(this->ec_, [this, profile_picture, callback, uuid](auto &&conn) {
                        if (this->ec_) return sql::transaction::rollback(this->ec_, std::bind(callback, std::nullopt), conn);

                        if (profile_picture) {
                            auto stmt = "DELETE FROM files WHERE uuid="_SQL + profile_picture.value() + " RETURNING path"_SQL;
                            buffer_.emplace();
                            sql::transaction::template fetch_value<string_t>(this->ec_, [this, callback, uuid](auto &&conn, std::optional<string_t> path) {
                                                                                 if (this->ec_) return sql::transaction::rollback(this->ec_, std::bind(callback, std::nullopt), conn);
                                                                                 old_path_ = path;
                                                                                 sql::transaction::commit(this->ec_, [this, callback, uuid]() {
                                                                                     on_add(callback, uuid);
                                                                                 }, conn);
                                                                             }, conn, std::move(stmt),
                                                                             buffer_.value()); // Delete old profile_picture row
                        } else {
                            sql::transaction::commit(this->ec_, [this, callback, uuid]() { on_add(callback, uuid); }, conn);
                        }

                    }, conn, std::move(stmt)); // Update user profile_picture reference

                }, conn, std::move(stmt)); // Insert file

            }, conn, std::move(stmt), buffer_.value()); // Select profile_picture

        }, this->connection_); // Begin transaction
    }

    void remove(void (CALLBACK_C::*callback_member)(), CALLBACK_C *callback_instance) override {
        sql::transaction::begin(this->ec_, [this, callback = std::bind(callback_member, callback_instance)](auto &&conn) {
            if (this->ec_) return callback();
            auto stmt = "SELECT profile_picture FROM users WHERE uuid="_SQL + user_->uuid;
            buffer_.emplace();
            sql::transaction::template fetch_value<uuid_t>(this->ec_, [this, callback](auto &&conn, std::optional<uuid_t> profile_picture) {
                if (this->ec_) return sql::transaction::rollback(this->ec_, callback, conn);
                if (!profile_picture) {
                    // User does not have a profile_picture
                    return sql::transaction::commit(this->ec_, callback, conn);
                }
                auto stmt = "UPDATE users SET profile_picture=null WHERE uuid="_SQL + user_->uuid;
                sql::transaction::execute(this->ec_, [this, profile_picture, callback](auto &&conn) {
                    if (this->ec_) return sql::transaction::rollback(this->ec_, callback, conn);
                    buffer_.emplace();
                    auto stmt = "DELETE FROM files WHERE uuid="_SQL + profile_picture.value() + " RETURNING path"_SQL;
                    sql::transaction::template fetch_value<string_t>(this->ec_, [this, callback](auto &&conn, std::optional<string_t> path) {
                        if (this->ec_) return sql::transaction::rollback(this->ec_, callback, conn);
                        sql::transaction::commit(this->ec_, [this, path, callback]() {
                            this->remove_file(path.value());
                            callback();
                        }, conn); // commit
                    }, conn, std::move(stmt), buffer_.value()); // delete file row
                }, conn, std::move(stmt)); // update user row to set profile_picture to null
            }, conn, std::move(stmt), buffer_.value()); // fetch profile_picture id
        }, this->connection_); // transaction begin
    }

};
} // namespace leaper::controllers

#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_USER_PROFILE_PICTURE_CONTROLLER_HPP
