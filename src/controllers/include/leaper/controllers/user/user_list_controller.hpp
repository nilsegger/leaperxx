//
// Created by nils on 03/06/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_LIST_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_LIST_CONTROLLER_HPP

#include "leaper/controllers/list_controller.hpp"
#include "leaper/models/user.hpp"

namespace leaper::controllers {
template<class CALLBACK_C>
class UserListController : public ListController<CALLBACK_C> {

protected:
    std::unique_ptr<models::Model> read_in(row_t& row) override {
        auto user = std::make_unique<models::User>();
        sql::io::read(this->ec_, row[0], user->uuid);
        sql::io::read(this->ec_, row[1], user->display_name);
        return user;
    }

public:

    void list(void (CALLBACK_C::* callback_member)(std::vector<std::unique_ptr<models::Model>>), CALLBACK_C *callback_instance, unsigned int offset, unsigned int limit) override {
        auto stmt = "SELECT uuid, display_name FROM users OFFSET "_SQL + offset + " LIMIT "_SQL + limit;
        this->call_list(stmt, callback_member, callback_instance);
    }

    using ListController<CALLBACK_C>::ListController;

};

} // namespace leaper::controllers
#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_LIST_CONTROLLER_HPP
