//
// Created by nils on 14/05/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_CONTROLLER_HPP

#include <vector>

#include "leaper/util/validation.hpp"
#include "leaper/sql/client.hpp"
#include "leaper/sql/transaction.hpp"
#include "leaper/errors.hpp"
#include "leaper/models/user.hpp"
#include "leaper/controllers/model_controller.hpp"

namespace leaper::controllers {

template<class CALLBACK_C>
class UserController : public ModelController<CALLBACK_C> {

    std::optional<ozo::result> address_buffer_;

    template<typename CALLBACK_T>
    void fetch_address(CALLBACK_T callback) {
        address_buffer_.emplace();
        auto stmt = "SELECT address AS id FROM users WHERE uuid="_SQL + get_user()->uuid;
        sql::fetch_value<int32_t>(this->ec_, callback, this->connection_, std::move(stmt), *address_buffer_);
    }

protected:
    std::unique_ptr<models::Model> model_from_row(row_t &row) override {
        std::unique_ptr<models::User> model = std::make_unique<models::User>();
        model->from_row(this->ec_, row);
        return model;
    }

    models::User* get_user() {
        return (models::User *)this->model_;
    }

public:

    using ModelController<CALLBACK_C>::ModelController;

    bool validate(validation_types type) override {

        util::validation::check_str(this->ec_, get_user()->display_name, true, 3, 50);

        if (get_user()->address) {
            util::validation::check_str(this->ec_, get_user()->address->street, true, 3, 100);
            util::validation::check_str(this->ec_, get_user()->address->location, true, 3, 100);
            util::validation::check_str(this->ec_, get_user()->address->canton, true, 3, 100);
            util::validation::validate_country(this->ec_, get_user()->address->country);
            util::validation::check_str(this->ec_, get_user()->address->postal_code, true, 3, 25);
        }

        if (get_user()->birthday) {
            util::validation::check_age(this->ec_, get_user()->birthday.value(),
                                        50491132); // Checks if user is aged 16
        }

        if (get_user()->gender) {
            util::validation::validate_gender(this->ec_, get_user()->gender.value());
        }

        if (this->ec_) return false;
        return true;
    }

    void exists(void(CALLBACK_C::* callback_member)(bool), CALLBACK_C *callback_instance) override {
        auto stmt = "select exists(select 1 from users where uuid="_SQL + get_user()->uuid + ") as exists"_SQL;
        this->call_exists(callback_member, callback_instance, stmt);
    }

    void create(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance) override {

        if (this->validate(validation_types::CREATE)) {
            if (get_user()->address) {
                sql::transaction::begin(this->ec_, [this, callback_member, callback_instance](auto &&conn) {
                    if (this->ec_) return (callback_instance->*callback_member)(); // Rollback not necessary if begin failed
                    address_buffer_.emplace();
                    auto stmt = this->create_address(get_user()->address.value());
                    sql::transaction::template fetch_value<int32_t>(this->ec_, [this, callback_member, callback_instance](auto &&conn, std::optional<int32_t> addressId) {
                        if (this->ec_) return sql::transaction::rollback(this->ec_, std::bind(callback_member, callback_instance), conn);
                        auto stmt = this->create_user(*get_user(), addressId.value());
                        sql::transaction::execute(this->ec_, [this, callback_member, callback_instance](auto &&conn) {
                            if (this->ec_) return sql::transaction::rollback(this->ec_, std::bind(callback_member, callback_instance), conn);
                            sql::transaction::commit(this->ec_, std::bind(callback_member, callback_instance), conn); // commit
                        }, conn, std::move(stmt)); // insert user with address reference
                    }, conn, std::move(stmt), *address_buffer_); // insert address with returning id
                }, this->connection_); // transaction begin
            } else {
                // Insert user without address reference
                auto stmt = this->create_user(*get_user(), std::nullopt);
                sql::execute(this->ec_, callback_member, callback_instance, this->connection_, std::move(stmt));
            }
        } else {
            // Validation failed.
            (callback_instance->*callback_member)();
        }

    }

    void
    read(void (CALLBACK_C::* callback_member)(std::optional<std::unique_ptr<models::Model>>), CALLBACK_C *callback_instance,
         bool join) override {

        if (!join) {
            auto stmt =
                    "SELECT uuid, display_name, birthday, gender, profile_picture FROM users WHERE uuid="_SQL + get_user()->uuid;
            this->call_read(callback_member, callback_instance, stmt);
        } else {
            auto stmt =
                    "SELECT users.uuid, display_name, birthday, gender, profile_picture, id, street, location, canton, country, postal_code, email, username FROM users LEFT JOIN addresses ON addresses.id = users.address JOIN logins ON logins.uuid = users.uuid WHERE users.uuid="_SQL +
                    get_user()->uuid;
            this->call_read(callback_member, callback_instance, stmt);
        }
    }

    void update(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance) override {

        if (validate(validation_types::UPDATE)) {

            if (!get_user()->address) {
                // Only user row has to be updated
                auto stmt = this->update_user(*get_user(), std::nullopt);
                sql::execute(this->ec_, callback_member, callback_instance, this->connection_, std::move(stmt));
            } else {
                fetch_address([this, callback_member, callback_instance](std::optional<int32_t> address) {
                    if (this->ec_) return (callback_instance->*callback_member)();
                    sql::transaction::begin(this->ec_, [this, address, callback = std::bind(callback_member, callback_instance)](auto &&conn) {
                        if (this->ec_) return callback();
                        if (!address.has_value()) {
                            // Address needs to be inserted
                            address_buffer_.emplace();
                            auto stmt = this->create_address(get_user()->address.value());
                            sql::transaction::template fetch_value<int32_t>(this->ec_, [this, callback](auto &&conn, std::optional<int32_t> addressId) {
                                if (this->ec_) return sql::transaction::rollback(this->ec_, callback, conn);
                                assert(addressId.has_value());
                                auto stmt = this->update_user(*get_user(), addressId.value());
                                sql::transaction::final_execute(this->ec_, callback, conn, std::move(stmt)); // update user with address id
                            }, conn, std::move(stmt), *address_buffer_); // Insert address returning its id
                        } else {
                            // Address needs to be updated
                            get_user()->address->id = address.value();
                            auto stmt = this->update_address(get_user()->address.value());
                            sql::transaction::execute(this->ec_, [this, callback](auto &&conn) {
                                if (this->ec_) return sql::transaction::rollback(this->ec_, callback, conn);
                                auto stmt = this->update_user(*get_user(), std::nullopt);
                                sql::transaction::final_execute(this->ec_, callback, conn, std::move(stmt)); // update user without address
                            }, conn, std::move(stmt)); // update address
                        }
                    }, this->connection_); // transaction begin
                }); // Fetch address
            }

        } else {
            // Validation failed
            (callback_instance->*callback_member)();
        }
    }

    void remove(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance) override {
        fetch_address([this, callback_instance, callback_member](std::optional<int32_t> address) {
            if (this->ec_) return (callback_instance->*callback_member)();

            sql::transaction::begin(this->ec_, [this, address, callback = std::bind(callback_member, callback_instance)](auto &&conn) {
                if (this->ec_) return callback();
                auto stmt = "DELETE FROM logins WHERE uuid="_SQL + get_user()->uuid;
                sql::transaction::execute(this->ec_, [this, address, callback](auto &&conn) {
                    if (this->ec_) return sql::transaction::rollback(this->ec_, callback, conn);
                    if (address) {
                        auto stmt = "DELETE FROM addresses WHERE id="_SQL + address.value();
                        sql::transaction::final_execute(this->ec_, callback, conn, stmt); // delete address
                    } else {
                        sql::transaction::commit(this->ec_, callback, conn);
                    }
                }, conn, std::move(stmt)); // delete login
            }, this->connection_); // transaction begin
        }); // fetch_address
    }
};
} // namespace leaper::controllers

#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_CONTROLLER_HPP
