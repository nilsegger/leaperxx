//
// Created by nils on 02/06/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_PERMISSION_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_PERMISSION_CONTROLLER_HPP

#include "leaper/controllers/permission_controller.hpp"
#include "leaper/controllers/user/user_controller.hpp"
#include "leaper/models/user.hpp"

namespace leaper::controllers {
template<class CALLBACK_C>
class UserPermissionController : public PermissionController<CALLBACK_C> {

	models::User* user_;
    	std::unique_ptr<UserController<UserPermissionController>> user_controller_;

    void exists_callback(bool userExists) {
    	if(!userExists) fail(this->ec_, error_codes::MODEL_NOT_FOUND);
	this->finish();
    }
public:

    UserPermissionController(std::error_code &ec, connection_t &connection, void (CALLBACK_C::* callbackMember)(), CALLBACK_C *callbackInstance, http::Requester* requester, models::User* user)
                            : PermissionController<CALLBACK_C>(ec, connection, callbackMember, callbackInstance, requester), user_(user)
			    {}

    void check(Permissions permission) override {
        if(this->ec_) return this->finish();
	bool canEdit = user_->uuid == this->requester_->uuid || this->requester_->role == "admin";
	switch(permission) {
		case Permissions::EDIT:
			if(canEdit) return this->finish();
		case Permissions::EXISTS_AND_EDIT:
			if(canEdit) {
				user_controller_ = std::make_unique<UserController<UserPermissionController>>(this->ec_, this->connection_, user_);	
				return user_controller_->exists(&UserPermissionController<CALLBACK_C>::exists_callback, this);
			}
		case Permissions::EXISTS:
				user_controller_ = std::make_unique<UserController<UserPermissionController>>(this->ec_, this->connection_, user_);	
				return user_controller_->exists(&UserPermissionController<CALLBACK_C>::exists_callback, this);
		default:
                	fail(this->ec_, error_codes::MODEL_VIEW_FORBIDDEN);
			return this->finish();
	}
   }
};
} // namespace leaper::controllers

#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_USER_PERMISSION_CONTROLLER_HPP
