//
// Created by nils on 20/05/2020.
//

#ifndef LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_CONTROLLER_HPP
#define LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_CONTROLLER_HPP

#include "leaper/errors.hpp"
#include "leaper/sql/client.hpp"
#include "leaper/models/model.hpp"
#include "leaper/models/address.hpp"
#include "leaper/models/user.hpp"
#include "leaper/controllers/controller.hpp"

namespace leaper::controllers {
enum validation_types {
    CREATE, UPDATE, DELETE
};

template<class CALLBACK_C>
class ModelController : public Controller {

protected:

    auto create_address(models::Address &address) {
        return "INSERT INTO addresses(street, location, canton, country, postal_code) VALUES ("_SQL
               + address.street + ", "_SQL
               + address.location + ", "_SQL
               + address.canton + ", "_SQL
               + address.country + ", "_SQL
               + address.postal_code +
               ") RETURNING ID;"_SQL;
    }

    auto update_address(models::Address& address) {
        return
                "UPDATE addresses SET"_SQL
                + " street="_SQL + address.street
                + ", location="_SQL + address.location
                + ", canton="_SQL + address.canton
                + ", country="_SQL + address.country
                + ", postal_code="_SQL + address.postal_code
                + " WHERE id="_SQL + address.id +";"_SQL;
    }

    auto create_user(models::User &user, std::optional<int32_t> address) {
        return
                "INSERT INTO users(uuid, display_name, birthday, gender, address) VALUES("_SQL
                + user.uuid + ", "_SQL
                + user.display_name + ", "_SQL
                + user.birthday + ", "_SQL
                + user.gender + ", "_SQL
                + address
                + ");"_SQL;
    }

    auto update_user(models::User &user, std::optional<int32_t> address) {
        return
                "UPDATE users SET"_SQL
                + " display_name="_SQL + user.display_name
                + ", birthday=COALESCE("_SQL + user.birthday + ", birthday)"_SQL
                + ", gender=COALESCE("_SQL + user.gender+ ", gender)"_SQL
                + ", address=COALESCE("_SQL + address + ", address) "_SQL
                + "WHERE uuid="_SQL + user.uuid + ";"_SQL;
    }



    virtual std::unique_ptr<models::Model> model_from_row(row_t &row) = 0;

    struct ReadContainer {
        ozo::result result;

        void (CALLBACK_C::* callback_member)(std::optional<std::unique_ptr<models::Model>>);

        CALLBACK_C *callback_instance;

        ReadContainer(void (CALLBACK_C::* callback_member)(std::optional<std::unique_ptr<models::Model>>),
                      CALLBACK_C *callback_instance)
                : callback_member(callback_member), callback_instance(callback_instance) {}
    };

    std::unique_ptr<ReadContainer> read_container_;

    template<typename QUERY_T>
    void
    call_read(void(CALLBACK_C::* callback_member)(std::optional<std::unique_ptr<models::Model>>), CALLBACK_C *callback_instance,
              QUERY_T &query) {
        read_container_ = std::make_unique<ReadContainer>(callback_member, callback_instance);
        sql::request(this->ec_, &ModelController::read_callback, this, this->connection_, std::move(query), read_container_->result);
    }

    void read_callback() {
        assert(read_container_->result.size() <= 1);
        if (!this->ec_ && read_container_->result.size() == 1) {
            std::optional<std::unique_ptr<models::Model>> model;
            row_t row = read_container_->result[0];
            model = model_from_row(row);
            (read_container_->callback_instance->*read_container_->callback_member)(std::move(model));
        } else {
            (read_container_->callback_instance->*read_container_->callback_member)(std::nullopt);
        }
    }

    struct ExistsContainer {
        ozo::result buffer;
        void (CALLBACK_C::* callback_member)(bool);
        CALLBACK_C *callback_instance;

        ExistsContainer(void(CALLBACK_C::* callback_member)(bool), CALLBACK_C *callback_instance) : callback_member(
                callback_member), callback_instance(callback_instance) {

        }
    };

	std::unique_ptr<ExistsContainer> exists_container_;

    template<typename QUERY_T>
    void call_exists(void(CALLBACK_C::* callback_member)(bool), CALLBACK_C *callback_instance, QUERY_T &query) {
    	exists_container_ = std::make_unique<ExistsContainer>(callback_member, callback_instance);
	sql::fetch_value(this->ec_, &ModelController::exists_callback, this, this->connection_, std::move(query), exists_container_->buffer);
    }

    void exists_callback(std::optional<bool> exists) {
        if (!this->ec_ && exists.value_or(false)) {
            (exists_container_->callback_instance->*exists_container_->callback_member)(true);
        } else {
            (exists_container_->callback_instance->*exists_container_->callback_member)(false);
        }
    }

public:

    models::Model* model_;

    ModelController(std::error_code &ec, connection_t &connection, models::Model* model)
            : model_(model), Controller(ec, connection) {

    }

    virtual void exists(void(CALLBACK_C::* callback_member)(bool), CALLBACK_C *callback_instance) = 0; 

    virtual bool validate(validation_types type) = 0; 

    virtual void create(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance) = 0; 

    virtual void read(void(CALLBACK_C::* callback_member)(std::optional<std::unique_ptr<models::Model>>), CALLBACK_C *callback_instance, bool join) = 0;
    
    virtual void update(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance) = 0; 

    virtual void remove(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance) = 0;

};

} // namespace leaper::controllers
#endif //LEAPER_CONTROLLERS_INCLUDE_CONTROLLERS_CONTROLLER_HPP
