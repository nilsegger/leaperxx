//
// Created by nils on 27/05/2020.
//

#ifndef LEAPER_CONTROLLER_HPP
#define LEAPER_CONTROLLER_HPP

#include "leaper/errors.hpp"
#include "leaper/sql/client.hpp"

namespace leaper::controllers {
class Controller {

protected:
    std::error_code &ec_;
    connection_t &connection_;

public:
    Controller(std::error_code &ec, connection_t &connection)
            : ec_(ec), connection_(connection) {

    }
};
} // namespace leaper::controllers
#endif //LEAPER_CONTROLLER_HPP
