#include "leaper/http/sessions/detect_session.hpp"

leaper::http::DetectSession::DetectSession(net::io_context &ioc, net::ssl::context& ctx, tcp::socket &&socket, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port)
		:stream_(std::move(socket)), ctx_(ctx), ioc_(ioc), pool_(pool), remote_ip_(remote_ip), remote_port_(remote_port) {}

void leaper::http::DetectSession::run() {
	net::dispatch(stream_.get_executor(), beast::bind_front_handler(&leaper::http::DetectSession::on_run, this->shared_from_this()));
}

void leaper::http::DetectSession::on_run() {
	stream_.expires_after(std::chrono::seconds(30));
	beast::async_detect_ssl(stream_, buffer_, beast::bind_front_handler(&leaper::http::DetectSession::on_detect, this->shared_from_this()));
}

void leaper::http::DetectSession::on_detect(beast::error_code ec, bool result) {
	if(ec) return;
	if(result) {
		create_ssl_session();
	} else {
		create_plain_session();
	}
}

