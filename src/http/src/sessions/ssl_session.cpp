#include "leaper/http/sessions/ssl_session.hpp"
#include "leaper/util/logger.hpp"

leaper::http::SSLSession::SSLSession(net::io_context &ioc, net::ssl::context& ctx, beast::tcp_stream&& stream, beast::flat_buffer&& buffer, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port)
    	: Session(ioc, pool, remote_ip, remote_port, std::move(buffer)), stream_(std::move(stream), ctx) {}

beast::ssl_stream<beast::tcp_stream>& leaper::http::SSLSession::stream() {
	return stream_;
}

void leaper::http::SSLSession::run() {
	beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));
	stream_.async_handshake(net::ssl::stream_base::server, buffer_.data(), beast::bind_front_handler(&leaper::http::SSLSession::on_handshake, this->shared_from_this()));
}

void leaper::http::SSLSession::on_handshake(beast::error_code ec, std::size_t bytes_used) {
	if(ec) {
		ERROR << "Handschake failed: " << ec.message() << std::endl;
		return;
	}

	buffer_.consume(bytes_used);

	do_read();
}


void leaper::http::SSLSession::do_close() {
	beast::get_lowest_layer(stream_).expires_after(std::chrono::seconds(30));
	stream_.async_shutdown(beast::bind_front_handler(&leaper::http::SSLSession::on_shutdown, shared_from_this()));
}

void leaper::http::SSLSession::on_shutdown(beast::error_code ec) {
	// At this point the connection is closed gracefully
}

