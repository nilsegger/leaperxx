#include "leaper/http/sessions/plain_session.hpp"

leaper::http::PlainSession::PlainSession(net::io_context &ioc, beast::tcp_stream&& stream, beast::flat_buffer&& buffer, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port)
    	: Session(ioc, pool, remote_ip, remote_port, std::move(buffer)), stream_(std::move(stream)) {}

beast::tcp_stream& leaper::http::PlainSession::stream() {
	return stream_;
}

void leaper::http::PlainSession::run() {
	this->do_read();
}

void leaper::http::PlainSession::do_close() {
	beast::error_code ec;
	stream_.socket().shutdown(tcp::socket::shutdown_send, ec);
}

