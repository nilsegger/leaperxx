//
// Created by nils on 07/06/2020.
//

#include "leaper/http/url.hpp"

std::pair<string_t, leaper::http::Parameters> leaper::http::parse_url(string_t target) {
    std::size_t params_pos = target.find_last_of("?");

    if (params_pos == std::string::npos) return {target, Parameters()};
    Parameters parameters;

    bool appendingName = true;
    string_t name;
    string_t value;
    for (size_t i = params_pos + 1; i < target.size(); i++) {
        if (appendingName && target[i] == '=') {
            appendingName = false;
        } else if (appendingName) {
            name.append(1, target[i]);
        } else if (!appendingName && target[i] == '&') {
            appendingName = true;
            parameters.add(name, value);
            name.clear();
            value.clear();
        } else if (!appendingName) {
            value.append(1, target[i]);
            if (!appendingName && i == target.size() - 1) {
                parameters.add(name, value);
            }
        }
    }

    return {target.subview(0, params_pos), parameters};
}

