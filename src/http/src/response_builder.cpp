//
// Created by nils on 08/06/2020.
//

#include "leaper/http/response_builder.hpp"

beast::http::response<beast::http::empty_body> leaper::http::response::build_successful(unsigned version) {
	return {beast::http::status::ok, version};
}

beast::http::response<beast::http::string_body> leaper::http::response::build_successful(unsigned version, string_t message) {
	beast::http::response<beast::http::string_body> response{beast::http::status::ok, version};
	response.body() = message.c_str();
	return response;
}

beast::http::response<beast::http::empty_body> leaper::http::response::build_error(unsigned version, beast::http::status status) {
	return {status, version};
}

beast::http::response<beast::http::string_body> leaper::http::response::build_error(unsigned version, beast::http::status status, string_t message) {
	beast::http::response<beast::http::string_body> response{status, version};
	response.body() = message.c_str();
	return response;
}

beast::http::response<beast::http::empty_body> leaper::http::response::build_not_found(unsigned version) {
	return {beast::http::status::not_found, version};
}

beast::http::response<beast::http::empty_body> leaper::http::response::build_method_not_allowed(unsigned version) {
	return {beast::http::status::method_not_allowed, version};
}

beast::http::response<beast::http::empty_body> leaper::http::response::build_unauthorized(unsigned version) {
	return {beast::http::status::unauthorized, version};
}


beast::http::response<beast::http::string_body> leaper::http::response::build_json(unsigned version, beast::http::status status, json::object &json) {
	beast::http::response<beast::http::string_body> response{status, version};
	response.set(beast::http::field::content_type, "application/json");
	response.body() = json::serialize(json).c_str();
	return response;
};

beast::http::response<beast::http::string_body> leaper::http::response::build_json(unsigned version, beast::http::status status, json::array &json) {
	beast::http::response<beast::http::string_body> response{status, version};
	response.set(beast::http::field::content_type, "application/json");
	response.body() = json::serialize(json).c_str();
	return response;
};
