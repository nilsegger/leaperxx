//
// Created by nils on 12/06/2020.
//

#include "leaper/http/resources/method_routed_resource.hpp"

void leaper::http::MethodResource::on_request() {
    switch (this->header_parser_.get().method()) {
	case beast::http::verb::options:
            do_options();
            break;
        case beast::http::verb::get:
            do_get();
            break;
        case beast::http::verb::post:
            do_post();
            break;
        case beast::http::verb::put:
            do_put();
            break;
        case beast::http::verb::delete_:
            do_delete();
            break;
        default:
            this->handle_response();
            break;
    }
}

void leaper::http::MethodResource::build_method_not_allowed() {
    beast::http::response<beast::http::empty_body> response{beast::http::status::method_not_allowed, this->header_parser_.get().version()};
    this->do_write(std::move(response));
}

void leaper::http::MethodResource::do_options() {
    this->handle_response();
}

void leaper::http::MethodResource::do_get() {
    this->handle_response();
}

void leaper::http::MethodResource::do_post() {
    this->handle_response();
}

void leaper::http::MethodResource::do_put() {
    this->handle_response();
}

void leaper::http::MethodResource::do_delete() {
    this->handle_response();
}

void leaper::http::MethodResource::build_options_response() {
	beast::http::response<beast::http::empty_body> response{beast::http::status::ok, this->version()};
	response.set(beast::http::field::access_control_allow_origin, "*");	
	response.set(beast::http::field::access_control_allow_methods, "OPTIONS, GET, POST, PUT, DELETE");
	response.set(beast::http::field::access_control_allow_headers, "content-type, content-length, authorization");
	this->do_write(std::move(response));
}

void leaper::http::MethodResource::build_get_response() {
    build_method_not_allowed();
}

void leaper::http::MethodResource::build_post_response() {
    build_method_not_allowed();
}

void leaper::http::MethodResource::build_put_response() {
    build_method_not_allowed();
}

void leaper::http::MethodResource::build_delete_response() {
    build_method_not_allowed();
}

void leaper::http::MethodResource::build_response() {
    switch (this->header_parser_.get().method()) {
	case beast::http::verb::options:
            build_options_response();
            break;
        case beast::http::verb::get:
            build_get_response();
            break;
        case beast::http::verb::post:
            build_post_response();
            break;
        case beast::http::verb::put:
            build_put_response();
            break;
        case beast::http::verb::delete_:
            build_delete_response();
            break;
        default:
            build_method_not_allowed();
            break;
    }

}
