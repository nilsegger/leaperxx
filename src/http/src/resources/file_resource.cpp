//
// Created by nils on 12/06/2020.
//

#include "leaper/http/resources/file_resource.hpp"


beast::http::verb leaper::http::FileResource::method() {
    if (parser_) return parser_->get().method();
    return Resource::method();
}

string_t leaper::http::FileResource::target() {
    if (parser_) return parser_->get().target();
    return Resource::target();
}

unsigned int leaper::http::FileResource::version() {
    if (parser_) return parser_->get().version();
    return Resource::version();
}

