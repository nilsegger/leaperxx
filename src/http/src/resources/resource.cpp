//
// Created by nils on 12/06/2020.
//

#include "leaper/http/resources/resource.hpp"

leaper::http::Resource::Resource(std::function<void(bool close)> callback, net::io_context &ioc, connection_pool_t *pool,
                   beast::tcp_stream*stream, beast::http::request_parser<beast::http::empty_body> &header_parser, beast::flat_buffer *buffer, net::ip::address remote_ip,
                   unsigned short remote_port,
                   Parameters &pathParameters, Parameters &queryParameters)
        : callback_(std::move(callback)), ioc_(ioc), pool_(pool), plain_stream_(stream), is_ssl_stream_(false), header_parser_(header_parser), buffer_(buffer), remote_ip_(remote_ip),
          remote_port_(remote_port), path_parameters_(pathParameters), query_parameters_(queryParameters), begin_(std::chrono::steady_clock::now()) {};

leaper::http::Resource::Resource(std::function<void(bool close)> callback, net::io_context &ioc, connection_pool_t *pool,
                   beast::ssl_stream<beast::tcp_stream>* stream, beast::http::request_parser<beast::http::empty_body> &header_parser, beast::flat_buffer *buffer, net::ip::address remote_ip,
                   unsigned short remote_port,
                   Parameters &pathParameters, Parameters &queryParameters)
        : callback_(std::move(callback)), ioc_(ioc), pool_(pool), ssl_stream_(stream), is_ssl_stream_(true), header_parser_(header_parser), buffer_(buffer), remote_ip_(remote_ip),
          remote_port_(remote_port), path_parameters_(pathParameters), query_parameters_(queryParameters), begin_(std::chrono::steady_clock::now()) {};


beast::http::response<beast::http::string_body> leaper::http::Resource::build_error_response() {
    assert(ec_.value() != 0);
    beast::http::status response_code;
    if (ec_.value() < 1000) {
        response_code = beast::http::status::internal_server_error;
    } else if (ec_.value() < 2000) {
        response_code = beast::http::status::unauthorized;
    } else if (ec_.value() < 3000) {
        response_code = beast::http::status::bad_request;
    } else if (ec_.value() < 4000) {
        response_code = beast::http::status::not_found;
    } else if (ec_.value() < 5000) {
        response_code = beast::http::status::conflict;
    } else if (ec_.value() < 6000) {
        response_code = beast::http::status::forbidden;
    } else if (ec_.value() < 7000) {
        response_code = beast::http::status::unsupported_media_type;
    }
    beast::http::response<beast::http::string_body> resp{response_code, this->header_parser_.get().version()};
    resp.body() = ec_.message();
    return resp;
}

bool leaper::http::Resource::contains_header(boost::beast::string_view key) {
    auto headers = header_parser_.get().base();
    auto iterator = headers.find(key);
    return iterator != headers.end();
}

void leaper::http::Resource::get_header(boost::beast::string_view key, string_t& value) {
    auto headers = header_parser_.get();
    auto iterator = headers.find(key);
    if (iterator != headers.end()) {
        value = iterator->value().to_string();
    } else {
        fail(ec_, HTTPErrors::MISSING_HEADER);
    }
}

void leaper::http::Resource::prepare(unsigned int callbacks) {
    prepare_queue(callbacks);
    prepare_connection();
}

void leaper::http::Resource::prepare_queue(unsigned int callbacks) {
	INFO << "Preparing queue of resource" << std::endl;
    // Queue must always be instantiated because it extends the resources lifetime.
    queue_ = std::make_unique<QUEUE_T>(&leaper::http::Resource::handle_response, this->shared_from_this(), callbacks);
}

void leaper::http::Resource::prepare_connection() {
	INFO << "Preparing connection of resource" << std::endl;
    connection_ = std::make_unique<connection_t>((*pool_)[ioc_]);
}

void leaper::http::Resource::queue_item_finished() {
	INFO << "Resource queue task finished" << std::endl;
    queue_->done();
}

beast::http::verb leaper::http::Resource::method() {
    return header_parser_.get().method();
}

string_t leaper::http::Resource::target() {
    return header_parser_.get().target();
}

unsigned int leaper::http::Resource::version() {
    return header_parser_.get().version();
}

string_t leaper::http::Resource::user_id() {
    return "-";
}

void leaper::http::Resource::handle_response() {
	INFO << "Resource handling response" << std::endl;

    if (ec_) {
        beast::http::response<beast::http::string_body> response = build_error_response();
        do_write(std::move(response));
    } else {
        build_response();
    }

}

void leaper::http::Resource::on_write(bool close, beast::error_code ec, std::size_t bytes_transferred) {
	INFO << "Resource wrote repsonse" << std::endl;

    boost::ignore_unused(bytes_transferred);

    if (ec) return;

    callback_(close);

}

void leaper::http::Resource::abort() {
	INFO << "Resource abort called" << std::endl;
    queue_ = nullptr;
    callback_(true);
}

void leaper::http::Resource::build_response() {
    beast::http::response<beast::http::empty_body> response{beast::http::status::not_found, header_parser_.get().version()};
    do_write(std::move(response));
};

unsigned long long leaper::http::Resource::elapsed_milliseconds() {
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    return std::chrono::duration_cast<std::chrono::milliseconds>(end - begin_).count();
}


void leaper::http::Resource::on_request() {
	INFO << "Resource on_request" << std::endl;
    handle_response();
};

void leaper::http::Resource::set_cache_control(string_t cache_control) {
	cache_control_ = cache_control;
}
