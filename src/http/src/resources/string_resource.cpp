//
// Created by nils on 12/06/2020.
//

#include "leaper/http/resources/string_resource.hpp"
#include "leaper/util/json.hpp"

beast::http::verb leaper::http::StringResource::method() {
    if (parser_) return payload_->method();
    return Resource::method();
}

string_t leaper::http::StringResource::target() {
    if (parser_) return payload_->target();
    return Resource::target();
}

unsigned int leaper::http::StringResource::version() {
    if (parser_) return payload_->version();
    return Resource::version();
}

bool leaper::http::StringResource::get_json(boost::json::object &body) {
    if (payload_.value()[beast::http::field::content_type] != "application/json") {
        fail(this->ec_, HTTPErrors::UNSUPPORTED_MEDIA);
        this->handle_response();
        return false;
    }
    util::json::parse_as_object(payload_->body(), body, this->ec_);
    if (this->ec_) {
        this->handle_response();
        return false;
    }
    return true;
}

