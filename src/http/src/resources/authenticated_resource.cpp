//
// Created by nils on 12/06/2020.
//

#include "leaper/http/resources/authenticated_resource.hpp"
#include "leaper/auth/jwt.hpp"
#include "leaper/util/uuid.hpp"
#include "leaper/config.hpp"
#include "leaper/http/response_builder.hpp"
#include "jwtpp/jwtpp.hh"

bool leaper::http::AuthenticatedResource::parse_token(string_t &token) {
	INFO << "Parsing access token" << std::endl;

    this->get_header("authorization", token);
    if (this->ec_) return false;

    if (token.size() <= 7) {
        fail(this->ec_, HTTPErrors::INVALID_BEARER);
        return false;
    }
    DEBUG << "Access token " << token << std::endl;
    return true;
}

bool leaper::http::AuthenticatedResource::validate_token() {
	INFO << "Validating token" << std::endl;
    string_t token;
    if (!parse_token(token)) return false;

    jwtpp::claims claims;
    auth::JSONWebToken jwt_;
    jwt_.verify_access_token(this->ec_, claims, token);

    if (this->ec_) return false;

    json::string uuid, role;
    claims.get().any(this->ec_, uuid, "uuid");
    claims.get().any(this->ec_, role, "role");

    if (this->ec_) return false;

    boost::uuids::uuid converted_uuid = util::uuid::from_string(this->ec_, uuid);

    if (this->ec_) return false;

    requester_ = std::make_unique<struct Requester>(
            converted_uuid, role);
    authenticated_ = true;
    return true;
}

void leaper::http::AuthenticatedResource::do_authenticated_get() {
    this->handle_response();
}

void leaper::http::AuthenticatedResource::do_authenticated_post() {
    this->handle_response();
}

void leaper::http::AuthenticatedResource::do_authenticated_put() {
    this->handle_response();
}

void leaper::http::AuthenticatedResource::do_authenticated_delete() {
    this->handle_response();
}

void leaper::http::AuthenticatedResource::build_authenticated_get_response() {
    this->build_method_not_allowed();
}

void leaper::http::AuthenticatedResource::build_authenticated_post_response() {
    this->build_method_not_allowed();
}

void leaper::http::AuthenticatedResource::build_authenticated_put_response() {
    this->build_method_not_allowed();
}

void leaper::http::AuthenticatedResource::build_authenticated_delete_response() {
    this->build_method_not_allowed();
}

void leaper::http::AuthenticatedResource::build_get_response() {
    this->do_write(std::move(response::build_unauthorized(this->header_parser_.get().version())));
}

void leaper::http::AuthenticatedResource::build_post_response() {
    this->do_write(std::move(response::build_unauthorized(this->header_parser_.get().version())));
}

void leaper::http::AuthenticatedResource::build_put_response() {
    this->do_write(std::move(response::build_unauthorized(this->header_parser_.get().version())));
}

void leaper::http::AuthenticatedResource::build_delete_response() {
    this->do_write(std::move(response::build_unauthorized(this->header_parser_.get().version())));
}

void leaper::http::AuthenticatedResource::build_response() {
	INFO << "leaper::http::AuthenticatedResource::build_response" << std::endl;
    if (authenticated_) {
        switch (this->header_parser_.get().method()) {
            case beast::http::verb::get:
                build_authenticated_get_response();
                break;
            case beast::http::verb::post:
                build_authenticated_post_response();
                break;
            case beast::http::verb::put:
                build_authenticated_put_response();
                break;
            case beast::http::verb::delete_:
                build_authenticated_delete_response();
                break;
            default:
                this->build_method_not_allowed();
                break;
        }
    } else {
        MethodResource::build_response();
    }
}

string_t leaper::http::AuthenticatedResource::user_id() {
    if (authenticated_) return util::uuid::to_string(requester_->uuid);
    return Resource::user_id();
}

void leaper::http::AuthenticatedResource::after_authentication() {
	INFO << "leaper::http::AuthenticatedResource::aufter_authentication" << std::endl;
    authenticated_method_route();
}

void leaper::http::AuthenticatedResource::authenticated_method_route() {
	INFO << "leaper::http::AuthenticatedResource::authenticated_method_route" << std::endl;
    if (this->ec_) return this->handle_response();
    switch (this->header_parser_.get().method()) {
        case beast::http::verb::get:
            do_authenticated_get();
            break;
        case beast::http::verb::post:
            do_authenticated_post();
            break;
        case beast::http::verb::put:
            do_authenticated_put();
            break;
        case beast::http::verb::delete_:
            do_authenticated_delete();
            break;
        default:
            this->handle_response();
            break;
    }
}

void leaper::http::AuthenticatedResource::on_request() {
	INFO << "leaper::http::AuthenticatedResource::on_request" << std::endl;
    if (this->contains_header("authorization") && validate_token() && !this->ec_) {
        after_authentication();
    } else if (!this->ec_) {
        MethodResource::on_request();
    } else {
        this->handle_response();
    }
}

