//
// Created by nils on 05/06/2020.
//

#include <boost/asio/ssl.hpp>

#include "leaper/http/ssl.hpp"

void leaper::http::load_server_certificate(boost::system::error_code& ec, boost::asio::ssl::context& ctx, string_t password, string_t certificate_file, string_t key_file, string_t dh_file)
{

     ctx.set_password_callback([password](std::size_t, boost::asio::ssl::context_base::password_purpose) {
                    return password.c_str();
     });

    ctx.set_options(
        boost::asio::ssl::context::default_workarounds |
        boost::asio::ssl::context::no_sslv2 |
        boost::asio::ssl::context::single_dh_use);

    ctx.use_certificate_chain_file(std::string(certificate_file.c_str(), certificate_file.size()), ec);

    if(ec) return;

    ctx.use_private_key_file(std::string(key_file.c_str(), key_file.size()), boost::asio::ssl::context::file_format::pem, ec);

    if(ec) return;

    ctx.use_tmp_dh_file(std::string(dh_file.c_str(), dh_file.size()), ec);
}

