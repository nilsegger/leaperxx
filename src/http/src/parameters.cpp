#include "leaper/http/parameters.hpp"

void leaper::http::Parameters::add(string_t& name, string_t& value) {
	parameters_.emplace_back(name, value);
}

string_t leaper::http::Parameters::find(string_t name, string_t default_) {
	for(auto & parameter : parameters_) {
		if(parameter.first == name) return parameter.second;
	}
	return default_;
}

string_t leaper::http::Parameters::get(std::error_code&ec, string_t& name) {
	string_t raw = find(name);
	if(raw.empty()) {
		fail(ec, HTTPErrors::MISSING_PARAMETER);
	}
	return raw;
}

string_t leaper::http::Parameters::get_string(std::error_code& ec, string_t name) {
	return get(ec, name);
}

uuid_t leaper::http::Parameters::get_uuid(std::error_code& ec, string_t name) {
	string_t raw = get(ec, name);
	if(!ec) {
		return util::uuid::from_string(ec, raw);
	}
	return util::uuid::nil();
}

int leaper::http::Parameters::get_int(std::error_code& ec, string_t name) {
	string_t raw = get(ec, name);
	if(!ec) {
		try {
			return std::stoi(std::string(raw.c_str(), raw.size()));
		} catch (std::exception& exc) {
			fail(ec, HTTPErrors::PARAMETER_WRONG_TYPE);
		}
	}
	return 0;
}

void leaper::http::Parameters::clear() {
	parameters_.clear();
}
