//
// Created by nils on 13/04/2020.
//

#include <boost/asio/strand.hpp>

#include "leaper/http/listener.hpp"

leaper::http::Listener::Listener(beast::error_code& ec, net::ssl::context& ctx, net::io_context& ioc, tcp::endpoint endpoint, connection_pool_t * pool)
   : ioc_(ioc), ctx_(ctx), acceptor_(net::make_strand(ioc)), pool_(pool) {

    // Open the acceptor
    acceptor_.open(endpoint.protocol(), ec);
    if(ec)
    {
        return;
    }

    // Allow address reuse
    acceptor_.set_option(net::socket_base::reuse_address(true), ec);
    if(ec)
    {
        return;
    }

    // Bind to the server address
    acceptor_.bind(endpoint, ec);
    if(ec)
    {
        return;
    }

    // Start listening for connections
    acceptor_.listen(
            net::socket_base::max_listen_connections, ec);
    if(ec)
    {
        return;
    }
}

void leaper::http::Listener::run() {
    do_accept();
}

void leaper::http::Listener::do_accept() {

    // The new connection gets its own strand
    acceptor_.async_accept(net::make_strand(ioc_),
            beast::bind_front_handler(&leaper::http::Listener::on_accept, shared_from_this()));
}

void leaper::http::Listener::on_accept(beast::error_code ec, tcp::socket socket) {

   if(!ec) {
        // Create the session and run it
        // std::make_shared<session>(ioc_, std::move(socket), pool)->run();
       create_session(socket)->run();
    }

    // Accept another connection
    do_accept();
}
