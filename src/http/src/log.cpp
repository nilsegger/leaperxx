//
// Created by nils on 27/05/2020.
//

#include "leaper/http/log.hpp"

leaper::http::Log::Log(const boost::asio::ip::address ip, const unsigned short port, string_t user_id, const std::chrono::system_clock::time_point time, const beast::http::verb method, const string_t url, unsigned int http_version, int status,
         std::uint64_t size, unsigned long long elapsed_ms) : user_id(user_id), time(time), url(url), method(method), status(status),
                                                                             ip(ip), port(port),
                                                                             http_version(http_version), size(size), elapsed_ms(elapsed_ms) {}

std::ostream & operator << (std::ostream &out, const leaper::http::Log &log)
{
    out << log.ip << ':' << log.port << ' ';
    out << log.user_id << ' ';
    out << '[' << leaper::util::chrono::to_string(log.time) << ']' << ' ';
    out << '"' << beast::http::to_string(log.method) << ' ' << log.url << ' ' << log.http_version << '"' << ' ';
    out << log.status << ' ';
    out << log.size << ' ';
    out << log.elapsed_ms;
    return out;
}
