//
// Created by nils on 02/06/2020.
//

#ifndef LEAPER_REQUESTER_HPP
#define LEAPER_REQUESTER_HPP

#include <boost/json.hpp>
#include <boost/uuid/uuid.hpp>

namespace leaper::http {

struct Requester {
        boost::uuids::uuid uuid;
        boost::json::string role;

        Requester(boost::uuids::uuid uuid, boost::json::string &role) : uuid(uuid), role(role) {}
};

} // namespace leaper::http

#endif //LEAPER_REQUESTER_HPP
