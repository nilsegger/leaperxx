//
// Created by nils on 13/04/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_LISTENER_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_LISTENER_HPP

#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/asio/ssl.hpp>

#include "leaper/http/sessions/detect_session.hpp"
#include "leaper/sql/client.hpp"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

namespace leaper::http {

class Listener  : public std::enable_shared_from_this<Listener> {
    tcp::acceptor acceptor_;
    void do_accept();
    void on_accept(beast::error_code ec, tcp::socket socket);

protected:
    net::io_context& ioc_;
    net::ssl::context& ctx_;
    connection_pool_t * pool_;

    virtual std::shared_ptr<DetectSession> create_session(tcp::socket& socket) = 0;
public:
    Listener(beast::error_code& ec, net::ssl::context& ctx, net::io_context& ioc, tcp::endpoint endpoint, connection_pool_t * pool);
    void run();

};

} // namespace leaper::http

#endif // LEAPER_LIBS_HTTP_INCLUDE_HTTP_LISTENER_HPP
