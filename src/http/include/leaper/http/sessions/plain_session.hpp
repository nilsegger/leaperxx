#ifndef LEAPER_SRC_HTTP_INCLUDE_SESSIONS_PLAIN_SESSION_HPP
#define LEAPER_SRC_HTTP_INCLUDE_SESSIONS_PLAIN_SESSION_HPP

#include "leaper/http/sessions/session.hpp"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

namespace leaper::http {

class PlainSession : public Session<PlainSession>,  public std::enable_shared_from_this<PlainSession> {

    beast::tcp_stream stream_;

public:

    PlainSession(net::io_context &ioc, beast::tcp_stream&& stream, beast::flat_buffer&& buffer, 
    			connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port);

	beast::tcp_stream& stream();

	void run();

	void do_close();

	virtual void route_request() = 0;
};

} // namespace leaper::http

#endif //LEAPER_SRC_HTTP_INCLUDE_SESSIONS_PLAIN_SESSION_HPP
