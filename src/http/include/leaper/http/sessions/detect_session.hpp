#ifndef SRC_HTTP_INCLUDE_HTTP_SESSIONS_DETECT_SESSION_HPP
#define SRC_HTTP_INCLUDE_HTTP_SESSIONS_DETECT_SESSION_HPP

#include <memory>

#include <boost/asio/ssl.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/beast.hpp>

#include "leaper/http/sessions/ssl_session.hpp"
#include "leaper/http/sessions/plain_session.hpp"

namespace beast = boost::beast;
namespace ssl = boost::asio::ssl;
namespace net = boost::asio;
using tcp = boost::asio::ip::tcp; 

namespace leaper::http {

class DetectSession : public std::enable_shared_from_this<DetectSession> {

protected:
	beast::tcp_stream stream_;
	ssl::context& ctx_;
	beast::flat_buffer buffer_;
	net::io_context& ioc_;
	connection_pool_t* pool_;
	net::ip::address remote_ip_;
	unsigned int remote_port_;

public:
	explicit DetectSession(net::io_context &ioc, net::ssl::context& ctx, tcp::socket &&socket,
				connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port);

	void run();

	void on_run();

	void on_detect(beast::error_code ec, bool result);

	virtual void create_ssl_session() = 0;

	virtual void create_plain_session() = 0;
};

} // namespace leaper::http

#endif // SRC_HTTP_INCLUDE_HTTP_SESSIONS_DETECT_SESSION_HPP
