#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_SESSION_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_SESSION_HPP

#include <iostream>
#include <fstream>
#include <string>

#include <boost/bind/bind.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/http/parser.hpp>
#include <boost/asio/dispatch.hpp>
#include <boost/asio/strand.hpp>
#include <boost/asio/ssl.hpp>
#include <boost/config.hpp>

#include "leaper/sql/client.hpp"
#include "leaper/definitions.hpp"
#include "leaper/http/resources/resource.hpp"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

namespace leaper::http {

template<class Derived>
class Session {

	Derived& derived() {
		return static_cast<Derived&>(*this);
	}
	
    struct SendLambda {
        std::shared_ptr<Derived> self_;

        explicit
        SendLambda(std::shared_ptr<Derived> self)
                : self_(self) {}

        template<class Body, class Fields>
        void operator()(beast::http::message<false, Body, Fields> &&msg) const {
            // The lifetime of the message has to extend
            // for the duration of the async operation so
            // we use a shared_ptr to manage it.
            auto sp = std::make_shared<
                    beast::http::message<false, Body, Fields>>(std::move(msg));

            // Store a type-erased version of the shared
            // pointer in the class to keep it alive.
            self_->res_ = sp;

            // Write the response
            beast::http::async_write(
                    self_->stream(),
                    *sp,
                    beast::bind_front_handler(
                            &Session::on_write,
                            self_,
                            sp->need_eof()));
        }
    };


protected:
	beast::flat_buffer buffer_;
    std::shared_ptr<void> res_;
    std::unique_ptr<SendLambda> send_;
    bool close_ = false;

    net::io_context &ioc_;
    connection_pool_t *pool_;
    net::ip::address remote_ip_;
    unsigned short remote_port_;

    std::optional<beast::http::request_parser<beast::http::empty_body>> parser_;

public:

    Session(net::io_context &ioc, connection_pool_t *pool, net::ip::address remote_ip, unsigned short remote_port, beast::flat_buffer buffer)
    	: ioc_(ioc), pool_(pool), remote_ip_(remote_ip), remote_port_(remote_port), buffer_(buffer) {}


    void do_read() {
	parser_.emplace();
	parser_->header_limit(4000);

	beast::get_lowest_layer(derived().stream()).expires_after(std::chrono::seconds(30));

    	beast::http::async_read_header(derived().stream(), buffer_, parser_.value(), beast::bind_front_handler(&Session::on_header_read, derived().shared_from_this()));
    }

    void on_header_read(beast::error_code ec, std::size_t bytes_transferred) {
	boost::ignore_unused(bytes_transferred);

	if(ec == beast::http::error::end_of_stream) return derived().do_close();
	else if(ec == beast::http::error::header_limit) {
		return send_fatal_response(beast::http::status::request_header_fields_too_large, "Header size exceeds maximum.");
	}
	else if(ec) return;

	derived().route_request();
    }

    void on_write(bool close, beast::error_code ec, std::size_t bytes_transferred) {

		boost::ignore_unused(bytes_transferred);

		if (ec) return;

		if (close || close_) {
		// This means we should close the connection, usually because
		// the response indicated the "Connection: close" semantic.
		// if close_ is true, send_fatal_response has been called.
			return derived().do_close();
		}

		// We're done with the response so delete it
		res_ = nullptr;

		// Read another request
		do_read();
	}

    void on_return(bool close) {
	if(close) {
        	return derived().do_close();
    	}
    	do_read();
    }
	
protected:

    template<typename Body, typename Fields>
    void handle_response(bool close, std::optional<beast::http::response<Body, Fields>> &response, std::optional<Log> & log) {
        if(close && response) {
            close_ = true;
        } else if(close && !response) {
            return derived().do_close();
        }
        std::cout << log.value() << std::endl;
        send_ = std::make_unique<SendLambda>(derived().shared_from_this());
        (*send_)(std::move(response.value()));
        send_.reset();
    }

public:
    template<typename T>
    std::shared_ptr<T> instantiate(Parameters& pathParameters, Parameters& queryParameters) {
    	std::shared_ptr<T> ptr = std::make_shared<T>(boost::bind(&Session::on_return, derived().shared_from_this(), boost::placeholders::_1), ioc_, pool_, &derived().stream(), parser_.value(), &buffer_, remote_ip_, remote_port_, pathParameters, queryParameters);
        ptr->on_request();
	return ptr;
    }

	void send_fatal_response(beast::http::status status, string_t message) {
	    close_ = true;
	    send_error_response(status, std::move(message));
	}

	void send_error_response(beast::http::status status, string_t message) {
	    if(!send_) {
	      send_ = std::make_unique<SendLambda>(derived().shared_from_this());
	    }

	    if(parser_->is_header_done()) {
		std::optional<Log> log = Log{remote_ip_, remote_port_, string_t("-"), util::chrono::now(), parser_->get().method(), parser_->get().target(), parser_->get().version(), int(status), 0, 0};
		std::optional<beast::http::response<beast::http::string_body>> resp = beast::http::response<beast::http::string_body>{status, parser_->get().version()};
		resp->body() = message.c_str();
		resp->prepare_payload();
		handle_response(false, resp, log);
	    } else {
		std::optional<Log> log = Log{remote_ip_, remote_port_, string_t("-"), util::chrono::now(), beast::http::verb::unknown, "unknown", 0, int(status), 0, 0};
		std::optional<beast::http::response<beast::http::string_body>> resp = beast::http::response<beast::http::string_body>{status, 11};
		resp->body() = message.c_str();
		resp->prepare_payload();
		handle_response(false, resp, log);
	    }
	}
    
};

} // namespace leaper::http

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_SESSION_HPP
