#ifndef LEAPER_SRC_HTTP_INCLUDE_SESSIONS_SSL_SESSION_HPP
#define LEAPER_SRC_HTTP_INCLUDE_SESSIONS_SSL_SESSION_HPP

#include "leaper/http/sessions/session.hpp"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace http = beast::http;           // from <boost/beast/http.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

namespace leaper::http {

class SSLSession : public Session<SSLSession>,  public std::enable_shared_from_this<SSLSession> {

    beast::ssl_stream<beast::tcp_stream> stream_;

public:

    SSLSession(net::io_context &ioc, net::ssl::context& ctx, beast::tcp_stream&& stream, 
    			beast::flat_buffer&& buffer, connection_pool_t *pool, net::ip::address remote_ip,
			unsigned short remote_port);

	beast::ssl_stream<beast::tcp_stream>& stream();

	void run();

	void on_handshake(beast::error_code ec, std::size_t bytes_used);

	void do_close();

    	void on_shutdown(beast::error_code ec);

    	virtual void route_request() = 0;
};

} // namespace leaper::http


#endif //LEAPER_SRC_HTTP_INCLUDE_SESSIONS_SSL_SESSION_HPP
