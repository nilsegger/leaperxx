//
// Created by nils on 29/05/2020.
//

#ifndef LEAPER_LOG_HPP
#define LEAPER_LOG_HPP

#include <chrono>

#include <boost/asio.hpp>
#include <boost/beast/core.hpp>
#include <boost/beast/http.hpp>

#include "leaper/definitions.hpp"
#include "leaper/util/uuid.hpp"
#include "leaper/util/chrono.hpp"

namespace beast = boost::beast;         // from <boost/beast.hpp>
namespace net = boost::asio;            // from <boost/asio.hpp>
using tcp = boost::asio::ip::tcp;       // from <boost/asio/ip/tcp.hpp>

namespace leaper::http {

struct Log {
    boost::asio::ip::address ip;
    unsigned short port;
    string_t user_id;
    std::chrono::system_clock::time_point time;
    beast::http::verb method;
    string_t url;
    unsigned int http_version;
    unsigned int status;
    std::uint64_t size;
    unsigned long long elapsed_ms;

    Log(const boost::asio::ip::address ip, const unsigned short port, string_t user_id,
    	const std::chrono::system_clock::time_point time, const beast::http::verb method, const string_t url,
	unsigned int http_version, int status, std::uint64_t size, unsigned long long elapsed_ms);
};

} // namespace leaper::http

std::ostream & operator << (std::ostream &out, const leaper::http::Log &log);

#endif //LEAPER_LOG_HPP

