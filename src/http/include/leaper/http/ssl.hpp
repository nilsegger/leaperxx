//
// Created by nils on 05/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_SSL_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_SSL_HPP

#include <boost/system/system_error.hpp>
#include <boost/asio/ssl.hpp>

#include "leaper/definitions.hpp"

namespace leaper::http {

void load_server_certificate(boost::system::error_code& ec, boost::asio::ssl::context& ctx, string_t password, string_t certificate_file, string_t key_file, string_t dh_file);

} // namespace leaper::http

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_SSL_HPP
