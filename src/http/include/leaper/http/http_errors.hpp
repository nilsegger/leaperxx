#ifndef LEAPER_LIBS_ERRORS_INCLUDE_HTTP_ERRORS_HPP
#define LEAPER_LIBS_ERRORS_INCLUDE_HTPP_ERRORS_HPP

#include <system_error>

enum class HTTPErrors {
    SUCCESS = 0,

    // Unauthorized = 1000

    // Bad Request
    BODY_EMPTY=2000,
    MISSING_BODY_KEY,
    MISSING_HEADER,
    INVALID_METHOD,
    BODY_LIMIT,
    PARAMETER_WRONG_TYPE,
    MISSING_PARAMETER,
    INVALID_PARAMETER,
    INVALID_BEARER,

    // Not Found=3000

    // Conflict
    DUPLICATE=4000,

    // Forbidden
    FORBIDDEN=5000,

    // Unsupported media
    UNSUPPORTED_MEDIA=6000
};

namespace std {
    template<>
    struct is_error_code_enum<HTTPErrors> : true_type {};
}

namespace {
    struct HTTPErrorsCategory : std::error_category {
        const char* name() const noexcept override {
            return "HTTPErrors";
        }

        std::string message(int e) const override {
            switch (static_cast<HTTPErrors>(e)) {
                case HTTPErrors::SUCCESS:
                    return "There was no error but it was handled???";
                case HTTPErrors::BODY_EMPTY:
                    return "Missing body in request.";
                case HTTPErrors::MISSING_BODY_KEY:
                    return "Missing key in body.";
                case HTTPErrors::MISSING_HEADER:
                    return "Missing header.";
                case HTTPErrors::INVALID_METHOD:
                    return "HTTP method not allowed on this resource.";
                case HTTPErrors::FORBIDDEN:
                    return "HTTP Forbidden";
                case HTTPErrors::DUPLICATE:
                    return "Duplicate resource.";
                case HTTPErrors::BODY_LIMIT:
                    return "body limit";
		case HTTPErrors::PARAMETER_WRONG_TYPE:
                    return "Parameter is not of expected type.";
                case HTTPErrors::MISSING_PARAMETER:
                    return "Missing parameter.";
                case HTTPErrors::INVALID_PARAMETER:
			return "Parameter is invalid.";
                case HTTPErrors::INVALID_BEARER:
			return "Authorization bearer is invalid.";
                case HTTPErrors::UNSUPPORTED_MEDIA:
                    return "Unsupported media type.";
                default:
                    return "Unknown error code received. '" + std::to_string(e) + "'";
            }
        }
    };

    const HTTPErrorsCategory http_codes_categories {};

    void fail(std::error_code& e, HTTPErrors c){
        e = c;
    }

}

inline std::error_code make_error_code(HTTPErrors e) {
    return {static_cast<int>(e), http_codes_categories};
}

#endif //LEAPER_LIBS_ERRORS_INCLUDE_HTTP_ERRORS_HPP


