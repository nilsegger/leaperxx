//
// Created by nils on 08/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESPONSE_BUILDER_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESPONSE_BUILDER_HPP

#include <boost/beast/http.hpp>
#include <boost/json.hpp>

#include "leaper/definitions.hpp"

namespace beast = boost::beast;
namespace json = boost::json;

namespace leaper::http::response {

    beast::http::response<beast::http::empty_body> build_successful(unsigned version) ;

    beast::http::response<beast::http::string_body> build_successful(unsigned version, string_t message);

    beast::http::response<beast::http::empty_body> build_error(unsigned version, beast::http::status status) ;

    beast::http::response<beast::http::string_body> build_error(unsigned version, beast::http::status status, string_t message);

    beast::http::response<beast::http::empty_body> build_not_found(unsigned version) ;

    beast::http::response<beast::http::empty_body> build_method_not_allowed(unsigned version) ;

    beast::http::response<beast::http::empty_body> build_unauthorized(unsigned version) ;

    beast::http::response<beast::http::string_body> build_json(unsigned version, beast::http::status status, json::object& json);

    beast::http::response<beast::http::string_body> build_json(unsigned version, beast::http::status status, json::array& json);

}; // namespace leaper::http::response

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESPONSE_BUILDER_HPP
