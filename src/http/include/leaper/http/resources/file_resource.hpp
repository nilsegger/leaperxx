//
// Created by nils on 09/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_FILE_RESOURCE_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_FILE_RESOURCE_HPP

#include "leaper/http/resources/authenticated_resource.hpp"

namespace beast = boost::beast;

namespace leaper::http {
class FileResource : public AuthenticatedResource {

protected:
    std::optional<beast::http::request_parser<beast::http::file_body>> parser_;

private:
    beast::error_code file_ec_;

    template<typename CALLBACK_T>
    void do_save(string_t file_name, CALLBACK_T callback) {
        parser_.emplace(std::move(this->header_parser_));
        parser_->body_limit(1000000); // 200kb

	parser_->get().body().open(file_name.c_str(), boost::beast::file_mode::write, file_ec_);
		INFO << "Saving file to " << file_name.c_str() << std::endl;	
        if(file_ec_) {
		ERROR << "Unable to open " << file_name.c_str() << " for saving" << std::endl;	
            fail(this->ec_, IOErrors::FILE_SAVE_ERROR);
            return this->handle_response();
        }

	if(this->is_ssl_stream_) {
		beast::get_lowest_layer(*this->ssl_stream_.value()).expires_after(std::chrono::seconds(10));
        	beast::http::async_read(*this->ssl_stream_.value(), *this->buffer_, parser_.value(), beast::bind_front_handler(&FileResource::on_save<CALLBACK_T>, this, callback));
	} else {
		beast::get_lowest_layer(*this->plain_stream_.value()).expires_after(std::chrono::seconds(10));
	        beast::http::async_read(*this->plain_stream_.value(), *this->buffer_, parser_.value(), beast::bind_front_handler(&FileResource::on_save<CALLBACK_T>, this, callback));
	}

        
    }

    template<typename CALLBACK_T>
    void on_save(CALLBACK_T callback, beast::error_code ec, std::size_t bytes_transferred) {

        boost::ignore_unused(bytes_transferred);

        if (ec == beast::http::error::body_limit) {
		ERROR << "Failed to save file because it is too large" << std::endl;
            return fail(this->ec_, HTTPErrors::BODY_LIMIT);
        } else if (ec) return;

	INFO << "Successfully saved file" << std::endl;

        callback();
    }


protected:

    beast::http::verb method() override;

    string_t target() override;

    unsigned int version() override;

    template<typename CALLBACK_T>
    void save(string_t file_name, CALLBACK_T callback) {
        do_save(file_name, callback);
    }

public:

    using AuthenticatedResource::AuthenticatedResource;

};

} // namespace leaper::http

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_FILE_RESOURCE_HPP
