//
// Created by nils on 08/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_AUTHENTICATED_RESOURCE_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_AUTHENTICATED_RESOURCE_HPP

#include <memory>

#include <boost/beast/core.hpp>

#include "leaper/definitions.hpp"
#include "leaper/http/resources/method_routed_resource.hpp"
#include "leaper/http/requester.hpp"

namespace leaper::http {
class AuthenticatedResource : public MethodResource {

    bool parse_token(string_t &token);

    bool validate_token();
protected:

    bool authenticated_ = false;
    std::unique_ptr<Requester> requester_;

    virtual void do_authenticated_get();

    virtual void do_authenticated_post();

    virtual void do_authenticated_put();

    virtual void do_authenticated_delete();

    virtual void build_authenticated_get_response();

    virtual void build_authenticated_post_response();

    virtual void build_authenticated_put_response();

    virtual void build_authenticated_delete_response();

    void build_get_response() override;

    void build_post_response() override;

    void build_put_response() override;

    void build_delete_response() override;

    void build_response() override;

    string_t user_id() override;

    virtual void after_authentication();

    void authenticated_method_route();

public:

    using MethodResource::MethodResource;

    void on_request() override;
};

} // namespace leaper::http
#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_AUTHENTICATED_RESOURCE_HPP
