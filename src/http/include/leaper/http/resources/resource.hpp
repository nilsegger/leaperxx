//
// Created by nils on 08/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_RESOURCE_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_RESOURCE_HPP

#include <chrono>
#include <memory>
#include <utility>

#include <boost/beast/core.hpp>
#include <boost/beast/version.hpp>
#include <boost/beast/http.hpp>
#include <boost/beast/ssl.hpp>
#include <boost/beast/http/parser.hpp>
#include <boost/asio/io_context.hpp>

#include "leaper/util/chrono.hpp"
#include "leaper/util/logger.hpp"
#include "leaper/definitions.hpp"
#include "leaper/sql/client.hpp"

#include "leaper/http/parameters.hpp"
#include "leaper/http/log.hpp"

namespace net = boost::asio;

namespace leaper::http {
class Resource : public std::enable_shared_from_this<Resource> {

protected:

    std::chrono::steady_clock::time_point begin_;

    std::error_code ec_;
    typedef util::queue::SharedQueue<Resource> QUEUE_T;
    std::unique_ptr<QUEUE_T> queue_;
    std::unique_ptr<connection_t> connection_;

    net::io_context &ioc_;
    connection_pool_t *pool_;
    net::ip::address remote_ip_;
    unsigned short remote_port_;

	bool is_ssl_stream_;
	std::optional<beast::ssl_stream<beast::tcp_stream>*> ssl_stream_;
	std::optional<beast::tcp_stream*> plain_stream_;

    beast::http::request_parser<beast::http::empty_body> &header_parser_;
    beast::flat_buffer *buffer_;
    std::shared_ptr<void> response_;
    std::function<void(bool close)> callback_;

    Parameters path_parameters_;
    Parameters query_parameters_;

	string_t cache_control_ = "no-store";

    bool contains_header(boost::beast::string_view key);

    void get_header(boost::beast::string_view key, string_t& value);

    virtual void prepare(unsigned int callbacks);

    virtual void prepare_queue(unsigned int callbacks);

    virtual void prepare_connection();

    void queue_item_finished();

    virtual beast::http::verb method();

    virtual string_t target();

    virtual unsigned int version();

    virtual string_t user_id();

    template<typename ResponseBody>
    Log build_log(beast::http::response<ResponseBody> &response) {
        return Log(remote_ip_, remote_port_, user_id(), util::chrono::now(), method(),
                   target(), version(), response.result_int(),
                   response.payload_size().value_or(0), elapsed_milliseconds());
    }

    template<typename ResponseBody, typename Fields>
    void prepare_response(beast::http::response<ResponseBody, Fields>& response) {
        response.set(beast::http::field::server, BOOST_BEAST_VERSION_STRING);
	response.keep_alive(header_parser_.get().keep_alive());
	response.set(beast::http::field::access_control_allow_origin, "*");
	response.set(beast::http::field::access_control_allow_methods, "OPTIONS, GET, POST, PUT, DELETE");
	response.set(beast::http::field::access_control_allow_headers, "content-type, content-length, authorization");
	response.set(beast::http::field::cache_control, cache_control_);

        response.prepare_payload();
    }

    void handle_response();

    template<typename Body, typename Fields>
    void do_write(beast::http::response<Body, Fields> &&resp) {

        std::cout << build_log(resp) << std::endl;

        prepare_response(resp);

        // The lifetime of the message has to extend
        // for the duration of the async operation so
        // we use a shared_ptr to manage it.
        auto sp = std::make_shared<beast::http::message<false, Body, Fields>>(std::move(resp));

        // Store a type-erased version of the shared
        // pointer in the class to keep it alive.
        response_ = sp;

	if(is_ssl_stream_) {
		INFO << "Writing ssl response" << std::endl;
        	beast::http::async_write(*ssl_stream_.value(), *sp, beast::bind_front_handler(&Resource::on_write, this->shared_from_this(), sp->need_eof()));
	} else {
		INFO << "Writing plain response" << std::endl;
        	beast::http::async_write(*plain_stream_.value(), *sp, beast::bind_front_handler(&Resource::on_write, this->shared_from_this(), sp->need_eof()));
	}

        queue_ = nullptr;
    }

    void on_write(bool close, beast::error_code ec, std::size_t bytes_transferred);

    void abort();

    virtual void build_response();

    beast::http::response<beast::http::string_body> build_error_response();

    unsigned long long elapsed_milliseconds();

public:

    Resource(std::function<void(bool close)> callback, net::io_context &ioc, connection_pool_t *pool,
             beast::tcp_stream* stream, beast::http::request_parser<beast::http::empty_body> &header_parser, beast::flat_buffer *buffer, net::ip::address remote_ip, unsigned short remote_port,
             Parameters &pathParameters, Parameters &queryParameters);

	Resource(std::function<void(bool close)> callback, net::io_context &ioc, connection_pool_t *pool,
             beast::ssl_stream<beast::tcp_stream>* stream, beast::http::request_parser<beast::http::empty_body> &header_parser, beast::flat_buffer *buffer, net::ip::address remote_ip, unsigned short remote_port,
             Parameters &pathParameters, Parameters &queryParameters);


    virtual void on_request();

    void set_cache_control(string_t cache_control);
};
} // namespace leaper::http

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_RESOURCE_HPP
