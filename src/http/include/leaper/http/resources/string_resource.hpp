//
// Created by nils on 08/06/2020.
//

#ifndef LEAPER_LIBS_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_STRING_RESOURCE_HPP
#define LEAPER_LIBS_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_STRING_RESOURCE_HPP

#include <boost/beast/http.hpp>
#include <boost/json.hpp>

#include "leaper/http/resources/authenticated_resource.hpp"

namespace beast = boost::beast;

namespace leaper::http {

class StringResource : public AuthenticatedResource {

    std::optional<beast::http::request_parser<beast::http::string_body>> parser_;

    template<typename CALLBACK_T>
    void do_read(CALLBACK_T callback) {
        parser_.emplace(std::move(this->header_parser_));
        parser_->body_limit(200000); // 200kb
	INFO << "Reading string content of request" << std::endl;
	if(this->is_ssl_stream_) {
		beast::get_lowest_layer(*this->ssl_stream_.value()).expires_after(std::chrono::seconds(10));
		beast::http::async_read(*this->ssl_stream_.value(), *this->buffer_, parser_.value(),
				 beast::bind_front_handler(&StringResource::on_read<CALLBACK_T>, this, callback));
	} else {
		beast::get_lowest_layer(*this->plain_stream_.value()).expires_after(std::chrono::seconds(10));
		beast::http::async_read(*this->plain_stream_.value(), *this->buffer_, parser_.value(),
				 beast::bind_front_handler(&StringResource::on_read<CALLBACK_T>, this, callback));

	}
    }

    template<typename CALLBACK_T>
    void on_read(CALLBACK_T callback, beast::error_code ec, std::size_t bytes_transferred) {

	INFO << "Request string content read" << std::endl;
        boost::ignore_unused(bytes_transferred);

        if (ec == beast::http::error::body_limit) {
		INFO << "Request body content is greater than allowed limit" << std::endl;
            return fail(this->ec_, HTTPErrors::BODY_LIMIT);
        } else if (ec) return abort();

        payload_ = std::move(parser_->get());

        callback();
    }

protected:

    std::optional<beast::http::request<beast::http::string_body>> payload_;

    template<typename CALLBACK_T>
    void read(CALLBACK_T callback) {
        do_read(callback);
    }

    beast::http::verb method() override;

    string_t target() override;

    unsigned int version() override;

    bool get_json(boost::json::object& body);

public:

    using AuthenticatedResource::AuthenticatedResource;

};

} // leaper::http

#endif //LEAPER_LIBS_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_STRING_RESOURCE_HPP
