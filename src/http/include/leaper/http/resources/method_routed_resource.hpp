//
// Created by nils on 08/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_METHOD_ROUTED_RESOURCE_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_METHOD_ROUTED_RESOURCE_HPP

#include <boost/beast/http.hpp>

#include "leaper/http/resources/resource.hpp"

namespace leaper::http {
class MethodResource : public Resource {

public:

    using Resource::Resource;

    void on_request() override;

    void build_method_not_allowed();

protected:

    virtual void do_options();

    virtual void do_get();

    virtual void do_post();

    virtual void do_put();

    virtual void do_delete();

    virtual void build_options_response();

    virtual void build_get_response();

    virtual void build_post_response();

    virtual void build_put_response();

    virtual void build_delete_response();

    void build_response() override;
};
} // namespace leaper::http

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_RESOURCES_METHOD_ROUTED_RESOURCE_HPP
