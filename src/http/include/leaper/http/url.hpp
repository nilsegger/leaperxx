//
// Created by nils on 07/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_URL_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_URL_HPP

#include "leaper/http/parameters.hpp"
#include "leaper/definitions.hpp"

namespace leaper::http {
std::pair<string_t, Parameters> parse_url(string_t target);
} // namespace leaper::http

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_URL_HPP
