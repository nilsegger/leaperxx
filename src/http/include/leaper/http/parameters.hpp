//
// Created by nils on 04/06/2020.
//

#ifndef LEAPER_LIBS_HTTP_INCLUDE_HTTP_PARAMETERS_HPP
#define LEAPER_LIBS_HTTP_INCLUDE_HTTP_PARAMETERS_HPP

#include <vector>

#include "leaper/http/http_errors.hpp"
#include "leaper/definitions.hpp"
#include "leaper/util/uuid.hpp"

namespace leaper::http {
class Parameters {
	std::vector<std::pair<string_t, string_t>> parameters_;

public:

	void add(string_t& name, string_t& value);

	string_t find(string_t name, string_t default_="");

	string_t get(std::error_code&ec, string_t& name);

	string_t get_string(std::error_code& ec, string_t name); 

	uuid_t get_uuid(std::error_code& ec, string_t name);

	int get_int(std::error_code& ec, string_t name);

	void clear();
};
} // namespace leaper::http

#endif //LEAPER_LIBS_HTTP_INCLUDE_HTTP_PARAMETERS_HPP
