project(leaper_http VERSION 1.0)

add_library(${PROJECT_NAME} 
	src/listener.cpp
	src/log.cpp
	src/ssl.cpp
	src/url.cpp
	src/parameters.cpp
	src/response_builder.cpp
	src/resources/resource.cpp
	src/resources/method_routed_resource.cpp
	src/resources/authenticated_resource.cpp
	src/resources/file_resource.cpp
	src/resources/string_resource.cpp
	src/sessions/plain_session.cpp
	src/sessions/ssl_session.cpp
	src/sessions/detect_session.cpp
)

add_library(leaper::http ALIAS ${PROJECT_NAME})

target_link_libraries(${PROJECT_NAME} leaper::util leaper::auth leaper::config)

