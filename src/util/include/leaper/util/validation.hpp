//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_VALIDATION_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_VALIDATION_HPP

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"
#include "leaper/util/chrono.hpp"

namespace leaper::util {
    namespace validation {
        void check_str(std::error_code &ec, string_t &str, bool must_be_nonempty, int min_length, int max_length);
        void validate_country(std::error_code &ec, string_t &country);
        void validate_gender(std::error_code &ec, string_t &gender);
        void check_age(std::error_code &ec, std::chrono::system_clock::time_point &time_point, long long min_age_s);
    }; // namespace validation
}

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_VALIDATION_HPP
