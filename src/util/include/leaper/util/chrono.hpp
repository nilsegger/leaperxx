//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_CHRONO_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_CHRONO_HPP

#include <chrono>

#include "leaper/definitions.hpp"

namespace leaper::util {

    namespace chrono {
        std::chrono::system_clock::time_point now();
        unsigned long long seconds_since_epoch();
        unsigned long long to_seconds(std::chrono::system_clock::time_point& time_point);
        string_t to_string(const std::chrono::system_clock::time_point& time_point);
    } // namespace chrono

} // namepsace util

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_CHRONO_HPP
