//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_FILE_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_FILE_HPP

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"

namespace leaper::util {

    namespace file {
        void read_as_string(string_t &file_name, string_t &result, std::error_code &ec);
    } // namespace file

} // namespace util

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_FILE_HPP
