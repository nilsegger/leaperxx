//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_BASE64_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_BASE64_HPP

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"
#include "leaper/util/string.hpp"

namespace leaper::util {

    namespace base64 {
        void encode(std::error_code& ec, const unsigned char *input, size_t input_length, char **buffer, size_t *buffer_length);
        void encode(std::error_code& ec, const unsigned char *input, size_t input_length, string_t& output);
        void encode_url_safe(std::error_code& ec, const unsigned char *input, size_t input_length, char **buffer, size_t *buffer_length);
        string_t encode_url_safe(std::error_code& ec, const unsigned char *input, size_t input_length);
        size_t decode_length(const char* input);
        void decode(std::error_code& ec, const char* encoded, unsigned char** buffer, size_t* length);
        void decode_url_safe(std::error_code& ec, const char* encoded, size_t encoded_length, unsigned char** buffer, size_t* buffer_length);
        string_t decode_url_safe(std::error_code& ec, const char* encoded, size_t encoded_length);
    } // namespace base64

} // namespace util

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_BASE64_HPP
