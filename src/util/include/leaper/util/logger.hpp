#ifndef SRC_UTIL_INCLUDE_UTIL_LOGGER_HPP
#define SRC_UTIL_INCLUDE_UTIL_LOGGER_HPP

#define DEBUG leaper::util::debug << leaper::util::logger::Logger::toRelativePath(__FILE__) << "@" << __LINE__ << ": "
#define INFO leaper::util::info << leaper::util::logger::Logger::toRelativePath(__FILE__) << "@" << __LINE__ << ": "
#define ERROR leaper::util::error << leaper::util::logger::Logger::toRelativePath(__FILE__) << "@" << __LINE__ << ": "

#include <iostream>

#include "leaper/definitions.hpp"

namespace leaper::util {
	
	namespace logger {

		class Logger : public std::streambuf {
			
			string_t prefix_;
			bool is_active_ = true;
			bool is_beginning_ = true;
			std::streambuf* buffer_;

			int overflow(int c) override;
			int sync() override;

		public:
			Logger(string_t prefix, std::streambuf* buffer);
			void set_active(bool flag);
			static string_t toRelativePath(string_t filePath);
		};

		static Logger info("I ", std::cout.rdbuf());
		static Logger debug("D ", std::cout.rdbuf());
		static Logger error("E ", std::cerr.rdbuf());

	} // namespace logger	

	static std::ostream info(&logger::info);
	static std::ostream debug(&logger::debug);
	static std::ostream error(&logger::error);

} // namespace util

#endif // SRC_UTIL_INCLUDE_UTIL_LOGGER_HPP
