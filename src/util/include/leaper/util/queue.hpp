//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_QUEUE_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_QUEUE_HPP

#include <assert.h>
#include <memory>

namespace leaper::util {

    namespace queue {

        class Queue {
            const unsigned int expected_callbacks_;
            unsigned int actual_callbacks_ = 0;
            bool finalized_ = false;
        protected:
            virtual void callback() = 0;

        public:
            Queue(unsigned int expected_callbacks);

            void done();

            void finalize();

            bool is_finalized();
        };

        template<class CALLBACK_C>
        class SharedQueue : public util::queue::Queue {

            void (CALLBACK_C::* callback_member_)();

            std::shared_ptr <CALLBACK_C> callback_instance_;
        protected:
            void callback() override {
                (callback_instance_.get()->*callback_member_)();
            }

        public:
            SharedQueue(void(CALLBACK_C::* callback_member)(), std::shared_ptr <CALLBACK_C> callback_instance, unsigned int expected_callbacks)
                    : callback_member_(callback_member), callback_instance_(callback_instance), Queue::Queue(expected_callbacks) {
            };

        };

        template<class CALLBACK_C>
        class MemberQueue : public util::queue::Queue {

            void (CALLBACK_C::* callback_member_)();

            CALLBACK_C *callback_instance_;
        protected:
            void callback() override {
                (callback_instance_->*callback_member_)();
            }

        public:
            MemberQueue(void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance, unsigned int expected_callbacks)
                    : callback_member_(callback_member), callback_instance_(callback_instance), Queue::Queue(expected_callbacks) {
            };

        };
    } // namespace queue

}// namespace util

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_QUEUE_HPP
