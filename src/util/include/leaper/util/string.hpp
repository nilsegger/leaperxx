//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_STRING_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_STRING_HPP

#include "leaper/definitions.hpp"

namespace leaper::util {
    namespace string {
        string_t convert_from_std_string(std::string& str);
        std::string convert_to_std_string(string_t& str);
        void replace_all(char * arr, char old, char new_);
        void replace_all(char * arr, size_t& size, char old, char new_);
        void replace_all(string_t& str, char old, char const * new_);
        void safe_url(string_t& str);
    } // namespace string
} // namespace util

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_STRING_HPP
