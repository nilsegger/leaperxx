//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_UUID_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_UUID_HPP

#include <boost/uuid/uuid.hpp>
/* Header must be included here, otherwise operator<< for uuid wont be found. */
#include <boost/uuid/uuid_io.hpp>

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"
#include "leaper/util/string.hpp"

namespace leaper::util {

    namespace uuid {
        boost::uuids::uuid create();

        boost::uuids::uuid nil();

        string_t to_string(boost::uuids::uuid uuid);

        boost::uuids::uuid from_string(std::error_code &ec, string_t &str);
    }; // namespace uuid
} // namespace util

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_UUID_HPP
