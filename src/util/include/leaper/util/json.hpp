//
// Created by nils on 12/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_JSON_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_JSON_HPP

#include <boost/json.hpp>

#include "leaper/definitions.hpp"
#include "leaper/json_errors.hpp"

namespace leaper::util {

    namespace json {

        template<boost::json::kind kind, typename STR_T>
        void parse_as(STR_T &str, boost::json::value &value, std::error_code &ec) {
            boost::json::error_code json_ec;
            value = boost::json::parse(str, json_ec);
            if (json_ec) {
                return fail(ec, JSONErrors::JSON_INVALID);
            } else if (value.kind() != kind) return fail(ec, JSONErrors::JSON_WRONG_TYPE);
        }

        template<typename STR_T>
        void parse_as_object(STR_T &str, boost::json::object &object, std::error_code &ec) {
            boost::json::value value;
            parse_as<boost::json::kind::object>(str, value, ec);
            if (ec) return;
            object = value.get_object();
        };

        void find_safe(boost::json::object &obj, string_t key, boost::json::value &val, std::error_code &ec);

        void find_optional(boost::json::object &obj, string_t key, std::optional <boost::json::value> &val);

        template<boost::json::kind kind>
        void find(boost::json::object &obj, string_t key, boost::json::value &value, std::error_code &ec) {
            find_safe(obj, key, value, ec);
            if (!ec) {
                if (value.kind() != kind) return fail(ec, JSONErrors::JSON_WRONG_TYPE);
            }
        }

        template<boost::json::kind kind>
        void find_optional(boost::json::object &obj, string_t key, std::optional <boost::json::value> &value, std::error_code &ec) {
            find_optional(obj, key, value);
            if (value) {
                if (value->kind() != kind) return fail(ec, JSONErrors::JSON_WRONG_TYPE);
            }
        }

        void find_string(boost::json::object &obj, string_t key, boost::json::string &value, std::error_code &ec);

        void find_string_optional(boost::json::object &obj, string_t key, std::optional <boost::json::string> &value, std::error_code &ec);

        void find_int32(boost::json::object &obj, string_t key, int32_t &value, std::error_code &ec);

        void find_int64_optional(boost::json::object &obj, string_t key, std::optional <int64_t> &value, std::error_code &ec);

        void find_object_optional(boost::json::object &obj, string_t key, std::optional <boost::json::object> &value, std::error_code &ec);

        void find_timestamp_optional(boost::json::object &obj, string_t key, std::optional <std::chrono::system_clock::time_point> &value, std::error_code &ec);

        void add_string_if_not_empty(boost::json::object &obj, string_t key, string_t &value);

	void to_string(std::error_code&ec, string_t& result, boost::json::value* val);
	void to_uint64(std::error_code&ec, std::uint64_t& result, boost::json::value* val);
    }; // namespace json

} // namespace util
#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_JSON_HPP
