//
// Created by nils on 16/06/2020.
//

#ifndef LEAPER_LIBS_UTIL_INCLUDE_UTIL_RANDOM_HPP
#define LEAPER_LIBS_UTIL_INCLUDE_UTIL_RANDOM_HPP

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"

namespace leaper::util {
    namespace random {

        void generate_secure_string(std::error_code&ec, size_t size, string_t& str);

    } // namespace random
} // namespace util

#endif //LEAPER_LIBS_UTIL_INCLUDE_UTIL_RANDOM_HPP
