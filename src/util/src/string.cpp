//
// Created by nils on 12/06/2020.
//

#include "leaper/util/string.hpp"

void leaper::util::string::replace_all(char *arr, size_t &size, char old, char new_) {
    for (size_t i = 0; i < size; i++, arr++) {
        if (*arr == old) *arr = new_;
    }
}

void leaper::util::string::replace_all(char *arr, char old, char new_) {
    for (; *arr != '\0'; arr++) {
        if (*arr == old) *arr = new_;
    }
}

void leaper::util::string::replace_all(string_t &str, char old, char const *new_) {
    for (std::string::size_type pos = str.find(old); pos != std::string::npos; pos = str.find(old, pos + 1)) {
        str.replace(pos, 1, new_);
    }
}

void leaper::util::string::safe_url(string_t &str) {
    replace_all(str, ' ', "%20");
    replace_all(str, '.', "%2E");
    replace_all(str, '$', "%24");
    replace_all(str, '&', "%26");
    replace_all(str, '`', "%60");
    replace_all(str, ':', "%3A");
    replace_all(str, '<', "%3C");
    replace_all(str, '>', "%3E");
    replace_all(str, '[', "%5B");
    replace_all(str, ']', "%5D");
    replace_all(str, ']', "%5D");
    replace_all(str, '{', "%7B");
    replace_all(str, '}', "%7D");
    replace_all(str, '"', "%22");
    replace_all(str, '+', "%2B");
    replace_all(str, '#', "%23");
    replace_all(str, '%', "%25");
    replace_all(str, '@', "%40");
    replace_all(str, '/', "%2F");
    replace_all(str, ';', "%3B");
    replace_all(str, '=', "%3D");
    replace_all(str, '?', "%3F");
    replace_all(str, '\\', "%5C");
    replace_all(str, '^', "%5E");
    replace_all(str, '|', "%7C");
    replace_all(str, '~', "%7E");
    replace_all(str, ',', "%2C");
}

string_t leaper::util::string::convert_from_std_string(std::string &str) {
    return string_t(str.c_str(), str.size());
}

std::string leaper::util::string::convert_to_std_string(string_t &str) {
    return std::string(str.c_str(), str.size());
}

