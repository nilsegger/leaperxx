//
// Created by nils on 12/06/2020.
//

#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/string_generator.hpp>
#include <boost/uuid/nil_generator.hpp>

#include "leaper/util/uuid.hpp"

boost::uuids::uuid leaper::util::uuid::create() {
    boost::uuids::basic_random_generator<boost::mt19937> gen;
    boost::uuids::uuid uuid = gen();
    return uuid;
}

boost::uuids::uuid leaper::util::uuid::nil() {
    return boost::uuids::nil_uuid();
}

string_t leaper::util::uuid::to_string(boost::uuids::uuid uuid) {
    std::string std_uuid = boost::uuids::to_string(uuid);
    return leaper::util::string::convert_from_std_string(std_uuid);
}

boost::uuids::uuid leaper::util::uuid::from_string(std::error_code &ec, string_t &str) {
    assert(!str.empty());
    if(str.empty()) {
        fail(ec, IOErrors::UUID_EMPTY);
        return nil();
    }

    try {
        boost::uuids::string_generator gen;
        return gen(leaper::util::string::convert_to_std_string(str));
    } catch (std::exception &exception) {
        fail(ec, IOErrors::UUID_INVALID);
        return nil();
    }

}
