//
// Created by nils on 12/06/2020.
//

#include "leaper/util/queue.hpp"

leaper::util::queue::Queue::Queue(unsigned int expected_callbacks) : expected_callbacks_(expected_callbacks) {
    assert(expected_callbacks > 0);
}

void leaper::util::queue::Queue::finalize() {
    assert(!finalized_);
    finalized_ = true;
    callback();
}

void leaper::util::queue::Queue::done() {
    actual_callbacks_++;
    assert(actual_callbacks_ <= expected_callbacks_);
    if (actual_callbacks_ == expected_callbacks_) {
        finalize();
    }
}

bool leaper::util::queue::Queue::is_finalized() {
    return finalized_;
}

