//
// Created by nils on 16/06/2020.
//

#include <openssl/rand.h>

#include "leaper/util/random.hpp"
#include "leaper/util/base64.hpp"

void leaper::util::random::generate_secure_string(std::error_code& ec, size_t size, string_t& str) {

    unsigned char buffer[size];
    int result = RAND_bytes(buffer, size);
    if (result == 0) {
        return fail(ec, IOErrors::OPENSSL_RAND_BYTES_ERROR);
    } else {
        str = leaper::util::base64::encode_url_safe(ec, buffer, size);
    }

}
