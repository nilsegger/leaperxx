//
// Created by nils on 12/06/2020.
//

#include <fstream>

#include "leaper/util/file.hpp"

void leaper::util::file::read_as_string(string_t &file_name, string_t &result, std::error_code &ec) {
    std::ifstream file(file_name.c_str(), std::ios::in);
    if (file.is_open()) {
        file.seekg(0, std::ios::end);
        size_t size = file.tellg();
        result = std::string(size, ' ');
        file.seekg(0);
        file.read(&result[0], size);
        file.close();
    } else {
        return fail(ec, IOErrors::FILE_NOT_FOUND);
    }
}
