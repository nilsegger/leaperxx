//
// Created by nils on 12/06/2020.
//

#include "leaper/util/json.hpp"


void leaper::util::json::find_safe(boost::json::object &obj, string_t key, boost::json::value &val, std::error_code &ec) {
    boost::json::key_value_pair *iter = obj.find(key);
    if (iter == obj.end()) return fail(ec, JSONErrors::JSON_KEY_MISSING);
    val = iter->value();
}

void leaper::util::json::find_optional(boost::json::object &obj, string_t key, std::optional<boost::json::value> &val) {
    boost::json::key_value_pair *iter = obj.find(key);
    if (iter != obj.end()) {
        val = iter->value();
    }
}

void
leaper::util::json::find_string(boost::json::object &obj, string_t key, boost::json::string &value, std::error_code &ec) {
    boost::json::value undetermined_value;
    find<boost::json::kind::string>(obj, key, undetermined_value, ec);
    if (ec) return;
    value = undetermined_value.get_string();
}

void
leaper::util::json::find_string_optional(boost::json::object &obj, string_t key, std::optional<boost::json::string> &value,
                                 std::error_code &ec) {
    std::optional<boost::json::value> tmp;
    find_optional<boost::json::kind::string>(obj, key, tmp, ec);
    if (!ec && tmp) {
        value = tmp->get_string();
    }
}

void leaper::util::json::find_int32(boost::json::object &obj, string_t key, int32_t &value, std::error_code &ec) {
    boost::json::value tmp;
    find<boost::json::kind::int64>(obj, key, tmp, ec);
    if (!ec) {
        value = (int32_t) tmp.get_int64();
    }
}

void leaper::util::json::find_int64_optional(boost::json::object &obj, string_t key, std::optional<int64_t> &value,
                                     std::error_code &ec) {
    std::optional<boost::json::value> tmp;
    find_optional<boost::json::kind::int64>(obj, key, tmp, ec);
    if (!ec && tmp) {
        value = tmp->get_int64();
    }
}

void
leaper::util::json::find_object_optional(boost::json::object &obj, string_t key, std::optional<boost::json::object> &value,
                                 std::error_code &ec) {
    std::optional<boost::json::value> tmp;
    find_optional<boost::json::kind::object>(obj, key, tmp, ec);
    if (!ec && tmp) {
        value = tmp->get_object();
    }
}

void leaper::util::json::find_timestamp_optional(boost::json::object& obj, string_t key, std::optional<std::chrono::system_clock::time_point>& value, std::error_code& ec) {
    std::optional<int64_t> timestamp;
    leaper::util::json::find_int64_optional(obj, key, timestamp, ec);
    if (timestamp) {
        value = std::chrono::system_clock::time_point(std::chrono::seconds(timestamp.value()));
    }
}

void leaper::util::json::add_string_if_not_empty(boost::json::object &obj, string_t key, string_t &value) {
    if (!value.empty()) obj.emplace(key, value);
}

void leaper::util::json::to_string(std::error_code& ec, string_t& result, boost::json::value* val) {
	string_t* str = val->is_string();
	if(str == nullptr) return fail(ec, JSONErrors::JSON_WRONG_TYPE);
	result = *str;
}

void leaper::util::json::to_uint64(std::error_code&ec, std::uint64_t& result, boost::json::value* val) {
	std::int64_t* conv = val->is_int64();
	if(conv == nullptr) return fail(ec, JSONErrors::JSON_WRONG_TYPE);
	result = std::uint64_t(*conv);

}
