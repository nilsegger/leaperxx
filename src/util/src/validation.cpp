//
// Created by nils on 12/06/2020.
//

#include "leaper/util/validation.hpp"

void leaper::util::validation::check_str(std::error_code &ec, string_t &str, bool must_be_nonempty, int min_length,
                                 int max_length) {
    if (str.empty() && !must_be_nonempty) {
        // Nothing to validate, string is empty and is allowed to be so
        return;
    } else if (str.empty() && must_be_nonempty) {
        return fail(ec, IOErrors::MODEL_VALIDATION_FAILED);
    } else if (str.size() < min_length || str.size() > max_length) {
        return fail(ec, IOErrors::MODEL_VALIDATION_FAILED);
    }
}

void leaper::util::validation::validate_country(std::error_code &ec, string_t &country) {
    if (country != "ch") {
        return fail(ec, IOErrors::MODEL_VALIDATION_FAILED);
    }
}

void leaper::util::validation::validate_gender(std::error_code &ec, string_t &gender) {
    if (gender != "male" && gender != "female" && gender != "n/a") {
        return fail(ec, IOErrors::MODEL_VALIDATION_FAILED);
    }
}

void leaper::util::validation::check_age(std::error_code &ec, std::chrono::system_clock::time_point &time_point,
                                 long long min_age_s) {
    auto diff = (leaper::util::chrono::now() - time_point);
    if (min_age_s > diff / std::chrono::seconds(1)) {
        fail(ec, IOErrors::MODEL_VALIDATION_FAILED);
    }
}
