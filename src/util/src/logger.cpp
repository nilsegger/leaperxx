#include <iostream>

#include "leaper/util/logger.hpp"
#include "leaper/config.hpp"

leaper::util::logger::Logger::Logger(string_t prefix, std::streambuf* buffer) 
	: prefix_(prefix), buffer_(buffer) {
}

int leaper::util::logger::Logger::overflow(int c) {
	if(c == std::char_traits<char>::eof() || !is_active_) {
		return std::char_traits<char>::not_eof(c);
	}
	switch(c) {
		case '\n':
		case '\r':
			is_beginning_ = true;
			break;
		default:
			if(is_beginning_) {
				is_beginning_ = false;
				this->buffer_->sputn(this->prefix_.c_str(), this->prefix_.size());
			}
	}
	return this->buffer_->sputc(c);
}

int leaper::util::logger::Logger::sync() {
	return this->buffer_->pubsync();
}

void leaper::util::logger::Logger::set_active(bool flag) {
	is_active_ = flag;
}

string_t leaper::util::logger::Logger::toRelativePath(string_t filePath) {
	return filePath.subview(Config::get("projectPath").size());
}

