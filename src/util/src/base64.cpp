//
// Created by nils on 12/06/2020.
//
#include <openssl/ssl.h>
#include <openssl/bio.h>

#include "leaper/util/base64.hpp"

void leaper::util::base64::encode(std::error_code &ec, const unsigned char *input, size_t input_length, char **buffer,
                          size_t *buffer_length) {
    BIO *bio, *b64;
    BUF_MEM *bufferPtr;

    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, bio);

    BIO_write(bio, input, input_length);
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &bufferPtr);
    BIO_set_close(bio, BIO_NOCLOSE);
    BIO_free_all(bio);
    *buffer = (*bufferPtr).data;
    *buffer_length = (*bufferPtr).length;
    if (*buffer_length < 1) {
        fail(ec, IOErrors::BASE64_ENCODING_ERROR);
    }
}

void leaper::util::base64::encode(std::error_code& ec, const unsigned char *input, size_t input_length, string_t& output) {
    char * buffer;
    size_t size;
    encode(ec, input, input_length, &buffer, &size);

    if(!ec) {
        output = string_t(buffer, size);
    }
    free(buffer);
}

void leaper::util::base64::encode_url_safe(std::error_code &ec, const unsigned char *input, size_t input_length, char **buffer,
                                   size_t *buffer_length) {
    encode(ec, input, input_length, buffer, buffer_length);
    if (ec) return;
    for (size_t i = (*buffer_length) - 1; i >= 0; i--) {
        if ((*buffer)[i] == '=' || (*buffer)[i] == '\n') {
            (*buffer)[i] = '\0';
            *buffer_length = i;
        } else {
            break;
        }
    }
    leaper::util::string::replace_all(*buffer, *buffer_length, '+', '-');
    leaper::util::string::replace_all(*buffer, *buffer_length, '/', '_');
}

string_t leaper::util::base64::encode_url_safe(std::error_code &ec, const unsigned char *input, size_t input_length) {
    char *buffer;
    size_t buffer_length;
    encode_url_safe(ec, input, input_length, &buffer, &buffer_length);
    if(ec) {
        free(buffer);
        return "";
    }
    string_t result = string_t(buffer, buffer_length);
    free(buffer);
    return result;
}

size_t leaper::util::base64::decode_length(const char *input) {
    size_t len = strlen(input), padding = 0;
    if (input[len - 1] == '=' && input[len - 2] == '=') //last two chars are =
        padding = 2;
    else if (input[len - 1] == '=') //last char is =
        padding = 1;
    return (len * 3) / 4 - padding;
}

void leaper::util::base64::decode(std::error_code &ec, const char *encoded, unsigned char **buffer, size_t *length) {
    BIO *bio, *b64;


    int decodeLen = decode_length(encoded);
    *buffer = (unsigned char *) malloc(decodeLen + 1);
    (*buffer)[decodeLen] = '\0';

    bio = BIO_new_mem_buf(encoded, -1);
    b64 = BIO_new(BIO_f_base64());
    BIO_set_flags(b64, BIO_FLAGS_BASE64_NO_NL);
    bio = BIO_push(b64, bio);

    *length = BIO_read(bio, *buffer, strlen(encoded));

    BIO_free_all(bio);

    if (*length < 1) {
        fail(ec, IOErrors::BASE64_DECODING_ERROR);
    }
}

void
leaper::util::base64::decode_url_safe(std::error_code &ec, const char *encoded, size_t encoded_length, unsigned char **buffer,
                              size_t *buffer_length) {

    int missing_length = 0;
    for (; (encoded_length + missing_length) % 4 != 0; missing_length++);

    size_t combined_length = encoded_length + missing_length;

    char tmp_buffer[combined_length];
    memcpy(tmp_buffer, encoded, encoded_length);
    for (int j = encoded_length; j < combined_length; j++) {
        tmp_buffer[j] = '=';
    }

    tmp_buffer[combined_length] = '\0';

    leaper::util::string::replace_all(tmp_buffer, encoded_length, '-', '+');
    leaper::util::string::replace_all(tmp_buffer, encoded_length, '_', '/');

    decode(ec, tmp_buffer, buffer, buffer_length);
}

string_t leaper::util::base64::decode_url_safe(std::error_code &ec, const char *encoded, size_t encoded_length) {
    unsigned char *buffer;
    size_t buffer_length;
    decode_url_safe(ec, encoded, encoded_length, &buffer, &buffer_length);
    if (!ec) return string_t(reinterpret_cast<const char *>(buffer), buffer_length);
    return "";
}

