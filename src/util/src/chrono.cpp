//
// Created by nils on 12/06/2020.
//

#include "leaper/util/chrono.hpp"


std::chrono::system_clock::time_point leaper::util::chrono::now() {
    return std::chrono::system_clock::now();
}

unsigned long long leaper::util::chrono::seconds_since_epoch() {
    return now().time_since_epoch() / std::chrono::seconds(1);
}

unsigned long long leaper::util::chrono::to_seconds(std::chrono::system_clock::time_point &time_point) {
    return time_point.time_since_epoch() / std::chrono::seconds(1);
}

string_t leaper::util::chrono::to_string(const std::chrono::system_clock::time_point& time_point) {
    struct tm * dt;
    char buffer [30];
    auto time_t = std::chrono::system_clock::to_time_t(time_point);
    dt = std::localtime(&time_t);
    strftime(buffer, sizeof(buffer), "%d/%b/%Y:%H:%M:%S %z", dt);
    return string_t(buffer);
}

