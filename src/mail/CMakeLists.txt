project(leaper_mail VERSION 1.0)

add_library(${PROJECT_NAME}
	src/curl_mail.cpp
	src/builder.cpp
)
add_library(leaper::mail ALIAS ${PROJECT_NAME})

set_target_properties(${PROJECT_NAME} PROPERTIES LINKER_LANGUAGE CXX)
