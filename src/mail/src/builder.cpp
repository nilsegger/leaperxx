//
// Created by nils on 15/06/2020.
//

#include <openssl/rand.h>
#include <iostream>

#include "leaper/mail/builder.hpp"
#include "leaper/util/chrono.hpp"
#include "leaper/util/base64.hpp"
#include "leaper/util/logger.hpp"

leaper::mail::MailBuilder::MailBuilder(std::error_code &ec, string_t fqdn, Sender sender, string_t receiver, string_t subject) : ec_(ec), fqdn_(std::move(fqdn)), sender_(std::move(sender)),
                                                                                                                   receiver_(std::move(receiver)), subject_(std::move(subject)) {

}

char *leaper::mail::MailBuilder::base36enc(unsigned long long value) {
    char base36[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    char buffer[14];
    unsigned int offset = sizeof(buffer);

    buffer[--offset] = '\0';
    do {
        buffer[--offset] = base36[value % 36];
    } while (value /= 36);

    return strdup(&buffer[offset]); // warning: this must be free-d by the user
}

void leaper::mail::MailBuilder::reduce_line_length(string_t& input) {
    const int maxLineLength = 74;
    if(input.size() > maxLineLength) {
        input.insert(maxLineLength, "\r\n");
        size_t count = 0;
        for(size_t i = maxLineLength; i < input.size(); i++, count++) {
            if(count > maxLineLength) {
                input.insert(i, "\r\n");
                count = 0;
            }
        }
    }
}

string_t leaper::mail::MailBuilder::create_message_id() {
    // Based upon https://www.jwz.org/doc/mid.html

    string_t messageId("<");

    char *time = base36enc(util::chrono::seconds_since_epoch());
    messageId.append(time);
    free(time);

    messageId.append(".");

    unsigned char buffer[8];
    int result = RAND_bytes(buffer, 8);
    if (result == 0) {
        fail(ec_, IOErrors::OPENSSL_RAND_BYTES_ERROR);
        return string_t();
    }

    unsigned long long randBytes;
    memcpy(&randBytes, buffer, sizeof randBytes);

    char *randId = base36enc(randBytes);
    messageId.append(randId);
    free(randId);

    messageId.append("@");
    messageId.append(fqdn_);
    messageId.append(">");

    return messageId;
}

void leaper::mail::MailBuilder::create_boundary() {
    if(boundary_) return;

    int size = 16;
    unsigned char buffer[size];
    int result = RAND_bytes(buffer, size);
    if (result == 0) return fail(ec_, IOErrors::OPENSSL_RAND_BYTES_ERROR);

    char * base64;
    size_t base64Length;
    util::base64::encode(ec_, buffer, size, &base64, &base64Length);

    if(!ec_) {
        boundary_.emplace(base64, base64Length);
        free(base64);
    }
}

void leaper::mail::MailBuilder::add_block(string_t mime, string_t content) {
    create_boundary();
    string_t block("--");
    block.append(boundary_.value());
    block.append("\r\nContent-Type: ");
    block.append(mime);
    block.append("; charset=\"utf-8\"\r\nContent-Transfer-Encoding: base64\r\n");

    util::base64::encode(ec_, (unsigned char*)(content.c_str()), content.size(), content);
    DEBUG << content << std::endl;
    reduce_line_length(content);

    block.append(content);
    block.append("\r\n");
    content_.push_back(block);
}

void leaper::mail::MailBuilder::add_content(string_t content) {
    add_block("text/plain", content);
}

void leaper::mail::MailBuilder::add_html_content(string_t content) {
    add_block("text/html", content);
}

void leaper::mail::MailBuilder::add_attachement(string_t mime, string_t name, string_t encoding, string_t content) {
    create_boundary();
    string_t block("--");
    block.append(boundary_.value());
    block.append("\r\nContent-Type: ");
    block.append(mime);
    block.append(";\r\nContent-Transfer-Encoding: ");
    block.append(encoding);
    block.append("\r\n");
    block.append("Content-Disposition: attachement; filename=\"");
    block.append(name);
    block.append("\"\r\n");
    block.append(content);
    block.append("\r\n");
    reduce_line_length(content);
    content_.push_back(block);
}

std::vector<string_t> leaper::mail::MailBuilder::render() {
    string_t date("Date: ");
    date.append(util::chrono::to_string(util::chrono::now()));
    date.append("\r\n");

    string_t to("To: ");
    to.append(receiver_);
    to.append("\r\n");

    string_t from("From: ");
    from.append(sender_.email);
    from.append("(");
    from.append(sender_.name);
    from.append(")\r\n");

    string_t messageId("Message-ID: ");
    messageId.append(create_message_id());
    messageId.append("\r\n");

    string_t subject("Subject: ");
    subject.append(subject_);
    subject.append("\r\n");

    string_t contentType("MIME-Version: 1.0\r\nContent-Type: multipart/mixed; boundary=\"");
    contentType.append(boundary_.value());
    contentType.append("\"\r\n");

    std::vector<string_t> message = {
            date,
            to,
            from,
            messageId,
            subject,
            contentType
    };

    message.insert(message.end(), content_.begin(), content_.end());

    string_t end("--");
    end.append(boundary_.value());
    end.append("--");
    message.emplace_back(end);

    return message;
}


