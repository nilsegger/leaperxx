//
// Created by nils on 15/06/2020.
//

#include "leaper/mail/curl_mail.hpp"
#include "leaper/config.hpp"


leaper::mail::CurlMail::CurlMail(std::error_code &ec) : ec_(ec) {

}

void leaper::mail::CurlMail::set_options(string_t server, string_t username, string_t password, std::optional<string_t> sslCert) {
    curl_ = curl_easy_init();

    if (!curl_) {
        return fail(ec_, IOErrors::MAIL_INITIALIZE);
    }

    /* Set username and password */
    curl_easy_setopt(curl_, CURLOPT_USERNAME, username.c_str());
    curl_easy_setopt(curl_, CURLOPT_PASSWORD, password.c_str());

    /* This is the URL for your mailserver. Note the use of port 587 here,
     * instead of the normal SMTP port (25). Port 587 is commonly used for
     * secure mail submission (see RFC4403), but you should use whatever
     * matches your server configuration. */
    curl_easy_setopt(curl_, CURLOPT_URL, server.c_str());

    /* In this example, we'll start with a plain text connection, and upgrade
         * to Transport Layer Security (TLS) using the STARTTLS command. Be careful
         * of using CURLUSESSL_TRY here, because if TLS upgrade fails, the transfer
         * will continue anyway - see the security discussion in the libcurl
         * tutorial for more details. */
    curl_easy_setopt(curl_, CURLOPT_USE_SSL, (long) CURLUSESSL_ALL);

    /* If your server doesn't have a valid certificate, then you can disable
         * part of the Transport Layer Security protection by setting the
         * CURLOPT_SSL_VERIFYPEER and CURLOPT_SSL_VERIFYHOST options to 0 (false).
         *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0L);
         *   curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0L);
         * That is, in general, a bad idea. It is still better than sending your
         * authentication details in plain text though.  Instead, you should get
         * the issuer certificate (or the host certificate if the certificate is
         * self-signed) and add it to the set of certificates that are known to
         * libcurl using CURLOPT_CAINFO and/or CURLOPT_CAPATH. See docs/SSLCERTS
         * for more information. */
    if (sslCert) {
        curl_easy_setopt(curl_, CURLOPT_CAINFO, sslCert->c_str());
    } else {
        curl_easy_setopt(curl_, CURLOPT_SSL_VERIFYPEER, 0L);
        curl_easy_setopt(curl_, CURLOPT_SSL_VERIFYHOST, 0L);
    }
}

void leaper::mail::CurlMail::set_sender(string_t sender) {
    /* Note that this option isn't strictly required, omitting it will result
         * in libcurl sending the MAIL FROM command with empty sender data. All
         * autoresponses should have an empty reverse-path, and should be directed
         * to the address in the reverse-path which triggered them. Otherwise,
         * they could cause an endless loop. See RFC 5321 Section 4.5.5 for more
         * details.
         */
    curl_easy_setopt(curl_, CURLOPT_MAIL_FROM, sender.c_str());
}

void leaper::mail::CurlMail::set_recipient(string_t recipient) {
    /* Add two recipients, in this particular case they correspond to the
     * To: and Cc: addressees in the header, but they could be any kind of
         * recipient. */
    recipients_ = curl_slist_append(recipients_, recipient.c_str());
    curl_easy_setopt(curl_, CURLOPT_MAIL_RCPT, recipients_);
}

size_t leaper::mail::CurlMail::read_content(void *ptr, size_t size, size_t nmemb, void *data) {

    if ((size == 0) || (nmemb == 0) || ((size * nmemb) < 1)) {
        return 0;
    }

    auto *payload = (struct MailPayload *) data;

    if (payload->position == payload->content.size() - 1) return 0;

    string_t buffer = payload->content[payload->position];

    memcpy(ptr, buffer.c_str(), buffer.size());
    payload->position++;
    return buffer.size();
}

void leaper::mail::CurlMail::set_payload(std::vector<string_t> &&content) {
    payload_.content = std::move(content);
    payload_.position = 0;
    /* We're using a callback function to specify the payload (the headers and
     * body of the message). You could just use the CURLOPT_READDATA option to
     * specify a FILE pointer to read from. */
    curl_easy_setopt(curl_, CURLOPT_READFUNCTION, leaper::mail::CurlMail::read_content);
    curl_easy_setopt(curl_, CURLOPT_READDATA, &payload_);
    curl_easy_setopt(curl_, CURLOPT_UPLOAD, 1L);
}

void leaper::mail::CurlMail::set_verbose() {
    /* Since the traffic will be encrypted, it is very useful to turn on debug
         * information within libcurl to see what is happening during the transfer.
         */
    curl_easy_setopt(curl_, CURLOPT_VERBOSE, 1L);
    verbose_ = true;
}

void leaper::mail::CurlMail::send() {
/* Send the message */
    res_ = curl_easy_perform(curl_);

    /* Check for errors */
    if (res_ != CURLE_OK) {
        if (verbose_) {
            fprintf(stderr, "curl_easy_perform() failed: %s\n",curl_easy_strerror(res_));
        }
        fail(ec_, IOErrors::MAIL_SEND);
    }

    /* Free the list of recipients */
    curl_slist_free_all(recipients_);

    /* Always cleanup */
    curl_easy_cleanup(curl_);
}

void leaper::mail::CurlMail::send(std::error_code&ec, string_t receiver, std::vector<string_t>&& payload) {
    CurlMail mail(ec);
    mail.set_options(Config::get("mailServer"), Config::get("mailUsername"), Config::get("mailPassword"), Config::get("mailSSL"));
    mail.set_sender(Config::get("mailSenderEmail"));
    mail.set_recipient(receiver);
    mail.set_payload(std::move(payload));
    mail.set_verbose();
    mail.send();
}
