//
// Created by nils on 15/06/2020.
//

#ifndef LEAPER_LIBS_MAIL_INCLUDE_MAIL_BUILDER_HPP
#define LEAPER_LIBS_MAIL_INCLUDE_MAIL_BUILDER_HPP

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"

namespace leaper::mail {
struct Sender {
    string_t name;
    string_t email;

    Sender(string_t name, string_t email) : name(name), email(email) {}
};

class MailBuilder {

    std::error_code& ec_;
    string_t fqdn_;
    string_t subject_;
    string_t receiver_;
    Sender sender_;

    std::optional<string_t> boundary_;
    std::vector<string_t> content_;

    static char *base36enc(unsigned long long value);
    void reduce_line_length(string_t& input);

    void create_boundary();
    void add_block(string_t mime, string_t content);
    string_t create_message_id();

public:

    MailBuilder(std::error_code& ec, string_t fqdn, Sender sender, string_t receiver_, string_t subject);

    void add_content(string_t content);

    void add_html_content(string_t content);

    void add_attachement(string_t mime, string_t name, string_t encoding, string_t content);

    std::vector<string_t> render();
};

} // namespace leaper::mail

#endif //LEAPER_LIBS_MAIL_INCLUDE_MAIL_BUILDER_HPP
