//
// Created by nils on 15/06/2020.
//

#ifndef LEAPER_LIBS_MAIL_INCLUDE_MAIL_CURL_MAIL_HPP
#define LEAPER_LIBS_MAIL_INCLUDE_MAIL_CURL_MAIL_HPP

#include <vector>

#include <curl/curl.h>

#include "leaper/io_errors.hpp"
#include "leaper/definitions.hpp"

namespace leaper::mail {

class CurlMail {

    struct MailPayload {
        std::vector<string_t> content;
        int position;
    };

    std::error_code& ec_;
    CURL * curl_;
    CURLcode res_ = CURLE_OK;
    struct curl_slist *recipients_ = nullptr;
    struct MailPayload payload_;
    bool verbose_ = false;

    static size_t read_content(void *ptr, size_t size, size_t nmemb, void *data);

public:

    CurlMail(std::error_code& ec);

    void set_options(string_t server, string_t username, string_t password, std::optional<string_t> sslCert);
    void set_sender(string_t sender);
    void set_recipient(string_t recipient);
    void set_payload(std::vector<string_t>&& content);
    void set_verbose();
    void send();

    static void send(std::error_code&ec, string_t receiver, std::vector<string_t>&& payload);
};

} // namespace leaper::mail

#endif //LEAPER_LIBS_MAIL_INCLUDE_MAIL_CURL_MAIL_HPP
