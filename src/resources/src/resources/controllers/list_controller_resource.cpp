//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/list_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

void leaper::resources::ListControllerResource::list_callback(std::vector<std::unique_ptr<models::Model>> models) {
    if (!this->ec_) {
        result_ = create_list(models);
    }
    this->handle_response();
}

void leaper::resources::ListControllerResource::check_permission() {
    this->authenticated_method_route();
}

std::pair<int, int> leaper::resources::ListControllerResource::get_offset_and_limit() {
    return {this->query_parameters_.get_int(this->ec_, "offset"), this->query_parameters_.get_int(this->ec_, "limit")};
}

void leaper::resources::ListControllerResource::after_authentication() {
    this->prepare_connection();
    check_permission();
}

void leaper::resources::ListControllerResource::do_authenticated_get() {
    this->prepare_queue(1);
    controller_ = create_controller();

    std::pair<int, int> range = get_offset_and_limit();

    if (this->ec_) return this->handle_response();

    controller_->list(&leaper::resources::ListControllerResource::list_callback, this, range.first, range.second);
}

void leaper::resources::ListControllerResource::build_authenticated_get_response() {
    this->do_write(std::move(http::response::build_json(this->header_parser_.get().version(), beast::http::status::ok, result_)));
}

