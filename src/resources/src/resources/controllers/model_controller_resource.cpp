//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/model_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

void leaper::resources::ModelControllerResource::read_callback(std::optional<std::unique_ptr<models::Model>> model) {
    if (!this->ec_ && !model) {
        fail(this->ec_, error_codes::MODEL_NOT_FOUND);
    } else if (!this->ec_ && model) {
        request_container_->model = std::move(model.value());
    }
    this->queue_->done();
}

bool leaper::resources::ModelControllerResource::input_model() {
    json::object body;
    if (this->get_json(body)) {
        request_container_->controller->model_->from_json(this->ec_, body);
        if (!this->ec_) return true;
    }
    this->handle_response();
    return false;
}

void leaper::resources::ModelControllerResource::post_exists_callback(bool exists) {
    if (!this->ec_ && !exists) {
        // Creates user profile
        this->read([this]() {
            if (input_model()) {
                request_container_->controller->create(&leaper::resources::ModelControllerResource::queue_item_finished, this);
            }
        });
    } else {
        if (!this->ec_) fail(this->ec_, HTTPErrors::DUPLICATE);
        return this->handle_response();
    }
}

void leaper::resources::ModelControllerResource::put_exists_callback(bool exists) {
    if (!this->ec_ && exists) {
        this->read([this]() {
            if (input_model()) {
                request_container_->controller->update(&leaper::resources::ModelControllerResource::queue_item_finished, this);
            }
        });
    } else {
        if (!this->ec_) fail(this->ec_, error_codes::MODEL_NOT_FOUND);
        return this->handle_response();
    }
}

void leaper::resources::ModelControllerResource::prepare(unsigned int callbacks) {
    Resource::prepare(callbacks);

    permission_controller_ = std::move(create_permission_controller());
    if (this->ec_) return this->handle_response();

    controller_ptr controller = create_controller();
    if (this->ec_) return this->handle_response();

    request_container_ = std::make_unique<RequestContainer>(controller);

    permission_controller_->check(controllers::Permissions::EDIT);
}

void leaper::resources::ModelControllerResource::after_authentication() {
    prepare(1);
}

void leaper::resources::ModelControllerResource::do_authenticated_get() {
    request_container_->controller->read(&leaper::resources::ModelControllerResource::read_callback, this, true);
}

void leaper::resources::ModelControllerResource::build_authenticated_get_response() {
    json::object response = request_container_->model->to_json();
    this->do_write(std::move(http::response::build_json(this->header_parser_.get().version(), beast::http::status::ok, response)));
}

void leaper::resources::ModelControllerResource::do_authenticated_post() {
    request_container_->controller->exists(&leaper::resources::ModelControllerResource::post_exists_callback, this);
}

void leaper::resources::ModelControllerResource::build_authenticated_post_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}

void leaper::resources::ModelControllerResource::do_authenticated_put() {
    request_container_->controller->exists(&leaper::resources::ModelControllerResource::put_exists_callback, this);
}

void leaper::resources::ModelControllerResource::build_authenticated_put_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}

void leaper::resources::ModelControllerResource::do_authenticated_delete() {
    request_container_->controller->remove(&leaper::resources::ModelControllerResource::queue_item_finished, this);
}

void leaper::resources::ModelControllerResource::build_authenticated_delete_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}
