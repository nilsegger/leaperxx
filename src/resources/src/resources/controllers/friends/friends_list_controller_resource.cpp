//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/friends/friends_list_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

json::array leaper::resources::FriendsListControllerResource::create_list(std::vector<std::unique_ptr<models::Model>> &models) {
    json::array arr;
    for (size_t i = 0; i < models.size(); i++) {
        models::User *user = (models::User *) (models[i].get());
        json::object obj = {{"uuid",         util::uuid::to_string(user->uuid)},
                            {"display_name", user->display_name}};
        arr.emplace_back(obj);
    }
    return arr;
}

std::unique_ptr<leaper::controllers::ListController<leaper::resources::ListControllerResource>> leaper::resources::FriendsListControllerResource::create_controller() {
    return std::make_unique<controllers::FriendsListController<ListControllerResource>>(this->ec_, *this->connection_, user_.get());
}

void leaper::resources::FriendsListControllerResource::check_permission() {
    user_ = std::make_unique<models::User>(this->path_parameters_.get_uuid(this->ec_, "uuid"));
    if (this->ec_) return this->handle_response();

    permission_controller_ = std::make_unique<controllers::UserPermissionController<FriendsListControllerResource>>(this->ec_, *this->connection_, &leaper::resources::FriendsListControllerResource::authenticated_method_route, this,
                                                                                                       this->requester_.get(), user_.get());
    permission_controller_->check(controllers::Permissions::EDIT);
}

