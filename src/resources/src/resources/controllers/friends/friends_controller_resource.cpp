//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/friends/friends_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

void leaper::resources::FriendsControllerResource::on_post(std::optional<bool> confirmed) {
    if (confirmed.has_value()) {
        if (confirmed.value()) {
            fail(this->ec_, error_codes::EXISTING_FRIENDS);
        } else {
            fail(this->ec_, error_codes::DUPLICATE_FRIEND_REQUEST);
        }
        this->handle_response();
    } else {
        controller_->add_friends(&leaper::resources::FriendsControllerResource::handle_response, this);
    }
}

void leaper::resources::FriendsControllerResource::on_put(bool received) {
    if (!received) {
        fail(this->ec_, error_codes::MISSING_FRIEND_REQUEST);
        this->handle_response();
    } else {
        controller_->accept_friend_request(&leaper::resources::FriendsControllerResource::handle_response, this);
    }
}


void leaper::resources::FriendsControllerResource::on_delete(std::optional<bool> confirmed) {
    if (confirmed) {
        controller_->remove_friend(&leaper::resources::FriendsControllerResource::handle_response, this);
    } else {
        fail(this->ec_, error_codes::FRIEND_NOT_FOUND);
        this->handle_response();
    }
}

void leaper::resources::FriendsControllerResource::after_authentication() {
    this->prepare(1);

    requesting_user_ = std::make_unique<models::User>(this->path_parameters_.get_uuid(this->ec_, "uuid"));
    friend_user_ = std::make_unique<models::User>(this->path_parameters_.get_uuid(this->ec_, "friend"));

    if (this->ec_) return this->handle_response();

    if (requesting_user_->uuid == friend_user_->uuid) {
        fail(this->ec_, error_codes::INVALID_SELF_REQUEST);
        return this->handle_response();
    }

    permission_controller_ = std::make_unique<controllers::FriendsPermissionController<FriendsControllerResource>>(this->ec_, *this->connection_, &leaper::resources::FriendsControllerResource::authenticated_method_route, this,
                                                                                                   this->requester_.get(), requesting_user_.get(), friend_user_.get());
    permission_controller_->check(controllers::Permissions::SELF_EDIT_OTHER_EXISTS);
}

void leaper::resources::FriendsControllerResource::do_authenticated_post() {
    // Send friend request, -> requesting_user is sender
    this->prepare_queue(1);
    controller_ = std::make_unique<controller_t>(this->ec_, *this->connection_, requesting_user_.get(), friend_user_.get());
    controller_->are_friends(&leaper::resources::FriendsControllerResource::on_post, this);
}

void leaper::resources::FriendsControllerResource::build_authenticated_post_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}

void leaper::resources::FriendsControllerResource::do_authenticated_put() {
    // Accept friend request, -> requesting_user was receiver
    this->prepare_queue(1);
    controller_ = std::make_unique<controller_t>(this->ec_, *this->connection_, friend_user_.get(), requesting_user_.get());
    controller_->has_received_request(&leaper::resources::FriendsControllerResource::on_put, this);
}

void leaper::resources::FriendsControllerResource::build_authenticated_put_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}

void leaper::resources::FriendsControllerResource::do_authenticated_delete() {
    // Revokes or removes friend requests. Can also be used to remove friend.
    // requesting_user can both be sender and receiver
    this->prepare_queue(1);
    controller_ = std::make_unique<controller_t>(this->ec_, *this->connection_, requesting_user_.get(), friend_user_.get());
    controller_->are_friends(&leaper::resources::FriendsControllerResource::on_delete, this);
}

void leaper::resources::FriendsControllerResource::build_authenticated_delete_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}


