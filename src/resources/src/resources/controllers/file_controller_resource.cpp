//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/file_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

void leaper::resources::FileControllerResource::on_post(std::optional<leaper::controllers::FileControllerAddResult> result) {
    if (this->ec_) return this->handle_response();
    this->post_uuid_ = result->file_uuid;
    this->save(result->file_path, std::bind(&leaper::resources::FileControllerResource::queue_item_finished, this));
}

void leaper::resources::FileControllerResource::do_authenticated_post() {

    if (!this->contains_header("content-type")) {
        fail(this->ec_, HTTPErrors::MISSING_HEADER);
    } else {
        check_mime(this->header_parser_.get()[beast::http::field::content_type]);
    }

    if (this->ec_) return this->handle_response();

    controller_ = create_controller();
    if (this->ec_) return this->handle_response();

    controller_->add(&leaper::resources::FileControllerResource::on_post, this, this->header_parser_.get()[beast::http::field::content_type]);
}

void leaper::resources::FileControllerResource::build_authenticated_post_response() {
	json::object response = {{"uuid", util::uuid::to_string(post_uuid_.value())}};
    this->do_write(std::move(http::response::build_json(this->version(), beast::http::status::ok, response)));
}

void leaper::resources::FileControllerResource::do_authenticated_delete() {

    controller_ = create_controller();

    if (this->ec_) return this->handle_response();

    controller_->remove(&leaper::resources::FileControllerResource::queue_item_finished, this);
}

void leaper::resources::FileControllerResource::build_authenticated_delete_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}

void leaper::resources::FileControllerResource::on_get(std::optional<string_t> path, std::optional<string_t> mime) {

    if (this->ec_) return this->handle_response();
        // If path is empty, requested picture does not exist
    else if (!path) {
        fail(this->ec_, IOErrors::FILE_NOT_FOUND);
    } else {
        read_container_ = std::make_unique<ReadContainer>(path, mime);
    }

    return this->handle_response();
}

void leaper::resources::FileControllerResource::do_authenticated_get() {

    controller_ = create_controller();
    if (this->ec_) return this->handle_response();

    controller_->get(&leaper::resources::FileControllerResource::on_get, this);
}

void leaper::resources::FileControllerResource::build_authenticated_get_response() {
    assert(read_container_);

    if(!read_container_->path || !read_container_->mime) {
        beast::http::response<beast::http::empty_body> notFoundResp{beast::http::status::not_found, this->version()};
        return this->do_write(std::move(notFoundResp));
    }

    beast::http::response<beast::http::file_body> response{beast::http::status::ok, this->version()};
    response.set("content-type", read_container_->mime.value());
    response.set("cache-control", cache_control_);
    DEBUG << "Writing " << read_container_->path.value().c_str() << " into response" << std::endl;
    response.body().open(read_container_->path.value().c_str(), boost::beast::file_mode::read, read_container_->file_ec);

    if(read_container_->file_ec) {
        beast::http::response<beast::http::empty_body> notFoundResp{beast::http::status::not_found, this->version()};
        return this->do_write(std::move(notFoundResp));
    }

    this->do_write(std::move(response));
}


