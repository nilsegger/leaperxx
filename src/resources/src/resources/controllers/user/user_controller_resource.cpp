//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/user/user_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

std::unique_ptr<leaper::controllers::ModelController<leaper::resources::ModelControllerResource>> leaper::resources::UserControllerResource::create_controller() {
    return std::make_unique<controllers::UserController<ModelControllerResource>>(this->ec_, *(this->connection_), user_.get());
}

std::unique_ptr<leaper::controllers::PermissionController<leaper::resources::ModelControllerResource>> leaper::resources::UserControllerResource::create_permission_controller() {
    user_ = std::make_unique<models::User>(this->path_parameters_.get_uuid(this->ec_, "uuid"));
    return std::make_unique<controllers::UserPermissionController<ModelControllerResource>>(this->ec_, *(this->connection_), &leaper::resources::UserControllerResource::authenticated_method_route, this, this->requester_.get(),
                                                                               (models::User*)user_.get());
}
