//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/user/user_profile_picture_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

std::unique_ptr<leaper::controllers::FileController<leaper::resources::FileControllerResource>> leaper::resources::UserProfilePictureControllerResource::create_controller() {
    return std::make_unique<controllers::UserProfilePictureController<FileControllerResource>>(this->ec_, *this->connection_, user_.get());
}

void leaper::resources::UserProfilePictureControllerResource::check_mime(string_t mime) {
    if (mime != "image/png" && mime != "image/jpeg") {
        fail(this->ec_, HTTPErrors::UNSUPPORTED_MEDIA);
    }
}

void leaper::resources::UserProfilePictureControllerResource::after_authentication() {
	this->prepare(1);
    user_ = std::make_unique<models::User>(this->path_parameters_.get_uuid(this->ec_, "uuid"));
    if (this->ec_) return this->handle_response();

    permission_controller_ = std::make_unique<controllers::UserPermissionController<UserProfilePictureControllerResource>>(this->ec_, *this->connection_,
                                                                                                              &leaper::resources::UserProfilePictureControllerResource::authenticated_method_route, this, requester_.get(), user_.get()); 
    permission_controller_->check(controllers::Permissions::EXISTS_AND_EDIT);
}
