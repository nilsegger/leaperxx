//
// Created by nils on 12/06/2020.
//

#include "leaper/resources/controllers/user/user_list_controller_resource.hpp"
#include "leaper/http/response_builder.hpp"

json::array leaper::resources::UserListControllerResource::create_list(std::vector<std::unique_ptr<models::Model>> &models) {
    json::array arr;
    for (size_t i = 0; i < models.size(); i++) {
        models::User *user = (models::User *) (models[i].get());
        json::object obj = {{"uuid",  util::uuid::to_string(user->uuid)},
                            {"display_name", user->display_name}};
        arr.emplace_back(obj);
    }
    return arr;
}

std::unique_ptr<leaper::controllers::ListController<leaper::resources::ListControllerResource>> leaper::resources::UserListControllerResource::create_controller() {
    return std::make_unique<controllers::UserListController<ListControllerResource>>(this->ec_, *this->connection_);
}

void leaper::resources::UserListControllerResource::check_permission() {
    if (this->requester_->role != "admin") {
        fail(this->ec_, error_codes::ROLE_FORBIDDEN);
    }
    this->authenticated_method_route();
}

