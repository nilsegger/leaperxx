//
// Created by nils on 16/06/2020.
//

#ifndef LEAPER_USER_REGISTER_RESOURCE_HPP
#define LEAPER_USER_REGISTER_RESOURCE_HPP

#include "leaper/auth/register_resource.hpp"

namespace leaper::resources {

class UserRegisterResource : public auth::RegisterResource {

    string_t get_role() override;

public:

    using auth::RegisterResource::RegisterResource;
};

} // namespace leaper::resources

#endif //LEAPER_USER_REGISTER_RESOURCE_HPP
