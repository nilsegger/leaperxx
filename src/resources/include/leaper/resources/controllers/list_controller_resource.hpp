//
// Created by nils on 03/06/2020.
//

#ifndef LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_LIST_CONTROLLER_RESOURCE_HPP
#define LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_LIST_CONTROLLER_RESOURCE_HPP

#include <boost/beast/http.hpp>

#include "leaper/http/resources/authenticated_resource.hpp"
#include "leaper/controllers/list_controller.hpp"

namespace leaper::resources {

class ListControllerResource : public http::AuthenticatedResource {

    typedef controllers::ListController<ListControllerResource> list_controller_t;
    std::unique_ptr<list_controller_t> controller_;
    json::array result_;

    void list_callback(std::vector<std::unique_ptr<models::Model>> models);

protected:

    virtual json::array create_list(std::vector<std::unique_ptr<models::Model>>& models) = 0;

    virtual std::unique_ptr<list_controller_t> create_controller() = 0;

    virtual void check_permission();

    virtual std::pair<int, int > get_offset_and_limit();

    void after_authentication() override;

    void do_authenticated_get() override;

    void build_authenticated_get_response() override;

public:

    using AuthenticatedResource::AuthenticatedResource;
};

} // namespace leaper::resources

#endif //LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_LIST_CONTROLLER_RESOURCE_HPP
