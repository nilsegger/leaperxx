//
// Created by nils on 25/05/2020.
//

#ifndef LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_CONTROLLER_RESOURCE_HPP
#define LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_CONTROLLER_RESOURCE_HPP

#include <boost/beast/http.hpp>

#include "leaper/controllers/model_controller.hpp"
#include "leaper/models/model.hpp"
#include "leaper/controllers/permission_controller.hpp"
#include "leaper/http/resources/string_resource.hpp"

namespace leaper::resources {

class ModelControllerResource : public http::StringResource {

    typedef std::unique_ptr<controllers::ModelController<ModelControllerResource>> controller_ptr;
    typedef std::unique_ptr<controllers::PermissionController<ModelControllerResource>> permission_ptr;

    struct RequestContainer {
        controller_ptr controller;
        std::unique_ptr<models::Model> model;

        explicit RequestContainer(controller_ptr &controller)
                : controller(std::move(controller)) {}
    };

    std::unique_ptr<RequestContainer> request_container_;
    permission_ptr permission_controller_;

    void read_callback(std::optional<std::unique_ptr<models::Model>> model);

    bool input_model();

    void post_exists_callback(bool exists);

    void put_exists_callback(bool exists);

protected:

    virtual controller_ptr create_controller() = 0;

    virtual permission_ptr create_permission_controller() = 0;

    void prepare(unsigned int callbacks) override;

    void after_authentication() override;

    void do_authenticated_get() override;

    void build_authenticated_get_response() override;

    void do_authenticated_post() override;

    void build_authenticated_post_response() override;

    void do_authenticated_put() override;

    void build_authenticated_put_response() override;

    void do_authenticated_delete() override;

    void build_authenticated_delete_response() override;

public:

    using StringResource::StringResource;

};

} // leaper::resources

#endif //LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_CONTROLLER_RESOURCE_HPP
