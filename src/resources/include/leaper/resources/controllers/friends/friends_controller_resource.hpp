//
// Created by nils on 27/05/2020.
//

#ifndef LEAPER_FRIENDS_CONTROLLER_RESOURCE_HPP
#define LEAPER_FRIENDS_CONTROLLER_RESOURCE_HPP

#include <boost/beast/http.hpp>

#include "leaper/http/resources/authenticated_resource.hpp"
#include "leaper/definitions.hpp"
#include "leaper/models/user.hpp"
#include "leaper/controllers/friends/friends_permission_controller.hpp"
#include "leaper/controllers/friends/friends_controller.hpp"

namespace leaper::resources {

class FriendsControllerResource : public http::AuthenticatedResource {

    std::unique_ptr<models::User> requesting_user_;
    std::unique_ptr<models::User> friend_user_;

    typedef controllers::FriendsController<FriendsControllerResource> controller_t;

    std::unique_ptr<controller_t> controller_;

    std::unique_ptr<controllers::FriendsPermissionController<FriendsControllerResource>> permission_controller_;

    void on_post(std::optional<bool> confirmed);

    void on_put(bool received);

    void on_delete(std::optional<bool> confirmed);

protected:

    void after_authentication() override;

    void do_authenticated_post() override;

    void build_authenticated_post_response() override;

    void do_authenticated_put() override;

    void build_authenticated_put_response() override;

    void do_authenticated_delete() override;

    void build_authenticated_delete_response() override;

public:

    using AuthenticatedResource::AuthenticatedResource;

};

} // leaper::resources

#endif //LEAPER_FRIENDS_CONTROLLER_RESOURCE_HPP
