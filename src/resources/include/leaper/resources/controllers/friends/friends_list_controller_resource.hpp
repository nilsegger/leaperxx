//
// Created by nils on 03/06/2020.
//

#ifndef LEAPER_FRIENDS_LIST_CONTROLLER_RESOURCE_HPP
#define LEAPER_FRIENDS_LIST_CONTROLLER_RESOURCE_HPP

#include "leaper/resources/controllers/list_controller_resource.hpp"
#include "leaper/models/user.hpp"
#include "leaper/controllers/friends/friends_list_controller.hpp"
#include "leaper/controllers/user/user_permission_controller.hpp"

namespace leaper::resources {

class FriendsListControllerResource : public ListControllerResource {

    std::unique_ptr<models::User> user_;
    std::unique_ptr<controllers::UserPermissionController<FriendsListControllerResource>> permission_controller_;

protected:
    json::array create_list(std::vector<std::unique_ptr<models::Model>> &models) override;

    std::unique_ptr<controllers::ListController<ListControllerResource>> create_controller() override;

    void check_permission() override;

public:

    using ListControllerResource::ListControllerResource;

};

} // leaper::resources

#endif //LEAPER_FRIENDS_LIST_CONTROLLER_RESOURCE_HPP
