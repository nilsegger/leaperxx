//
// Created by nils on 03/06/2020.
//

#ifndef LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLER_USER_LIST_CONTROLLER_RESOURCE_HPP
#define LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLER_USER_LIST_CONTROLLER_RESOURCE_HPP

#include "leaper/resources/controllers/list_controller_resource.hpp"
#include "leaper/controllers/user/user_list_controller.hpp"
#include "leaper/models/user.hpp"

namespace leaper::resources {

class UserListControllerResource : public ListControllerResource {

protected:

    json::array create_list(std::vector<std::unique_ptr<models::Model>> &models) override;

    std::unique_ptr<controllers::ListController<ListControllerResource>> create_controller() override;

    void check_permission() override;

public:

    using ListControllerResource::ListControllerResource;
};

} // namespace leaper::resources

#endif //LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLER_USER_LIST_CONTROLLER_RESOURCE_HPP
