//
// Created by nils on 18/05/2020.
//

#ifndef LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_USER_CONTROLLER_RESOURCE_HPP
#define LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_USER_CONTROLLER_RESOURCE_HPP

#include "leaper/models/user.hpp"
#include "leaper/models/address.hpp"
#include "leaper/controllers/user/user_controller.hpp"
#include "leaper/resources/controllers/model_controller_resource.hpp"
#include "leaper/controllers/user/user_permission_controller.hpp"

namespace leaper::resources {

class UserControllerResource : public ModelControllerResource {

    std::unique_ptr<models::Model> user_;

protected:
    std::unique_ptr<controllers::ModelController<ModelControllerResource>> create_controller() override;

    std::unique_ptr<controllers::PermissionController<ModelControllerResource>> create_permission_controller() override;

public:

    using ModelControllerResource::ModelControllerResource;

};

} // namespace leaper::resources

#endif //LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_USER_CONTROLLER_RESOURCE_HPP
