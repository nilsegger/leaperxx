//
// Created by nils on 10/06/2020.
//

#ifndef LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_USER_PROFILE_PICTURE_CONTROLLER_RESOURCE_HPP
#define LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_USER_PROFILE_PICTURE_CONTROLLER_RESOURCE_HPP

#include "leaper/resources/controllers/file_controller_resource.hpp"
#include "leaper/models/user.hpp"
#include "leaper/controllers/user/user_profile_picture_controller.hpp"
#include "leaper/controllers/user/user_permission_controller.hpp"

namespace leaper::resources {

class UserProfilePictureControllerResource : public FileControllerResource {

    std::unique_ptr<models::User> user_;
    std::unique_ptr<controllers::UserPermissionController<UserProfilePictureControllerResource>> permission_controller_;

protected:
    std::unique_ptr<controllers::FileController<FileControllerResource>> create_controller() override;

    void check_mime(string_t mime) override;

    void after_authentication() override;

public:
    using FileControllerResource::FileControllerResource;

};

} // namespace leaper::resources

#endif //LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_USER_PROFILE_PICTURE_CONTROLLER_RESOURCE_HPP
