//
// Created by nils on 07/06/2020.
//

#ifndef LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_FILE_CONTROLLER_RESOURCE_HPP
#define LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_FILE_CONTROLLER_RESOURCE_HPP

#include "leaper/http/resources/file_resource.hpp"
#include "leaper/controllers/file_controller.hpp"

namespace leaper::resources {

class FileControllerResource : public http::FileResource {

    std::unique_ptr<controllers::FileController<FileControllerResource>> controller_;

    struct ReadContainer {
        std::optional<string_t> path, mime;
        beast::error_code file_ec;

        ReadContainer(std::optional<string_t> &path, std::optional<string_t> &mime) : path(path), mime(mime) {}
    };

    std::unique_ptr<ReadContainer> read_container_;
    std::optional<uuid_t> post_uuid_;

    void on_get(std::optional<string_t> path, std::optional<string_t> mime);

    void on_post(std::optional<controllers::FileControllerAddResult> result);

protected:

    virtual std::unique_ptr<controllers::FileController<FileControllerResource>> create_controller() = 0;

    virtual void check_mime(string_t mime) = 0;

    void do_authenticated_get() override;

    void build_authenticated_get_response() override;

    void do_authenticated_post() override;

    void build_authenticated_post_response() override;

    void do_authenticated_delete() override;

    void build_authenticated_delete_response() override;

public:

    using FileResource::FileResource;

};

} // namespace leaper::resources

#endif //LEAPER_RESOURCES_INCLUDE_RESOURCES_CONTROLLERS_FILE_CONTROLLER_RESOURCE_HPP
