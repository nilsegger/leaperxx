//
// Created by nils on 05/05/2020.
//

#ifndef LEAPER_RESOURCES_INCLUDE_RESOURCES_TEST_USER_INFO_RESOURCE_HPP
#define LEAPER_RESOURCES_INCLUDE_RESOURCES_TEST_USER_INFO_RESOURCE_HPP

#include <boost/beast/http.hpp>

#include "leaper/http/resources/string_resource.hpp"
#include "leaper/http/response_builder.hpp"
#include "leaper/auth/authentication.hpp"

namespace leaper::resources {
class user_resource : public http::StringResource {

    struct PostResult {
        bool is_duplicate;
        std::optional<boost::uuids::uuid> uuid;
        PostResult(bool isDuplicate) : is_duplicate(isDuplicate) {}
    };

    std::unique_ptr<PostResult> post_result_;
    std::unique_ptr<auth::Authentication> authentication_;

public:
    using StringResource::StringResource;

protected:

   void do_post() override {
        this->prepare(2);
        this->read([this]() {
            json::object body;
            if (this->get_json(body)) {

                string_t email;
                string_t username;
                string_t role;
                string_t password;

                util::json::find_string(body, "email", email, this->ec_);
                util::json::find_string(body, "username", username, this->ec_);
                util::json::find_string(body, "role", role, this->ec_);
                util::json::find_string(body, "password", password, this->ec_);

                if (this->ec_) return this->handle_response();

                bool email_invalid = email.empty() || username.size() > 50;
                bool username_invalid = username.empty() || username.size() > 50;
                bool role_invalid = role.empty() || role.size() > 30;
                bool password_invalid = password.empty() || password.size() > 100;

                if (email_invalid || username_invalid || role_invalid || password_invalid) {
                    fail(this->ec_, HTTPErrors::INVALID_PARAMETER);
                    return this->handle_response();
                } else {
                    this->prepare(1);
                    authentication_ = std::make_unique<auth::Authentication>(this->ec_, *this->connection_);
                    authentication_->duplicate_lookup([this, email, username, role, password](bool exists) {
                        post_result_ = std::make_unique<PostResult>(exists);
                        if(exists) return this->handle_response();
                        authentication_->create_user([this](uuid_t uuid) {
                            if(!this->ec_) post_result_->uuid = uuid;
                            return this->handle_response();
                        }, email, username, role, password);
                    }, email, username);
                }
            }
        });
    }

    void build_post_response() override {
        if (!post_result_->is_duplicate) {
            json::object response = {{"uuid", util::uuid::to_string(post_result_->uuid.value())}};
            do_write(std::move(http::response::build_json(this->header_parser_.get().version(), beast::http::status::ok, response)));
        } else {
            do_write(std::move(http::response::build_error(this->header_parser_.get().version(), beast::http::status::conflict)));
        }
    }
};

} // namespace leaper::resources

#endif //LEAPER_RESOURCES_INCLUDE_RESOURCES_TEST_USER_INFO_RESOURCE_HPP
