//
// Created by nils on 14/05/2020.
//

#ifndef LEAPER_MODELS_INCLUDE_MODELS_ADDRESS_HPP
#define LEAPER_MODELS_INCLUDE_MODELS_ADDRESS_HPP

#include <boost/json.hpp>

#include "leaper/models/model.hpp"
#include "leaper/errors.hpp"
#include "leaper/definitions.hpp"

namespace leaper::models {
struct Address : public Model {

    int32_t id;
    string_t street;
    string_t location;
    string_t canton;
    string_t country;
    string_t postal_code;

    Address() {};

    Address(int32_t id, const string_t &street, const string_t &location, const string_t &canton,
            const string_t &country, const string_t &postalCode);

	void from_json(std::error_code& ec, json::object& inp) override;

    	json::object to_json() override;

    void from_row(std::error_code&ec, row_t& row) override;
};
} // namespace leaper::models

#endif //LEAPER_MODELS_INCLUDE_MODELS_ADDRESS_HPP
