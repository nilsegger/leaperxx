//
// Created by nils on 14/05/2020.
//

#ifndef LEAPER_MODELS_INCLUDE_MODELS_USER_HPP
#define LEAPER_MODELS_INCLUDE_MODELS_USER_HPP

#include <boost/json.hpp>
#include <ozo/pg/types.h>

#include "leaper/models/model.hpp"
#include "leaper/models/address.hpp"
#include "leaper/definitions.hpp"
#include "leaper/errors.hpp"
#include "leaper/util/uuid.hpp"

namespace leaper::models {
struct User : public Model {
    uuid_t uuid;
    string_t display_name;
    std::optional<ozo::pg::timestamp> birthday;
    std::optional<string_t> gender;
    std::optional<Address> address;
    std::optional<uuid_t> profile_picture;
    std::optional<string_t> username;
    std::optional<string_t> email;

    User() {};
    User(uuid_t uuid);
    User(uuid_t& uuid);

    void from_row(std::error_code& ec, row_t &row) override;

    void from_json(std::error_code &ec, json::object &inp) override;

    boost::json::object to_json() override;

};
} // namespace leaper::models

#endif //LEAPER_MODELS_INCLUDE_MODELS_USER_HPP
