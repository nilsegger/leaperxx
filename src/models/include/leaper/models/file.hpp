//
// Created by nils on 09/06/2020.
//

#ifndef LEAPER_MODELS_INCLUDE_MODELS_FILE_HPP
#define LEAPER_MODELS_INCLUDE_MODELS_FILE_HPP

#include "leaper/definitions.hpp"

namespace leaper::models {
struct File {
    uuid_t uuid;
    uuid_t owner;
    bool public_;
    string_t path;
    string_t mime;

    File() {}

    File(uuid_t uuid) : uuid(uuid) {}

    File(uuid_t uuid, uuid_t owner, bool public_, string_t path, string_t mime);
};

} // namespace leaper::models


#endif //LEAPER_MODELS_INCLUDE_MODELS_FILE_HPP
