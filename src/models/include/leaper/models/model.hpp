//
// Created by nils on 14/05/2020.
//

#ifndef LEAPER_MODELS_INCLUDE_MODELS_MODEL_HPP
#define LEAPER_MODELS_INCLUDE_MODELS_MODEL_HPP

#include <boost/json.hpp>
#include <ozo/result.h>

#include "leaper/errors.hpp"

typedef ozo::row<std::decay_t<decltype(*std::declval<ozo::pg::result>())>> row_t;

namespace json = boost::json;

namespace leaper::models {

struct Model {

    virtual void from_json(std::error_code&ec, json::object& inp) = 0;

    virtual json::object to_json() = 0;

    virtual void from_row(std::error_code&ec, row_t& row) = 0;
};

} // namespace leaper::models

#endif //LEAPER_MODELS_INCLUDE_MODELS_MODEL_HPP
