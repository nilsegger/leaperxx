#include "leaper/models/user.hpp"
#include "leaper/util/json.hpp"
#include "leaper/util/chrono.hpp"
#include "leaper/sql/client.hpp"
#include "leaper/sql/io.hpp"

leaper::models::User::User(uuid_t uuid) : uuid(uuid) {}
leaper::models::User::User(uuid_t& uuid) : uuid(uuid) {}

void leaper::models::User::from_row(std::error_code& ec, row_t &row) {
        sql::io::read(ec, row[0], uuid);
        sql::io::read(ec, row[1], display_name);
        sql::io::read(ec, row[2], birthday);
        sql::io::read(ec, row[3], gender);
        sql::io::read(ec, row[4], profile_picture);

        auto address_iter = row.find("id");
        if(address_iter != row.end() && !address_iter->is_null()) {
            address = Address();
            sql::io::read(ec, *address_iter, address->id);
            sql::io::read(ec, *(++address_iter), address->street);
            sql::io::read(ec, *(++address_iter), address->location);
            sql::io::read(ec, *(++address_iter), address->canton);
            sql::io::read(ec, *(++address_iter), address->country);
            sql::io::read(ec, *(++address_iter), address->postal_code);
        }

        auto username_iter = row.find("username");
        if(username_iter != row.end()) {
            sql::io::read(ec, *username_iter, username);
        }

        auto email_iter = row.find("email");
        if(email_iter != row.end()) {
            sql::io::read(ec, *email_iter, email);
        }
    }

void leaper::models::User::from_json(std::error_code &ec, json::object &inp) {
        util::json::find_string(inp, "display_name", display_name, ec);
        util::json::find_string_optional(inp, "gender", gender, ec);

        util::json::find_timestamp_optional(inp, "birthday", birthday, ec);

        std::optional<json::object> address_container;
        util::json::find_object_optional(inp, "address", address_container, ec);
        if(address_container) {
            address = Address();
            address->from_json(ec, address_container.value());
        }
}

json::object leaper::models::User::to_json() {
        json::object result {
                {"uuid", util::uuid::to_string(uuid)},
                {"display_name", display_name},
        };

        if(birthday) result.emplace("birthday", util::chrono::to_seconds(birthday.value()));
        if(gender) result.emplace("gender", gender.value());
        if(address) result.emplace("address", address->to_json());
        if(email) result.emplace("email", email.value());
        if(username) result.emplace("username", username.value());
        if(profile_picture) result.emplace("profile_picture", util::uuid::to_string(profile_picture.value()));

        return result;
}

