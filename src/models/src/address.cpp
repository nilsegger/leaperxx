#include "leaper/models/address.hpp"
#include "leaper/util/json.hpp"
#include "leaper/sql/io.hpp"
#include "leaper/sql/client.hpp"

leaper::models::Address::Address(int32_t id, const string_t &street, const string_t &location, const string_t &canton, const string_t &country, const string_t &postalCode)
	: id(id), street(street), location(location), canton(canton), country(country), postal_code(postalCode)
{

}


void leaper::models::Address::from_json(std::error_code& ec, json::object& inp) {
	util::json::find_string(inp, "street", street, ec);
        util::json::find_string(inp, "location", location, ec);
        util::json::find_string(inp, "canton", canton, ec);
        util::json::find_string(inp, "country", country, ec);
        util::json::find_string(inp, "postal_code", postal_code, ec);

}

json::object leaper::models::Address::to_json() {
	return {
                {"street", street},
                {"location", location},
                {"canton", canton},
                {"country", country},
                {"postal_code", postal_code}
        };
}

void leaper::models::Address::from_row(std::error_code&ec, row_t& row) {
	sql::io::read(ec, row[0], this->id);
	sql::io::read(ec, row[1], this->street);
	sql::io::read(ec, row[2], this->location);
	sql::io::read(ec, row[3], this->canton);
	sql::io::read(ec, row[4], this->country);
	sql::io::read(ec, row[5], this->postal_code);
}
