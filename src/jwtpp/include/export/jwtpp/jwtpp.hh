// The MIT License (MIT)
//
// Copyright (c) 2016-2020 Artur Troian
//
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#pragma once

#include <memory>
#include <functional>
#include <vector>
#include <string>
#include <sstream>
#include <iostream>

#include <boost/json.hpp>

#include <openssl/crypto.h>
#include <openssl/ossl_typ.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/ec.h>
#include <openssl/ecdsa.h>
#include <openssl/sha.h>
#include <openssl/objects.h>
#include <openssl/pem.h>

#include "leaper/auth/jwt_errors.hpp"
#include "leaper/util/json.hpp"

#if __cplusplus >= 201703L
#   define __NODISCARD [[nodiscard]]
#else
#   define __NODISCARD
#endif

#if OPENSSL_VERSION_NUMBER >= 0x10101000L
#   define JWTPP_SUPPORTED_EDDSA
#endif

namespace jwtpp {

class claims;
class crypto;
class hmac;
class rsa;
class ecdsa;

#if defined(_MSC_VER) && (_MSC_VER < 1700)
enum alg_t {
#else
enum class alg_t {
#endif // defined(_MSC_VER) && (_MSC_VER < 1700)
	NONE = 0,
	HS256,
	HS384,
	HS512,
	RS256,
	RS384,
	RS512,
	ES256,
	ES384,
	ES512,
	PS256,
	PS384,
	PS512,
#if defined(JWTPP_SUPPORTED_EDDSA)
	EdDSA,
#endif // defined(JWTPP_SUPPORTED_EDDSA)
	UNKNOWN
};

#if defined(_MSC_VER) && (_MSC_VER < 1700)
#   define final

	typedef std::shared_ptr<class claims>   sp_claims;
	typedef std::unique_ptr<class claims>   up_claims;
	typedef std::shared_ptr<class crypto>   sp_crypto;
	typedef std::shared_ptr<class hmac>     sp_hmac;
	typedef std::shared_ptr<class rsa>      sp_rsa;
	typedef std::shared_ptr<class ecdsa>    sp_ecdsa;
	typedef std::shared_ptr<RSA>            sp_rsa_key;
	typedef std::shared_ptr<EC_KEY>         sp_ecdsa_key;
#else
	using sp_claims       = typename std::shared_ptr<class claims>;
	using up_claims       = typename std::unique_ptr<class claims>;
	using sp_crypto       = typename std::shared_ptr<class crypto>;
	using sp_hmac         = typename std::shared_ptr<class hmac>;
	using sp_rsa          = typename std::shared_ptr<class rsa>;
	using sp_ecdsa        = typename std::shared_ptr<class ecdsa>;
	using sp_rsa_key      = typename std::shared_ptr<RSA>;
	using sp_ecdsa_key    = typename std::shared_ptr<EC_KEY>;
	using sp_evp_key      = typename std::shared_ptr<EVP_PKEY>;

	using sp_evp_md_ctx   = typename std::shared_ptr<EVP_MD_CTX>;
	using sp_evp_pkey_ctx = typename std::shared_ptr<EVP_PKEY_CTX>;
#endif // defined(_MSC_VER) && (_MSC_VER < 1700)

template <class T>
class secure_allocator : public std::allocator<T> {
public:
	template <class U>
	struct rebind {
		typedef secure_allocator<U> other;
	};

	secure_allocator() noexcept = default;

	secure_allocator(const secure_allocator &) noexcept
		: std::allocator<T>()
	{}

	template <class U>
	explicit secure_allocator(const secure_allocator<U> &) noexcept {}

	void deallocate(T *p, std::size_t n) noexcept {
		OPENSSL_cleanse(p, n);
		std::allocator<T>::deallocate(p, n);
	}
};

using secure_string = std::basic_string<char, std::char_traits<char>, secure_allocator<char>>;

class b64 final {
private:
	static const boost::json::string base64_chars;

	static inline bool is_base64(unsigned char c) {
		return (isalnum(c) || (c == '+') || (c == '/'));
	}

	static void uri_enc(char *buf, size_t len);

	static void uri_dec(char *buf, size_t len);

public:
	/**
	 * \brief
	 *
	 * \param[out]  b64: output data in base64
	 * \param[in]
	 *
	 * \return  None
	 */
	static boost::json::string encode(const uint8_t *stream, size_t in_len);
	static boost::json::string encode(const std::vector<uint8_t> &stream);
	static boost::json::string encode(const std::vector<uint8_t> *stream);
	static boost::json::string encode(const boost::json::string &stream);

	static boost::json::string encode_uri(const uint8_t * stream, size_t in_len);
	static boost::json::string encode_uri(const boost::json::string &stream);
	static boost::json::string encode_uri(const std::vector<uint8_t> &stream);
	static boost::json::string encode_uri(const std::vector<uint8_t> * stream);

	static std::vector<uint8_t> decode(const char *in, size_t in_size);
	static std::vector<uint8_t> decode_uri(const char *in, size_t in_size);
	static boost::json::string decode(const boost::json::string &in);
	static boost::json::string decode_uri(const boost::json::string &in);
};

/**
 * \brief
 */
class digest final {
public:
#if defined(_MSC_VER) && (_MSC_VER < 1700)
	enum type {
#else
	enum class type {
#endif // defined(_MSC_VER) && (_MSC_VER < 1700)
		SHA256,
		SHA384,
		SHA512
	};

public:
	digest(digest::type type, const uint8_t *in_data, size_t in_size);
	~digest();

	__NODISCARD
	size_t size() const;

	uint8_t *data();

	__NODISCARD
	boost::json::string to_string() const;

public:
	static const EVP_MD *md(digest::type t) {
		switch (t) {
		default:
			[[fallthrough]];
		case type::SHA256:
			return EVP_sha256();
		case type::SHA384:
			return EVP_sha384();
		case type::SHA512:
			return EVP_sha512();
		}
	}

private:
	size_t                   _size;
	std::shared_ptr<uint8_t> _data;
};

/**
* \brief
*
* TODO https://github.com/troian/jwtpp/issues/38
*/
class claims final {
private:
	class has {
	public:
		explicit has(boost::json::object* c) : _claims(c) {}
	public:
		bool any(const boost::json::string& key) {
			auto iter = _claims->find(key);
			return iter != _claims->end();
		}	
		bool iss() { return any("iss"); }
		bool sub() { return any("sub"); }
		bool aud() { return any("aud"); }
		bool exp() { return any("exp"); }
		bool nbf() { return any("nbf"); }
		bool iat() { return any("iat"); }
		bool jti() { return any("jti"); }
	private:
		boost::json::object* _claims;
	};

	class check {
	public:
		explicit check(boost::json::object* c) : _claims(c) {}
	public:
		void any(std::error_code& ec, const boost::json::string &key, const boost::json::string &value) {
			auto obj = _claims->find(key);
			if(obj == _claims->end()) {
				return fail(ec, JWTErrors::JWT_INVALID_PAYLOAD);
			}
			boost::json::string* str = obj->value().is_string();
			if(str == nullptr) {
				return fail(ec, JWTErrors::CLAIM_VALUE_WRONG_TYPE);	
			}
			else if(*str != value) {
				fail(ec, JWTErrors::JWT_INVALID_PAYLOAD);
			}
		}

		void iss(std::error_code& ec, const boost::json::string &value) { return any(ec, "iss", value); }
		void sub(std::error_code& ec, const boost::json::string &value) { return any(ec, "sub", value); }
		void aud(std::error_code& ec, const boost::json::string &value) { return any(ec, "aud", value); }
		void nbf(std::error_code& ec, const boost::json::string &value) { return any(ec, "nbf", value); }
		void iat(std::error_code& ec, const boost::json::string &value) { return any(ec, "iat", value); }
		void jti(std::error_code& ec, const boost::json::string &value) { return any(ec, "jti", value); }
	private:
		boost::json::object* _claims;
	};

	class del {
	public:
		explicit del(boost::json::object* c) : _claims(c) {}
	public:
		void any(const boost::json::string &key) { _claims->erase(key); }
		void iss() { any("iss"); }
		void sub() { any("sub"); }
		void aud() { any("aud"); }
		void exp() { any("exp"); }
		void nbf() { any("nbf"); }
		void iat() { any("nbf"); }
		void jti() { any("jti"); }
	private:
		boost::json::object* _claims;
	};


	class get {
	public:
		explicit get(boost::json::object* c) : _claims(c) {}
	public:
		void any(std::error_code& ec, boost::json::string& value, const boost::json::string &key) {
			auto iter = _claims->find(key);
			if(iter == _claims->end()) return fail(ec, JWTErrors::CLAIM_KEY_MISSING);
			leaper::util::json::to_string(ec, value, &iter->value());
		}
			
		void any(std::error_code& ec, std::uint64_t& value, const boost::json::string &key) {
			auto iter = _claims->find(key);
			if(iter == _claims->end()) {
				return fail(ec, JWTErrors::CLAIM_KEY_MISSING);
			}
			leaper::util::json::to_uint64(ec, value, &iter->value());
		}
	
		void iss(std::error_code& ec, boost::json::string& value) { return any(ec, value, "iss"); }
		void sub(std::error_code& ec, boost::json::string& value) { return any(ec, value, "sub"); }
		void aud(std::error_code& ec, boost::json::string& value) { return any(ec, value, "aud"); }
		void exp(std::error_code& ec, std::uint64_t& value) { return any(ec, value, "exp"); }
		void nbf(std::error_code& ec, boost::json::string& value) { return any(ec, value, "nbf"); }
		void iat(std::error_code& ec, boost::json::string& value) { return any(ec, value, "iat"); }
		void jti(std::error_code& ec, boost::json::string& value) { return any(ec, value, "jti"); }
	private:
		boost::json::object*_claims;
	};

	class set {
	public:
		explicit set(boost::json::object* c) : _claims(c) {}
	public:
		void any(const boost::json::string &key, std::uint64_t value) { _claims->operator[](key) = value; }
		void any(const boost::json::string &key, std::int64_t value) { _claims->operator[](key) = value; }
		void any(const boost::json::string &key, double value) { _claims->operator[](key) = value; }
		void any(const boost::json::string &key, const boost::json::string &value);
		
		void iss(const boost::json::string &value) { any("iss", value); }
		void sub(const boost::json::string &value) { any("sub", value); }
		void aud(const boost::json::string &value) { any("aud", value); }
		void exp(const std::uint64_t& value) { any("exp", value); }
		void nbf(const boost::json::string &value) { any("nbf", value); }
		void iat(const boost::json::string &value) { any("iat", value); }
		void jti(const boost::json::string &value) { any("jti", value); }

	private:
		boost::json::object*_claims;
	};
public:
	/**
	 * \brief
	 */
	claims();

	/**
	 * \brief
	 *
	 * \param d
	 */
	explicit claims(const boost::json::string &d, bool b64 = false);

	/**
	 * \brief
	 *
	 * \param key
	 * \param value
	 *
	 * \return
	 */
	class claims::set &set() { return _set; }

	/**
	 * \brief
	 *
	 * \param key
	 *
	 * \return
	 */
	class claims::has &has() { return _has; }

	/**
	 * \brief
	 *
	 * \param key
	 *
	 * \return
	 */
	class claims::del &del() { return _del; }

	/**
	 * \brief
	 *
	 * \param key
	 *
	 * \return
	 */
	class claims::get &get() { return _get; }

	class claims::check &check() { return _check; }

	boost::json::string b64();

#if !(defined(_MSC_VER) && (_MSC_VER < 1700))
public:
	template <typename... _Args>
	static sp_claims make_shared(_Args&&... __args) {
		return std::make_shared<class claims>(__args...);
	}
#endif // !(defined(_MSC_VER) && (_MSC_VER < 1700))

private:
	boost::json::object _claims;

	class set   _set;
	class get   _get;
	class has   _has;
	class del   _del;
	class check _check;
};

class hdr final {
public:
	explicit hdr(jwtpp::alg_t alg);

	explicit hdr(const boost::json::string &data);

	boost::json::string b64();

private:
	boost::json::object _h;
};

/**
 * \brief
 */
#if defined(_MSC_VER) && (_MSC_VER < 1700)
	typedef std::shared_ptr<class jws> sp_jws;
#else
	using sp_jws = typename std::shared_ptr<class jws>;
#endif // defined(_MSC_VER) && (_MSC_VER < 1700)

class jws final {
public:
#if defined(_MSC_VER) && (_MSC_VER < 1700)
	typedef std::function<bool (sp_claims cl)> verify_cb;
#else
	using verify_cb = typename std::function<bool (sp_claims cl)>;
#endif // defined(_MSC_VER) && (_MSC_VER < 1700)

private:
	/**
	 * \brief
	 *
	 * \param alg
	 * \param data
	 * \param cl
	 * \param sig
	 */
	jws(alg_t a, const boost::json::string &data, sp_claims cl, const boost::json::string &sig);

public:
	/**
	 * \brief
	 *
	 * \return
	 */
	bool is_jwt();

	/**
	 * \brief
	 *
	 * \param c
	 * \param v
	 * \return
	 */
	bool verify(sp_crypto c, verify_cb v = nullptr);

	/**
	 * \brief
	 *
	 * \return
	 */
	class claims &claims() {
		return *(_claims.get());
	}

public:
	/**
	 * \brief
	 *
	 * \param b
	 *
	 * \return
	 */
	static sp_jws parse(const boost::json::string &b);

	/**
	 * \brief Sign content and return signature
	 *
	 * \param[in]  data - data to be signed
	 * \param[in]  c - crypto to sign with
	 *
	 * \return signature
	 */
	static boost::json::string sign(const boost::json::string &data, sp_crypto c);

	static boost::json::string sign_claims(class claims &cl, sp_crypto c);

	static boost::json::string sign_bearer(class claims &cl, sp_crypto c);
private:
	/**
	 * \brief
	 *
	 * \param text
	 * \param sep
	 * \return
	 */
	static std::vector<boost::json::string> tokenize(const boost::json::string &text, char sep);

private:
	alg_t        _alg;
	boost::json::string  _data;
	sp_claims    _claims;
	boost::json::string  _sig;
};

class crypto {
public:
	using password_cb = std::function<void(secure_string &pass, int rwflag)>;

protected:
	struct on_password_wrap {
		explicit on_password_wrap(password_cb cb)
			: cb(cb)
			, required(false)
		{}

		password_cb cb;
		bool        required;
	};

public:
	/**
	 * \brief
	 *
	 * \param alg
	 */
	explicit crypto(alg_t a = alg_t::NONE);

	virtual ~crypto() = 0;

public:
	/**
	 * \brief
	 *
	 * \return
	 */
	__NODISCARD
	alg_t alg() { return _alg; }

	__NODISCARD
	alg_t alg() const { return _alg; }

	/**
	 * \brief
	 *
	 * \param data
	 *
	 * \return
	 */
	virtual boost::json::string sign(const boost::json::string &data) = 0;

	/**
	 * \brief
	 *
	 * \param data
	 * \param sig
	 *
	 * \return
	 */
	virtual bool verify(const boost::json::string &data, const boost::json::string &sig) = 0;

public:
	/**
	 * \brief
	 *
	 * \param alg
	 *
	 * \return
	 */
	static const char *alg2str(alg_t a);

	static alg_t str2alg(const boost::json::string &a);


protected:
	static int hash2nid(digest::type type);

protected:
	alg_t          _alg;
	boost::json::object _hdr;
	digest::type   _hash_type;
};

class hmac : public crypto {
public:
	explicit hmac(const secure_string &secret, alg_t a = alg_t::HS256);

	~hmac() override = default;

public:
	boost::json::string sign(const boost::json::string &data) override;
	bool verify(const boost::json::string &data, const boost::json::string &sig) override;

#if !(defined(_MSC_VER) && (_MSC_VER < 1700))
public:
	template <typename... _Args>
	static sp_hmac make_shared(_Args&&... __args) {
		return std::make_shared<class hmac>(__args...);
	}
#endif // !(defined(_MSC_VER) && (_MSC_VER < 1700))

private:
	secure_string _secret;
};

class rsa : public crypto {
public:
	explicit rsa(sp_rsa_key key, alg_t a = alg_t::RS256);

	~rsa() override;

public:
	boost::json::string sign(const boost::json::string &data) override;
	bool verify(const boost::json::string &data, const boost::json::string &sig) override;

public:
#if !(defined(_MSC_VER) && (_MSC_VER < 1700))
	template <typename... _Args>
	static sp_rsa make_shared(_Args&&... __args) {
		return std::make_shared<class rsa>(__args...);
	}
#endif // !(defined(_MSC_VER) && (_MSC_VER < 1700))

	static sp_rsa_key gen(int size);

	static sp_rsa_key load_from_file(const boost::json::string &path, password_cb on_password = nullptr);

	static sp_rsa_key load_from_string(const boost::json::string &str) {
		auto key = std::shared_ptr<RSA>(RSA_new(), ::RSA_free);

		return key;
	}

private:
	static int password_loader(char *buf, int size, int rwflag, void *u);

private:
	sp_rsa_key   _r;
	unsigned int _key_size;
};

class ecdsa : public crypto {
public:
	explicit ecdsa(sp_ecdsa_key key, alg_t a = alg_t::ES256);

	~ecdsa() override = default;

public:
	boost::json::string sign(const boost::json::string &data) override;
	bool verify(const boost::json::string &data, const boost::json::string &sig) override;

public:

#if !(defined(_MSC_VER) && (_MSC_VER < 1700))
	template <typename... _Args>
	static sp_ecdsa make_shared(_Args&&... __args) {
		return std::make_shared<class ecdsa>(__args...);
	}
#endif // !(defined(_MSC_VER) && (_MSC_VER < 1700))

	static sp_ecdsa_key gen(int nid);

private:
	sp_ecdsa_key _e;
};

#if defined(JWTPP_SUPPORTED_EDDSA)
class eddsa : public crypto {
public:
	explicit eddsa(sp_evp_key key, alg_t a = alg_t::EdDSA);

	~eddsa() override = default;

public:
	boost::json::string sign(const boost::json::string &data) override;
	bool verify(const boost::json::string &data, const boost::json::string &sig) override;

public:

#if !(defined(_MSC_VER) && (_MSC_VER < 1700))
	template <typename... _Args>
	static sp_ecdsa make_shared(_Args&&... __args) {
		return std::make_shared<class ecdsa>(__args...);
	}
#endif // !(defined(_MSC_VER) && (_MSC_VER < 1700))

	static sp_evp_key gen();
	static sp_evp_key get_pub(sp_evp_key priv);

private:
	sp_evp_key _e;
};
#endif // defined(JWTPP_SUPPORTED_EDDSA)

class pss : public crypto {
public:
	explicit pss(sp_rsa_key key, alg_t a = alg_t::PS256);

	~pss() override = default;

public:
	boost::json::string sign(const boost::json::string &data) override;
	bool verify(const boost::json::string &data, const boost::json::string &sig) override;

private:
	sp_rsa_key _r;
	size_t     _key_size;
};

boost::json::string marshal(const boost::json::object &json);

boost::json::string marshal_b64(const boost::json::object &json);

boost::json::object unmarshal(const boost::json::string &in);

boost::json::object unmarshal_b64(const boost::json::string &b);

#if defined(_MSC_VER) && (_MSC_VER < 1700)
#   undef final
#endif

} // namespace jwtpp
