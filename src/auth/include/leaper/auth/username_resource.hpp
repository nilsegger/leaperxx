//
// Created by nils on 18/06/2020.
//

#ifndef LEAPER_USERNAME_RESOURCE_HPP
#define LEAPER_USERNAME_RESOURCE_HPP

#include "leaper/http/resources/string_resource.hpp"
#include "leaper/auth/authentication.hpp"

namespace beast = boost::beast;

namespace leaper::auth {
class UsernameResource : public http::StringResource {

    std::unique_ptr<auth::Authentication> authentication_;

    std::optional<bool> duplicate_username_;

protected:

    void do_authenticated_post() override;

    void build_authenticated_post_response() override;

    void build_get_response() override;

    void do_get() override;

public:

    using StringResource::StringResource;
};

}; // namespace leaper::auth

#endif //LEAPER_USERNAME_RESOURCE_HPP
