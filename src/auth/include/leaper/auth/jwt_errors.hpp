#ifndef LEAPER_LIBS_ERRORS_INCLUDE_JWT_ERRORS_HPP
#define LEAPER_LIBS_ERRORS_INCLUDE_JWT_ERRORS_HPP

#include <system_error>

enum class JWTErrors {
    SUCCESS = 0,

    // Internal Server Error
    JWT_FAILED_TO_CREATE_ACCESS_TOKEN,
    JWT_FAILED_TO_VERIFY_TOKEN,

    // Unauthorized
    JWT_INVALID_FORMAT=1000,
    JWT_INVALID_HEADER,
    JWT_INVALID_PAYLOAD,
    JWT_EXPIRED,
    JWT_INVALID_SIGNATURE,
    CLAIM_KEY_MISSING,
    CLAIM_VALUE_WRONG_TYPE
};

namespace std {
    template<>
    struct is_error_code_enum<JWTErrors> : true_type {};
}

namespace {
    struct JWTErrorCategories: std::error_category {
        const char* name() const noexcept override {
            return "JWT Error codes";
        }

        std::string message(int e) const override {
            switch (static_cast<JWTErrors>(e)) {
                case JWTErrors::SUCCESS:
                    return "There was no error but it was handled???";
                case JWTErrors::JWT_FAILED_TO_CREATE_ACCESS_TOKEN:
			return "Failed to create token.";
                case JWTErrors::JWT_FAILED_TO_VERIFY_TOKEN:
			return "Failed to verify token.";
                case JWTErrors::JWT_INVALID_FORMAT:
                    return "JWT has invalid format.";
                case JWTErrors::JWT_INVALID_HEADER:
                    return "JWT invalid header.";
                case JWTErrors::JWT_INVALID_PAYLOAD:
                    return "JWT invalid payload.";
                case JWTErrors::JWT_INVALID_SIGNATURE:
                    return "JWT invalid signature.";
                case JWTErrors::JWT_EXPIRED:
			return "Token expired.";
                case JWTErrors::CLAIM_KEY_MISSING:
			return "Claim key missing.";
                case JWTErrors::CLAIM_VALUE_WRONG_TYPE:
			return "Claim value is not of expected type.";
                default:
                    return "Unknown error code received. '" + std::to_string(e) + "'";
            }
        }
    };

    const JWTErrorCategories jwt_JWTErrors{};

    void fail(std::error_code& e, JWTErrors c){
        e = c;
    }

}

inline std::error_code make_error_code(JWTErrors e) {
    return {static_cast<int>(e), jwt_JWTErrors};
}

#endif //LEAPER_LIBS_ERRORS_INCLUDE_JWT_ERRORS_HPP


