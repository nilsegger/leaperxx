//
// Created by nils on 29/04/2020.
//

#ifndef LEAPER_LIBS_AUTH_INCLUDE_AUTH_RESOURCE_HPP
#define LEAPER_LIBS_AUTH_INCLUDE_AUTH_RESOURCE_HPP

#include "leaper/auth/authentication.hpp"
#include "leaper/http/resources/string_resource.hpp"

namespace beast = boost::beast;

namespace leaper::auth {

    class auth_resource : public http::StringResource {

        std::unique_ptr<Authentication> auth_;
        std::optional<string_t> access_token_, refresh_token_;

    protected:

        void do_post() override;

        void build_post_response() override;

        void do_put() override;

        void build_put_response() override;

        void do_authenticated_delete() override;

        void build_authenticated_delete_response() override;

    public:
        using StringResource::StringResource;

    };

}; // namespace leaper::auth

#endif //LEAPER_LIBS_AUTH_INCLUDE_AUTH_RESOURCE_HPP
