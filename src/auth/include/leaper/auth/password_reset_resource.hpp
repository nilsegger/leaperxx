//
// Created by nils on 18/06/2020.
//

#ifndef LEAPER_PASSWORD_RESET_RESOURCE_HPP
#define LEAPER_PASSWORD_RESET_RESOURCE_HPP

#include "leaper/http/resources/string_resource.hpp"
#include "leaper/auth/password_reset.hpp"

namespace beast = boost::beast;

namespace leaper::auth {
    class PasswordResetResource : public http::StringResource {

        std::unique_ptr<auth::PasswordReset> password_reset_;

        std::optional<bool> password_reset_confirmed_;
    protected:

        void do_post() override;

        // Sends mail
        void build_post_response() override;

        // Do actual update
        void do_put() override;

        void build_put_response() override;

    public:

        using StringResource::StringResource;

    };
} // namespace leaper::auth

#endif //LEAPER_PASSWORD_RESET_RESOURCE_HPP
