//
// Created by nils on 16/06/2020.
//

#ifndef LEAPER_LIBS_AUTH_INCLUDE_AUTH_REGISTER_RESOURCE_HPP
#define LEAPER_LIBS_AUTH_INCLUDE_AUTH_REGISTER_RESOURCE_HPP

#include <boost/json.hpp>

#include "leaper/http/resources/string_resource.hpp"
#include "leaper/auth/register.hpp"

namespace json = boost::json;
namespace beast = boost::beast;

namespace leaper::auth {

    class RegisterResource : public http::StringResource {

        std::unique_ptr<Register> register_;
        std::optional<bool> email_verified_;

    protected:

        virtual string_t get_role() = 0;

        void do_get() override;

        void build_get_response() override;

        void do_post() override;

        void build_post_response() override;

    public:

        using StringResource::StringResource;

    };

}; // namespace leaper::auth
#endif //LEAPER_LIBS_AUTH_INCLUDE_AUTH_REGISTER_RESOURCE_HPP
