//
// Created by nils on 20/04/2020.
//

#ifndef LEAPER_LIBS_AUTH_INCLUDE_AUTH_JWT_HPP
#define LEAPER_LIBS_AUTH_INCLUDE_AUTH_JWT_HPP

#include "jwtpp/jwtpp.hh"
#include "leaper/definitions.hpp"
#include "leaper/auth/jwt_errors.hpp"
#include "leaper/io_errors.hpp"

#define REFRESH_TOKEN_LENGTH 500

namespace json = boost::json;

namespace leaper::auth {
    class JSONWebToken {

    private:
    	static jwtpp::sp_rsa_key kPublicKey;
	static jwtpp::sp_crypto kPublicCrypto;
	static string_t getKeyPath(string_t file);
    	static jwtpp::sp_rsa_key loadPrivateKey();
    	static jwtpp::sp_rsa_key loadPublicKey();
	static jwtpp::sp_crypto getPrivateCrypto();
	static jwtpp::sp_crypto getPublicCrypto();

	jwtpp::sp_jws jws_;

    public:

        void create_access_token(std::error_code &ec, string_t& token, jwtpp::claims& claims, unsigned int &expire_in_s);

        void verify_access_token(std::error_code&ec, jwtpp::claims& claims, string_t &token);

        void create_refresh_token(std::error_code &ec, string_t &token);
    };
}; // namespace leaper::auth


#endif //LEAPER_LIBS_AUTH_INCLUDE_AUTH_JWT_HPP
