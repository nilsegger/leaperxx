//
// Created by nils on 16/06/2020.
//

#ifndef LEAPER_REGISTER_HPP
#define LEAPER_REGISTER_HPP

#include <system_error>

#include <ozo/query_builder.h>

#include "leaper/definitions.hpp"
#include "leaper/sql/client.hpp"
#include "leaper/util/uuid.hpp"
#include "leaper/util/chrono.hpp"
#include "leaper/config.hpp"

using namespace ozo::literals;

typedef std::tuple<uuid_t, bool, std::optional<string_t>, std::optional<ozo::pg::timestamp>> activation_row_t;

namespace leaper::auth {

    class Register {

        std::error_code &ec_;
        connection_t &connection_;

        std::optional<std::vector<activation_row_t>> register_buffer_;
        std::optional<ozo::result> activate_buffer_;

        string_t create_activation_code();

        string_t create_activation_link(string_t &code);

        void send_activation_mail(string_t email, string_t activationCode);

        template<typename CALLBACK_T>
        void create_activation(CALLBACK_T callback, string_t role, string_t email) {
            // Email has not yet been registered.
            uuid_t uuid = util::uuid::create();
            string_t activationCode = create_activation_code();
            auto expires = util::chrono::now() + std::chrono::seconds(900); // 15 Minutes
            auto stmt = "INSERT INTO logins(uuid, role, email, email_activated, email_activation_code, email_activation_expires) VALUES("_SQL
                        + uuid + ", "_SQL
                        + role + ", "_SQL
                        + email + ", "_SQL
                        + false + ", "_SQL
                        + activationCode + ", "_SQL
                        + expires + ")"_SQL;
            sql::execute(ec_, [this, email, activationCode, callback]() {
                send_activation(callback, email, activationCode);
            }, connection_, std::move(stmt));
        }

        template<typename CALLBACK_T>
        void update_activation(CALLBACK_T callback, uuid_t uuid, string_t email) {
            string_t activationCode = create_activation_code();
            auto expires = util::chrono::now() + std::chrono::seconds(900); // 15 Minutes
            auto stmt = "UPDATE logins SET email_activation_code="_SQL + activationCode + ", email_activation_expires="_SQL + expires + " WHERE uuid="_SQL + uuid;
            sql::execute(ec_, [this, email, activationCode, callback]() {
                send_activation(callback, email, activationCode);
            }, connection_, std::move(stmt));
        }

        template<typename CALLBACK_T>
        void send_activation(CALLBACK_T callback, string_t email, string_t activationCode) {
            if (!ec_) {
                send_activation_mail(email, activationCode);
            }
            callback();
        }

    public:

        Register(std::error_code &ec_, connection_t &connection_);

        template<typename CALLBACK_T>
        void register_email(CALLBACK_T callback, string_t &email, string_t &role) {
            /*
             * Checks if email has already been used. If true, sends mail to user for password reset,
             * otherwise creates activation code and sends it to the user.
             */

            register_buffer_.emplace();
            auto stmt = "SELECT uuid, email_activated, email_activation_code, email_activation_expires FROM logins WHERE email="_SQL + email + " LIMIT 1"_SQL;
            sql::fetch_row(ec_, [this, email, callback, role](std::optional<activation_row_t> row) {
                if (!ec_ && row && std::get<1>(row.value())) {
                    // TODO Email has already been activated, send password reset mail
                    callback();
                } else if (!ec_) {
                    if (!row) {
                        create_activation(callback, role, email);
                    } else {
                        // Email has already been registered, check expire date of activation code and resend code
                        bool activationCodeValid = std::get<2>(row.value()).has_value() && std::get<3>(row.value()).has_value() && std::get<3>(row.value()) > util::chrono::now();
                        if (activationCodeValid) {
                            send_activation(callback, email, std::get<2>(row.value()).value());
                        } else {
                            update_activation(callback, std::get<0>(row.value()), email);
                        }
                    }
                } else {
                    callback();
                }
            }, connection_, std::move(stmt), *register_buffer_);
        }

        template<typename CALLBACK_T>
        void activate_email(CALLBACK_T callback, string_t &activationCode) {
            activate_buffer_.emplace();
            auto stmt = "SELECT uuid FROM logins WHERE email_activated="_SQL + false + " AND email_activation_code="_SQL + activationCode + " AND email_activation_expires>"_SQL + util::chrono::now();
            sql::template fetch_value<uuid_t>(ec_, [this, callback](std::optional<uuid_t> uuid) {
                if (!uuid) return callback(false);
                else {
                    auto stmt = "UPDATE logins SET email_activated="_SQL + true + " WHERE uuid="_SQL + uuid;
                    sql::execute(ec_, [callback]() {
                        callback(true);
                    }, connection_, std::move(stmt));
                }
            }, connection_, std::move(stmt), *activate_buffer_);
        }
    };

} // namespace leaper::auth

#endif //LEAPER_REGISTER_HPP
