//
// Created by nils on 16/06/2020.
//

#ifndef LEAPER_EMAIL_UPDATE_HPP
#define LEAPER_EMAIL_UPDATE_HPP

#include "leaper/definitions.hpp"
#include "leaper/sql/client.hpp"
#include "leaper/sql/transaction.hpp"
#include "leaper/util/chrono.hpp"
#include "leaper/util/random.hpp"

namespace leaper::auth {

    class EmailUpdateVerification {
        std::error_code &ec_;
        connection_t &connection_;


        typedef std::tuple<string_t, string_t, ozo::pg::timestamp> email_update_row_t;
        std::optional<std::vector<email_update_row_t>> update_buffer_;

        std::optional<ozo::result> exists_buffer_;

        typedef std::tuple<uuid_t, string_t> verification_row_t;
        std::optional<std::vector<verification_row_t>> verification_buffer_;

        string_t create_email_update_verification_link(string_t verificationCode);

        void send_email_update_verification(string_t email, string_t verificationCode);
        void send_duplicate_email_notice(string_t email);

        template<typename CALLBACK_T>
        void create_verification(CALLBACK_T callback, uuid_t uuid, string_t email) {
            string_t verificationCode;
            util::random::generate_secure_string(ec_, 1000, verificationCode);
            auto expires = util::chrono::now() + std::chrono::seconds(900); // 15 minutes

            if (ec_) callback();

            auto stmt = "INSERT INTO email_update_verification(uuid, email, confirmation_code, confirmation_expires) VALUES("_SQL + uuid + ", "_SQL + email + ", "_SQL + verificationCode + ", "_SQL +
                        expires + ")"_SQL;
            sql::execute(this->ec_, [this, callback, email, verificationCode]() {
                if (!ec_) {
                    send_email_update_verification(email, verificationCode);
                }
                callback();
            }, connection_, std::move(stmt));
        }

        template<typename CALLBACK_T>
        void update_verification(CALLBACK_T callback, uuid_t uuid, string_t email) {
            string_t verificationCode;
            util::random::generate_secure_string(ec_, 1000, verificationCode);
            auto expires = util::chrono::now() + std::chrono::seconds(900); // 15 minutes

            if (ec_) callback();

            auto stmt = "UPDATE email_update_verification SET email="_SQL + email + ", confirmation_code="_SQL + verificationCode + ", confirmation_expires="_SQL + expires + " WHERE uuid="_SQL + uuid;
            sql::execute(ec_, [this, callback, email, verificationCode]() {
                if (!ec_) {
                    send_email_update_verification(email, verificationCode);
                }
                callback();
            }, connection_, std::move(stmt));
        }

    public:

        EmailUpdateVerification(std::error_code &ec, connection_t &connection);

        template<typename CALLBACK_T>
        void update_email(CALLBACK_T callback, const uuid_t &uuid, string_t &email) {
            exists_buffer_.emplace();
            auto stmt = "SELECT EXISTS(SELECT 1 FROM logins WHERE email="_SQL + email + ")"_SQL;
            sql::exists(this->ec_, [this, uuid, email, callback](bool exists) {
                if (exists) {
                    // Someone is already using this email, send mock mail
                    send_duplicate_email_notice(email);
                    callback();
                } else {
                    // Email is available for use
                    update_buffer_.emplace();
                    auto stmt = "SELECT email, confirmation_code, confirmation_expires FROM email_update_verification WHERE uuid="_SQL + uuid;
                    sql::fetch_row(ec_, [this, callback, uuid, email](std::optional<email_update_row_t> row) {
                        if (ec_) return callback();
                        if (!row) {
                            create_verification(callback, uuid, email);
                        }
                            // Here it is important to check that emails still match
                        else if (row && std::get<0>(row.value()) == email && std::get<2>(row.value()) > util::chrono::now()) {
                            send_email_update_verification(email, std::get<1>(row.value()));
                            callback();
                        } else {
                            update_verification(callback, uuid, email);
                        }
                    }, connection_, std::move(stmt), *update_buffer_);
                }
            }, connection_, std::move(stmt), *exists_buffer_);
        }

        template<typename CALLBACK_T>
        void verify_email_update(CALLBACK_T callback, string_t &verificationCode) {

            verification_buffer_.emplace();
            auto stmt = "SELECT uuid, email FROM email_update_verification WHERE confirmation_code="_SQL + verificationCode + " AND confirmation_expires > "_SQL + util::chrono::now() + " LIMIT 1"_SQL;
            sql::fetch_row(ec_, [this, callback](std::optional<verification_row_t> row) {
                if (!ec_ && row) {
                    sql::transaction::begin(ec_, [this, callback, row](auto &conn) {
                        if (ec_) return callback(false);
                        auto stmt = "UPDATE logins SET email="_SQL + std::get<1>(row.value()) + " WHERE uuid="_SQL + std::get<0>(row.value());
                        sql::transaction::execute(ec_, [this, callback, row](auto &conn) {
                            if (ec_) return sql::transaction::rollback(ec_, std::bind(callback, false), conn);
                            // Delete by email is necessary in case someone else tried verifying this email
                            auto stmt = "DELETE FROM email_update_verification WHERE email="_SQL + std::get<1>(row.value());
                            sql::transaction::final_execute(ec_, std::bind(callback, true), conn, std::move(stmt)); // delete email_update_verification_row
                        }, conn, std::move(stmt)); // update login row to insert new email
                    }, connection_); // transaction begin
                } else {
                    callback(false);
                }
            }, connection_, std::move(stmt), *verification_buffer_); // fetches row with uuid and new email

        }
    };

} // namespace leaper::auth

#endif //LEAPER_EMAIL_UPDATE_HPP
