//
// Created by nils on 17/06/2020.
//

#ifndef LEAPER_EMAIL_UPDATE_RESOURCE_HPP
#define LEAPER_EMAIL_UPDATE_RESOURCE_HPP

#include "leaper/http/resources/string_resource.hpp"
#include "leaper/auth/email_update.hpp"

namespace beast = boost::beast;

namespace leaper::auth {

class EmailUpdateResource : public http::StringResource {

	    std::unique_ptr<EmailUpdateVerification> controller_;
	    std::optional<bool> verified;
	protected:
	    void do_authenticated_post() override;

	    void build_authenticated_post_response() override;

	    void build_get_response() override;

	    void do_get() override;

	public:

	    using StringResource::StringResource;
	};

} // namespace leaper::auth

#endif //LEAPER_EMAIL_UPDATE_RESOURCE_HPP
