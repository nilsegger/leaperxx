//
// Created by nils on 02/05/2020.
//

#ifndef LEAPER_LIBS_AUTH_INCLUDE_AUTH_HASHER_HPP
#define LEAPER_LIBS_AUTH_INCLUDE_AUTH_HASHER_HPP

#include "leaper/definitions.hpp"
#include "leaper/io_errors.hpp"

namespace leaper::auth {
    class Hasher {

        const size_t HASH_LENGTH;
        const size_t SALT_LENGTH;
        const size_t ENCODE_LENGTH;
        const uint32_t T_COST;
        const uint32_t M_COST;
        const uint32_t PARALLELISM;

        void create_salt(uint8_t *arr);

    public:
        explicit Hasher(size_t hash_length = 32, size_t salt_length = 16, size_t encode_length = 128,
                        uint32_t t_cost = 4, uint32_t m_cost = (1 << 16), uint32_t parallelism = 1);

        void hash_pwd(std::error_code &ec, string_t &hash, const char *password, const size_t password_length);

        void verify_pwd(std::error_code &ec, bool &authenticated, const char *password, const size_t password_length,
                        const char *hash);
    };

}; // namespace leaper::auth
#endif //LEAPER_LIBS_AUTH_INCLUDE_AUTH_HASHER_HPP
