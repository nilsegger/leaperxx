//
// Created by nils on 20/04/2020.
//

#ifndef LEAPER_LIBS_AUTH_INCLUDE_AUTH_HPP
#define LEAPER_LIBS_AUTH_INCLUDE_AUTH_HPP

#include <string>
#include <iostream>
#include <boost/json.hpp>
#include <vector>

#include "leaper/sql/client.hpp"
#include "leaper/definitions.hpp"
#include "leaper/auth/hasher.hpp"
#include "leaper/auth/jwt.hpp"
#include "leaper/config.hpp"
#include "leaper/util/random.hpp"
#include "leaper/util/chrono.hpp"
#include "leaper/util/uuid.hpp"

namespace json = boost::json;

typedef std::tuple<uuid_t, std::optional<string_t>, std::optional<string_t>, std::optional<string_t>, std::optional<ozo::pg::timestamp>, std::optional<ozo::pg::boolean>> update_row_t;
typedef std::tuple<uuid_t, string_t> account_row_t;

namespace leaper::auth {

    class Authentication {

        std::error_code &ec_;
        connection_t &connection_;

        Hasher hasher_;
        JSONWebToken jwt_;

        std::optional<std::vector<update_row_t>> verification_buffer_;
        std::optional<std::vector<account_row_t>> refresh_token_verification_buffer_;
        std::optional<ozo::result> duplicate_lookup_buffer_;

        string_t create_access_token(boost::uuids::uuid &uuid, string_t &role);

        template<typename CALLBACK_T>
        void create_refresh_token(CALLBACK_T callback, uuid_t &uuid) {
            string_t refresh_token;
            jwt_.create_refresh_token(ec_, refresh_token);
            if (ec_) return;
            std::chrono::system_clock::time_point refresh_token_expires =
                    util::chrono::now() + std::chrono::seconds(30 * 86400);
            auto stmt = "UPDATE logins SET refresh_token="_SQL + refresh_token +
                        ", refresh_token_expires="_SQL + refresh_token_expires +
                        ", refresh_token_revoked="_SQL + false + "WHERE uuid="_SQL + uuid;
            sql::execute(ec_, std::bind(callback, refresh_token), connection_, std::move(stmt));
        }

        void fake_hash();

    public:

        Authentication(std::error_code &ec, connection_t &connection);

        template<typename CALLBACK_T>
        void duplicate_lookup(CALLBACK_T callback, json::string email, json::string username) {
            duplicate_lookup_buffer_.emplace();
            auto stmt = "SELECT EXISTS(SELECT 1 FROM logins WHERE email="_SQL + email + " OR username="_SQL + username + ")"_SQL;
            sql::exists(ec_, callback, connection_, std::move(stmt), *duplicate_lookup_buffer_);
        }

        template<typename CALLBACK_T>
        void username_duplicate_lookup(CALLBACK_T callback, json::string& username) {
            duplicate_lookup_buffer_.emplace();
            auto stmt = "SELECT EXISTS(SELECT 1 FROM logins WHERE username="_SQL + username + ")"_SQL;
            sql::exists(ec_, callback, connection_, std::move(stmt), *duplicate_lookup_buffer_);
        }

        template<typename CALLBACK_T>
        void create_user(CALLBACK_T callback, string_t email, string_t username, string_t role, string_t password) {
            /*
             * Method for creating test users. Email activated will be set to true.
             */
            auto uuid = util::uuid::create();
            string_t password_hash;
            hasher_.hash_pwd(ec_, password_hash, password.c_str(), password.size());
            if (!ec_) {
                assert(!email.empty());
                assert(!username.empty());
                assert(!role.empty());
                assert(!password_hash.empty());

                auto stmt = "INSERT INTO public.logins(uuid, email, email_activated, username, role, password) VALUES ("_SQL
                            + ""_SQL + uuid
                            + ", "_SQL + email
                            + ", "_SQL + true
                            + ", "_SQL + username
                            + ", "_SQL + role
                            + ", "_SQL + password_hash
                            + ");"_SQL;
                sql::execute(ec_, std::bind(callback, uuid), connection_, stmt);
            } else {
                (callback)(util::uuid::nil());
            }
        }

        template<typename CALLBACK_T>
        void update_username(CALLBACK_T callback, const uuid_t& uuid, const json::string& username) {
            auto stmt = "UPDATE logins SET username="_SQL + username + " WHERE uuid="_SQL + uuid;
            sql::execute(ec_, callback, connection_, std::move(stmt));
        }

        template<typename CALLBACK_T>
        void update_password(CALLBACK_T callback, uuid_t& uuid, json::string& password) {
            string_t password_hash;
            hasher_.hash_pwd(ec_, password_hash, password.c_str(), password.size());
            auto stmt = "UPDATE logins SET password="_SQL + password_hash + ", refresh_token_revoked="_SQL + true + " WHERE uuid="_SQL + uuid;
            sql::execute(ec_, callback, connection_, std::move(stmt));
        }

        template<typename CALLBACK_T>
        void force_update_email(CALLBACK_T callback, uuid_t& uuid, json::string& email) {
            // force disables email confirmation
            auto stmt = "UPDATE logins SET email="_SQL + email + " WHERE uuid="_SQL + uuid;
            sql::execute(ec_, callback, connection_, std::move(stmt));
        }

        template<typename CALLBACK_T>
        void verify_login(CALLBACK_T callback, json::string &username, json::string &password) {
            assert(!username.empty());
            assert(!password.empty());

            verification_buffer_.emplace();
            auto stmt = "SELECT uuid, role, password, refresh_token, refresh_token_expires, refresh_token_revoked FROM logins WHERE username="_SQL +
                        username +
                        " AND email_activated="_SQL + true + "LIMIT 1"_SQL;

            sql::fetch_row(ec_, [this, callback, password](std::optional<update_row_t> row) {

                // Check if row exists
                if (!ec_ && row) {
                    bool verified = false;
                    hasher_.verify_pwd(ec_, verified, password.c_str(), password.size(), std::get<2>(row.value())->c_str());

                    if (ec_ || !verified) goto abort;

                    string_t access_token = create_access_token(std::get<0>(row.value()), std::get<1>(row.value()).value());

                    if (ec_) goto abort;

                    // Checks if refresh token has been revoked or expired
                    auto now = util::chrono::now();
                    if (!std::get<3>(row.value()).has_value() || std::get<5>(row.value()).value_or(true) || std::get<4>(row.value()).value_or(now) <= now) {
                        return create_refresh_token([callback, access_token](string_t refresh_token) {
                            callback(access_token, refresh_token);
                        }, std::get<0>(row.value()));
                    } else {
                        return callback(access_token, std::get<3>(row.value()));
                    }
                }

                abort:

                if (!ec_) fake_hash();
                callback(std::nullopt, std::nullopt);

            }, connection_, std::move(stmt), *verification_buffer_);
        };

        template<typename CALLBACK_T>
        void verify_refresh_token(CALLBACK_T callback, json::string &refresh_token) {
            assert(!refresh_token.empty());
            refresh_token_verification_buffer_.emplace();
            auto stmt = "SELECT uuid, role FROM logins WHERE email_activated="_SQL + true + "AND refresh_token="_SQL + refresh_token +
                        " AND refresh_token_expires >"_SQL + util::chrono::now() +
                        "AND refresh_token_revoked=false LIMIT 1"_SQL;
            sql::fetch_row(ec_, [this, callback](std::optional<account_row_t> row) {
                if (!ec_ && row) {
                    return callback(create_access_token(std::get<0>(row.value()), std::get<1>(row.value())));
                }
                callback(std::nullopt);
            }, connection_, std::move(stmt), *refresh_token_verification_buffer_);
        }

        template<typename CALLBACK_T>
        void revoke_refresh_token(CALLBACK_T callback, boost::uuids::uuid &uuid) {
            assert(!uuid.is_nil());
            auto stmt = "UPDATE logins SET refresh_token=null, refresh_token_expires=null, refresh_token_revoked=true WHERE uuid="_SQL + uuid;
            sql::execute(ec_, callback, connection_, std::move(stmt));
        }
    };

}; // namespace leaper::auth

#endif //LEAPER_LIBS_AUTH_INCLUDE_AUTH_HPP
