//
// Created by nils on 16/06/2020.
//

#ifndef LEAPER_PASSWORD_RESET_HPP
#define LEAPER_PASSWORD_RESET_HPP

#include "leaper/definitions.hpp"
#include "leaper/sql/transaction.hpp"
#include "leaper/util/random.hpp"
#include "leaper/util/chrono.hpp"
#include "leaper/auth/hasher.hpp"

namespace leaper::auth {

    class PasswordReset {

        typedef std::tuple<uuid_t, string_t, ozo::pg::timestamp> row_t;

        std::error_code &ec_;
        connection_t &connection_;

        std::optional<ozo::result> login_lookup_buffer_;
        std::optional<std::vector<row_t>> row_buffer_;
        std::optional<ozo::result> confirmation_lookup_buffer_;

        void send_confirmation_code(const string_t &email, const string_t &confirmationCode);

        void send_signup_mail(const string_t &email);

        template<typename CALLBACK_T>
        void create_password_reset(CALLBACK_T callback, const uuid_t &uuid, const string_t &email) {
            string_t confirmationCode;
            util::random::generate_secure_string(ec_, 1000, confirmationCode);

            if (ec_) return callback();

            auto expires = util::chrono::now() + std::chrono::seconds(900); // 15 Minutes

            auto stmt = "INSERT INTO password_reset_verification(uuid, confirmation_code, confirmation_expires) VALUES ("_SQL + uuid + ", "_SQL + confirmationCode + ", "_SQL + expires + ")"_SQL;
            sql::execute(ec_, [this, callback, email, confirmationCode]() {
                if (!ec_) {
                    send_confirmation_code(email, confirmationCode);
                }
                callback();
            }, connection_, std::move(stmt));
        }

        template<typename CALLBACK_T>
        void update_password_reset(CALLBACK_T callback, const uuid_t& uuid, const string_t &email) {
            string_t confirmationCode;
            util::random::generate_secure_string(ec_, 1000, confirmationCode);

            if (ec_) return callback();

            auto expires = util::chrono::now() + std::chrono::seconds(900); // 15 Minutes

            auto stmt = "UPDATE password_reset_verification SET confirmation_code="_SQL + confirmationCode + ", confirmation_expires="_SQL + expires + " WHERE uuid="_SQL + uuid;
            sql::execute(ec_, [this, callback, email, confirmationCode]() {
                if (!ec_) {
                    send_confirmation_code(email, confirmationCode);
                }
                callback();
            }, connection_, std::move(stmt));
        }

    public:

        PasswordReset(std::error_code &ec, connection_t &connection);

        template<typename CALLBACK_T>
        void reset_password(CALLBACK_T callback, string_t &email) {
            // Sends email with code required for password reset to user
            login_lookup_buffer_.emplace();
            auto stmt = "SELECT uuid FROM logins WHERE email="_SQL + email + " LIMIT 1"_SQL;
            sql::template fetch_value<uuid_t>(ec_, [this, callback, email](std::optional<uuid_t> account) {
                if (ec_) return callback();
                if (account) {
                    row_buffer_.emplace();
                    auto stmt = "SELECT uuid, confirmation_code, confirmation_expires FROM password_reset_verification WHERE uuid="_SQL + account.value();
                    sql::fetch_row(ec_, [this, callback, email, account](std::optional<row_t> row) {
                        if (ec_) return callback();
                        if (!row) {
                            // Create new password reset
                            create_password_reset(callback, account.value(), email);
                        } else if (row && std::get<2>(row.value()) > util::chrono::now()) {
                            // update existing password reset
                            send_confirmation_code(email, std::get<1>(row.value()));
                            callback();
                        } else {
                            update_password_reset(callback, account.value(), email);
                        }
                    }, connection_, std::move(stmt), *row_buffer_); // check if there is an existing password reset
                } else {
                    send_signup_mail(email);
                    callback();
                }
            }, connection_, std::move(stmt), *login_lookup_buffer_); // lookup account
        }

        template<typename CALLBACK_T>
        void verify_password_reset(CALLBACK_T callback, const string_t &activationCode, const string_t &password) {

            sql::transaction::begin(ec_, [this, activationCode, password, callback](auto &&conn) {
                if(ec_) return callback(false);

                confirmation_lookup_buffer_.emplace();
                auto stmt = "SELECT uuid FROM password_reset_verification WHERE confirmation_code="_SQL + activationCode + " AND confirmation_expires > "_SQL + util::chrono::now() + " LIMIT 1"_SQL;
                sql::transaction::template fetch_value<uuid_t>(ec_, [this, password, callback](auto &&conn, std::optional<uuid_t> uuid) {
                    if(ec_) return sql::transaction::rollback(ec_, std::bind(callback, false), conn);
                    if(uuid) {
                        // update password
                        string_t hash;
                        Hasher hasher_;
                        hasher_.hash_pwd(ec_, hash, password.c_str(), password.size());

                        if(ec_) return sql::transaction::rollback(ec_, std::bind(callback, false), conn);

                        auto stmt = "UPDATE logins SET password="_SQL + hash + ", refresh_token_revoked="_SQL + true + " WHERE uuid="_SQL + uuid.value();
                        sql::transaction::execute(ec_, [this, callback, uuid](auto && conn) {
                            if(ec_) return sql::transaction::rollback(ec_, std::bind(callback, false), conn);
                            auto stmt = "DELETE FROM password_reset_verification WHERE uuid="_SQL + uuid.value();
                            sql::transaction::final_execute(ec_, std::bind(callback, true), conn, std::move(stmt));
                        }, conn, std::move(stmt));
                    } else {
                        callback(false);
                    }
                }, conn, std::move(stmt), *confirmation_lookup_buffer_); // lookup if code is valid
            }, connection_); // transaction begin

        }
    };

} // namespace leaper::auth

#endif //LEAPER_PASSWORD_RESET_HPP
