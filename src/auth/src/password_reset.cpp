//
// Created by nils on 18/06/2020.
//

#include "leaper/auth/password_reset.hpp"
#include "leaper/mail/curl_mail.hpp"
#include "leaper/mail/builder.hpp"
#include "leaper/config.hpp"

leaper::auth::PasswordReset::PasswordReset(std::error_code &ec, connection_t &connection) : ec_(ec), connection_(connection) {

}

void leaper::auth::PasswordReset::send_confirmation_code(const string_t& email, const string_t& confirmationCode) {
    string_t resetPassword("#");
    string_t content("<h1>Hi there! Forgot your password?</h1>");
    content.append("<p>If you are trying to reset your mail, please click this <a href=\"");
    content.append(resetPassword);
    content.append("\">link</a>.</p>");
    content.append("<p>");
    content.append(confirmationCode);
    content.append("</p>");

    mail::MailBuilder builder(ec_, Config::get("fqdn"), mail::Sender(Config::get("mailSenderName"), Config::get("mailSenderEmail")), email, "Password reset");
    builder.add_html_content(content);
    std::vector<string_t> payload = builder.render();

    if(ec_) return;

    mail::CurlMail::send(ec_, email, std::move(payload));
}

void leaper::auth::PasswordReset::send_signup_mail(const string_t& email) {
    string_t content("<h1>Signup to leaper!</h1>");

    mail::MailBuilder builder(ec_, Config::get("fqdn"), mail::Sender(Config::get("mailSenderName"), Config::get("mailSenderEmail")), email, "Mail Confirmation");
    builder.add_html_content(content);
    std::vector<string_t> payload = builder.render();

    if(ec_) return;

    mail::CurlMail::send(ec_, email, std::move(payload));
}
