//
// Created by nils on 18/06/2020.
//

#include "leaper/auth/password_reset_resource.hpp"
#include "leaper/http/response_builder.hpp"
#include "leaper/util/json.hpp"

void leaper::auth::PasswordResetResource::do_post() {
    this->prepare(1);
    this->read([this]() {
        json::object body;
        if(this->get_json(body)) {
            string_t email;
            util::json::find_string(body, "email", email, this->ec_);
            if(this->ec_) return this->handle_response();

            password_reset_ = std::make_unique<leaper::auth::PasswordReset>(this->ec_, *this->connection_);
            password_reset_->reset_password(std::bind(&PasswordResetResource::handle_response, this), email);
        }
    });
}

void leaper::auth::PasswordResetResource::build_post_response() {
    this->do_write(std::move(http::response::build_successful(this->version())));
}

void leaper::auth::PasswordResetResource::do_put() {
    this->prepare(1);
    this->read([this]() {
        json::object body;
        if(this->get_json(body)) {
            string_t confirmationCode, password;
            util::json::find_string(body, "code", confirmationCode, this->ec_);
            util::json::find_string(body, "password", password, this->ec_);
            if(this->ec_) return this->handle_response();

            password_reset_ = std::make_unique<leaper::auth::PasswordReset>(this->ec_, *this->connection_);
            password_reset_->verify_password_reset([this](bool passwordReset) {
                password_reset_confirmed_ = passwordReset;
                this->handle_response();
            }, confirmationCode, password);
        }
    });
}

void leaper::auth::PasswordResetResource::build_put_response() {
    if(password_reset_confirmed_.value()) {
        this->do_write(std::move(http::response::build_successful(this->version())));
    } else {
        this->do_write(std::move(http::response::build_not_found(this->version())));
    }
}


