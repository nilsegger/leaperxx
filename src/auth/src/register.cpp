//
// Created by nils on 16/06/2020.
//

#include "leaper/auth/register.hpp"
#include "leaper/util/random.hpp"
#include "leaper/mail/builder.hpp"
#include "leaper/mail/curl_mail.hpp"

leaper::auth::Register::Register(std::error_code &ec, connection_t &connection) :ec_(ec), connection_(connection) {

}

string_t leaper::auth::Register::create_activation_code() {
    string_t result;
    util::random::generate_secure_string(ec_, 1000, result);
    return result;
}

string_t leaper::auth::Register::create_activation_link(string_t& code) {
   string_t url = Config::get("self");
   url.append("register?verify=");
   url.append(code);
   return url;
}

void leaper::auth::Register::send_activation_mail(string_t email, string_t activationCode) {
    string_t activationUrl = create_activation_link(activationCode);
    string_t content("<h1>Welcome to leaper!</h1>");
    content.append("<p>To continue signing in, please click on this <a href=\"");
    content.append(activationUrl);
    content.append("\">link</a>.</p>");

    mail::MailBuilder builder(ec_, Config::get("fqdn"), mail::Sender(Config::get("mailSenderName"), Config::get("mailSenderEmail")), email, "Leaper Registration");
    builder.add_html_content(content);
    std::vector<string_t> payload = builder.render();

    if(ec_) return;

    mail::CurlMail::send(ec_, email, std::move(payload));
}

