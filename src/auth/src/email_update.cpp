//
// Created by nils on 16/06/2020.
//

#include "leaper/auth/email_update.hpp"
#include "leaper/config.hpp"
#include "leaper/mail/curl_mail.hpp"
#include "leaper/mail/builder.hpp"

leaper::auth::EmailUpdateVerification::EmailUpdateVerification(std::error_code &ec, connection_t &connection) : ec_(ec), connection_(connection) {

}

string_t leaper::auth::EmailUpdateVerification::create_email_update_verification_link(string_t verificationCode) {
    string_t url = Config::get("self");
    url.append("accounts/email?verify=");
    url.append(verificationCode);
    return url;
}

void leaper::auth::EmailUpdateVerification::send_email_update_verification(string_t email, string_t verificationCode) {
    string_t verificationUrl = create_email_update_verification_link(verificationCode);
    string_t content("<h1>Hi there! Is this the correct email?</h1>");
    content.append("<p>You recently tried to update your email address. If this is you, continue with this <a href=\"");
    content.append(verificationUrl);
    content.append("\">link</a>.</p>");

    mail::MailBuilder builder(ec_, Config::get("fqdn"), mail::Sender(Config::get("mailSenderName"), Config::get("mailSenderEmail")), email, "Mail Confirmation");
    builder.add_html_content(content);
    std::vector<string_t> payload = builder.render();

    if(ec_) return;

    mail::CurlMail::send(ec_, email, std::move(payload));
}

void leaper::auth::EmailUpdateVerification::send_duplicate_email_notice(string_t email) {
    string_t content("<h1>Hi there!</h1><p>Someone is trying to reuse this email for a leaper account. If this was you, please request a password reset.</p>");

    mail::MailBuilder builder(ec_, Config::get("fqdn"), mail::Sender(Config::get("mailSenderName"), Config::get("mailSenderEmail")), email, "Mail Confirmation");
    builder.add_html_content(content);
    std::vector<string_t> payload = builder.render();

    if(ec_) return;

    mail::CurlMail::send(ec_, email, std::move(payload));
}
