//
// Created by nils on 18/06/2020.
//

#include "leaper/auth/email_update_resource.hpp"
#include "leaper/http/response_builder.hpp"
#include "leaper/util/json.hpp"

void leaper::auth::EmailUpdateResource::do_authenticated_post() {

    uuid_t account = this->path_parameters_.get_uuid(this->ec_, "uuid");

    if (this->ec_) return handle_response();

    if (this->requester_->role != "admin" && account != this->requester_->uuid) {
        fail(this->ec_, HTTPErrors::FORBIDDEN);
        return this->handle_response();
    }

    this->prepare(1);
    this->read([this, account]() {
                   json::object body;
                   if (this->get_json(body)) {
                       string_t email;
                       util::json::find_string(body, "email", email, this->ec_);

                       if (this->ec_) return this->handle_response();

                       controller_ = std::make_unique<leaper::auth::EmailUpdateVerification>(this->ec_, *this->connection_);

                       controller_->update_email([this]() {
                           this->handle_response();
                       }, account, email);
                   }
               }
    );
}

void leaper::auth::EmailUpdateResource::build_authenticated_post_response() {
    this->do_write(std::move(http::response::build_successful(this->version())));
}

void leaper::auth::EmailUpdateResource::do_get() {

    string_t verificationCode = this->query_parameters_.get_string(this->ec_, "verify");
    if (this->ec_) return this->handle_response();

    this->prepare(1);

    controller_ = std::make_unique<leaper::auth::EmailUpdateVerification>(this->ec_, *this->connection_);
    controller_->verify_email_update([this](bool verified) {
        this->verified = verified;
        this->handle_response();
    }, verificationCode);
}

void leaper::auth::EmailUpdateResource::build_get_response() {
    if (verified.value()) {
        this->do_write(std::move(http::response::build_successful(this->version())));
    } else {
        this->do_write(std::move(http::response::build_not_found(this->version())));
    }
}

