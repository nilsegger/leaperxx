//
// Created by nils on 16/06/2020.
//

#include "leaper/auth/authentication.hpp"

leaper::auth::Authentication::Authentication(std::error_code &ec, connection_t &connection) : ec_(ec), connection_(connection) {

}

string_t leaper::auth::Authentication::create_access_token(boost::uuids::uuid &uuid, string_t &role) {
    unsigned int expire_in_s = 900;

	jwtpp::claims claims;
	claims.set().any("uuid", util::uuid::to_string(uuid));
	claims.set().any("role", role);
    	string_t token;
    	jwt_.create_access_token(this->ec_, token, claims, expire_in_s);
    	return token;
}

void leaper::auth::Authentication::fake_hash() {
    // Since we know that the user doesnt exist,
    // but a hacker does not,
    // a fake has has to be executed,
    // to reduce time difference between found and not found users.
    bool fake_verified;
    string_t fake_pwd = "fake password!?";
    string_t fake_hash = "$argon2id$v=19$m=16,t=2,p=1$R3ZCa1N0MlBZaHhwNnZzTA$RRLXUqZd41mwJcob0OUoVg";
    hasher_.verify_pwd(ec_, fake_verified, fake_pwd.c_str(), fake_pwd.size(), fake_hash.c_str());
}
