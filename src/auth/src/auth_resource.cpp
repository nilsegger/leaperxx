//
// Created by nils on 16/06/2020.
//

#include <boost/beast/http.hpp>

#include "leaper/auth/auth_resource.hpp"
#include "leaper/http/response_builder.hpp"
#include "leaper/auth/jwt.hpp"

void leaper::auth::auth_resource::do_post() {
    this->prepare(1);
    this->read([this]() {
        json::object body;
        if (this->get_json(body)) {

            json::string username;
            json::string password;
            util::json::find_string(body, "username", username, this->ec_);
            util::json::find_string(body, "password", password, this->ec_);

            if (this->ec_) return this->handle_response();

            if (username.empty() || username.size() > 50 || password.empty() || password.size() > 100) {
                fail(this->ec_, HTTPErrors::INVALID_PARAMETER);
                return this->handle_response();
            } else {
                this->prepare(1);
                auth_ = std::make_unique<Authentication>(this->ec_, *this->connection_);
                auth_->verify_login([this](std::optional<string_t> access_token, std::optional<string_t> refresh_token) {
                    if (!this->ec_) {
                        access_token_ = std::move(access_token);
                        refresh_token_ = std::move(refresh_token);
                    }
                    this->handle_response();
                }, username, password);
            }
        }
    });
}

void leaper::auth::auth_resource::build_post_response() {
    if (access_token_ && refresh_token_) {
        json::object response = {{"access_token",  access_token_.value()},
                                 {"refresh_token", refresh_token_.value()}};
        this->do_write(std::move(http::response::build_json(this->header_parser_.get().version(), beast::http::status::ok, response)));
    } else {
        this->do_write(std::move(http::response::build_error(this->header_parser_.get().version(), beast::http::status::unauthorized)));
    }
}

void leaper::auth::auth_resource::do_put() {
    this->prepare(1);
    this->read([this]() {
        json::object body;
        if (this->get_json(body)) {
            json::string refresh_token;
            util::json::find_string(body, "refresh_token", refresh_token, this->ec_);

            if (this->ec_) return this->handle_response();

            if (refresh_token.empty() || refresh_token.size() > 5000) {
                fail(this->ec_, HTTPErrors::INVALID_PARAMETER);
                return this->handle_response();
            } else {
                auth_ = std::make_unique<Authentication>(this->ec_, *this->connection_);
                auth_->verify_refresh_token([this](std::optional<string_t> accessToken) {
                    access_token_ = std::move(accessToken);
                    this->handle_response();
                }, refresh_token);
            }
        }
    });
}

void leaper::auth::auth_resource::build_put_response() {
    if (access_token_) {
        json::object response = {{"access_token", access_token_.value()}};
        this->do_write(std::move(http::response::build_json(this->header_parser_.get().version(), beast::http::status::ok, response)));
    } else {
        this->do_write(std::move(http::response::build_error(this->header_parser_.get().version(),
                                                                          beast::http::status::unauthorized)));
    }
}

void leaper::auth::auth_resource::do_authenticated_delete() {
    this->prepare(1);
    auth_ = std::make_unique<Authentication>(this->ec_, *this->connection_);

    auth_->revoke_refresh_token(std::bind(&auth_resource::handle_response, this), this->requester_->uuid);
}

void leaper::auth::auth_resource::build_authenticated_delete_response() {
    this->do_write(std::move(http::response::build_successful(this->header_parser_.get().version())));
}
