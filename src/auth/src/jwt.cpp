//
// Created by nils on 20/04/2020.
//

#include <openssl/rand.h>

#include "leaper/auth/jwt.hpp"
#include "leaper/util/base64.hpp"
#include "leaper/util/chrono.hpp"
#include "leaper/util/json.hpp"
#include "leaper/config.hpp"

jwtpp::sp_rsa_key leaper::auth::JSONWebToken::kPublicKey = nullptr;
jwtpp::sp_crypto leaper::auth::JSONWebToken::kPublicCrypto = nullptr;

string_t leaper::auth::JSONWebToken::getKeyPath(string_t file) {
	string_t path = Config::get("rsaKeys");
	path.append("/");
	path.append(file);
	return path;
}

jwtpp::sp_rsa_key leaper::auth::JSONWebToken::loadPrivateKey() {
	return jwtpp::rsa::load_from_file(getKeyPath("key.private.pem"));
}

jwtpp::sp_rsa_key leaper::auth::JSONWebToken::loadPublicKey() {
	// Public key can be kept in memory without worrying about it
	if(!kPublicKey) {
		kPublicKey = jwtpp::rsa::load_from_file(getKeyPath("key.public.pem"));
	}
	return kPublicKey;
}

jwtpp::sp_crypto leaper::auth::JSONWebToken::getPrivateCrypto() {
	return std::make_shared<jwtpp::rsa>(loadPrivateKey());
}

jwtpp::sp_crypto leaper::auth::JSONWebToken::getPublicCrypto() {
	if(!kPublicCrypto) {
		kPublicCrypto = std::make_shared<jwtpp::rsa>(loadPublicKey());
	}
	return kPublicCrypto; 
}

void leaper::auth::JSONWebToken::create_access_token(std::error_code &ec, string_t& token, jwtpp::claims& claims, unsigned int &expire_in_s) {
	try {
	auto crypto = getPrivateCrypto();
	claims.set().aud(Config::get("audience"));
	claims.set().iss(Config::get("issuer"));
	std::chrono::time_point expires = util::chrono::now() + std::chrono::seconds(expire_in_s);
	claims.set().exp(util::chrono::to_seconds(expires));
	token = jwtpp::jws::sign_claims(claims, crypto);
	} catch(std::exception& exc) {
		fail(ec, JWTErrors::JWT_FAILED_TO_CREATE_ACCESS_TOKEN);
	}
}

void leaper::auth::JSONWebToken::verify_access_token(std::error_code&ec, jwtpp::claims& claims, string_t &token) {
	try {
		jws_ = jwtpp::jws::parse(token);
		if(!jws_->verify(getPublicCrypto())) return fail(ec, JWTErrors::JWT_INVALID_SIGNATURE);
		claims = jws_->claims();
	} catch(std::exception& exc) {
		return fail(ec, JWTErrors::JWT_FAILED_TO_VERIFY_TOKEN);
	}
	claims.check().aud(ec, Config::get("audience"));
	claims.check().iss(ec, Config::get("issuer"));
	if(ec) return;
	std::uint64_t expireTimestamp;
	claims.get().exp(ec, expireTimestamp);
	if(ec) return;
	if(expireTimestamp < util::chrono::seconds_since_epoch()) return fail(ec, JWTErrors::JWT_EXPIRED);
}	

void leaper::auth::JSONWebToken::create_refresh_token(std::error_code &ec, string_t &token) {
	// https://www.openssl.org/docs/man1.0.2/man3/RAND_bytes.html
	unsigned char buffer[REFRESH_TOKEN_LENGTH];
	int result = RAND_bytes(buffer, REFRESH_TOKEN_LENGTH);
	if (result == 0) {
	    return fail(ec, IOErrors::OPENSSL_RAND_BYTES_ERROR);
	} else {
	    char *base64_buffer;
	    size_t base64_size;
	    util::base64::encode(ec, buffer, REFRESH_TOKEN_LENGTH, &base64_buffer, &base64_size);
	    if (ec) return;
	    token = json::string(base64_buffer, base64_size);
	}
}
