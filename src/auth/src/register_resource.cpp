//
// Created by nils on 16/06/2020.
//

#include "leaper/auth/register_resource.hpp"
#include "leaper/http/response_builder.hpp"
#include "leaper/util/json.hpp"

void leaper::auth::RegisterResource::do_get() {
    this->prepare(1);

    string_t activationCode = query_parameters_.get_string(this->ec_, "verify");

    if(ec_) this->handle_response();

    register_ = std::make_unique<Register>(this->ec_, *this->connection_);
    register_->activate_email([this](bool verified) {
        email_verified_ = verified;
        return this->handle_response();
    }, activationCode);
}

void leaper::auth::RegisterResource::build_get_response() {
    assert(email_verified_);
    if(email_verified_.value()) {
        this->do_write(std::move(http::response::build_successful(this->version())));
    } else {
        this->do_write(std::move(http::response::build_error(this->version(), beast::http::status::bad_request)));
    }
}

void leaper::auth::RegisterResource::do_post() {
    this->prepare(1);
    this->read([this]() {
        json::object body;
        if(this->get_json(body)) {

            string_t email;
            util::json::find_string(body, "email", email, this->ec_);

            if(this->ec_) return this->handle_response();

            string_t role = get_role();

            register_ = std::make_unique<Register>(this->ec_, *this->connection_);
            register_->register_email([this]() {
                this->handle_response();
            }, email, role);
        }
    });
}

void leaper::auth::RegisterResource::build_post_response() {
    this->do_write(std::move(http::response::build_successful(version())));
}
