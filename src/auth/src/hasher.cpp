//
// Created by nils on 02/05/2020.
//

#include <argon2.h>

#include "leaper/auth/hasher.hpp"
#include "leaper/util/base64.hpp"
#include "leaper/util/uuid.hpp"

namespace leaper::auth {
    Hasher::Hasher(size_t hash_length, size_t salt_length, size_t encode_length, uint32_t t_cost, uint32_t m_cost,
                   uint32_t parallelism) : HASH_LENGTH(hash_length), SALT_LENGTH(salt_length),
                                           ENCODE_LENGTH(encode_length), T_COST(t_cost), M_COST(m_cost),
                                           PARALLELISM(parallelism) {

    }

    void Hasher::hash_pwd(std::error_code &ec, string_t &hash, const char *password, const size_t password_length) {

        uint8_t salt[SALT_LENGTH];
        create_salt(salt);

        char encoded[ENCODE_LENGTH];

        int res = argon2id_hash_encoded(T_COST, M_COST, PARALLELISM, password, password_length, salt, SALT_LENGTH,
                                        HASH_LENGTH, encoded, ENCODE_LENGTH);

        if (res == Argon2_ErrorCodes::ARGON2_OK) {
            hash = string_t(encoded);
        } else {
            return fail(ec, IOErrors::ARGON2_ERROR);
        }

    }

    void Hasher::create_salt(uint8_t *arr) {
        auto uuid = util::uuid::create();
        for (uint8_t *pos = uuid.begin(); pos != uuid.end(); pos++, arr++) {
            *arr = *pos;
        }
    }

    void
    Hasher::verify_pwd(std::error_code &ec, bool &authenticated, const char *password, const size_t password_length,
                       const char *hash) {
        int res = argon2id_verify(hash, password, password_length);
        if (res == Argon2_ErrorCodes::ARGON2_OK) authenticated = true;
        else if (res == Argon2_ErrorCodes::ARGON2_VERIFY_MISMATCH) authenticated = false;
        else {
            authenticated = false;
            return fail(ec, IOErrors::ARGON2_ERROR);
        }
    }

}; // namespace auth
