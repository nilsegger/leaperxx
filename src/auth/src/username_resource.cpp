//
// Created by nils on 18/06/2020.
//

#include "leaper/auth/username_resource.hpp"
#include "leaper/http/response_builder.hpp"

void leaper::auth::UsernameResource::do_get() {
    this->prepare(1);

    string_t username = this->query_parameters_.get_string(this->ec_, "q");
    if (this->ec_) return this->handle_response();

    authentication_ = std::make_unique<auth::Authentication>(this->ec_, *this->connection_);
    authentication_->username_duplicate_lookup([this](bool exists) {
        duplicate_username_ = exists;
        this->handle_response();
    }, username);
}

void leaper::auth::UsernameResource::build_get_response() {
    if (duplicate_username_.value()) {
        this->do_write(std::move(http::response::build_error(this->version(), beast::http::status::conflict)));
    } else {
        this->do_write(std::move(http::response::build_successful(this->version())));
    }
}

void leaper::auth::UsernameResource::do_authenticated_post() {
    uuid_t account = this->path_parameters_.get_uuid(this->ec_, "uuid");
    if (this->ec_) return this->handle_response();

    if (account != this->requester_->uuid && this->requester_->role != "admin") {
        fail(this->ec_, HTTPErrors::FORBIDDEN);
        return this->handle_response();
    }

    this->prepare(1);
    this->read([this, account]() {
        json::object body;
        if (this->get_json(body)) {
            string_t username;
            util::json::find_string(body, "username", username, this->ec_);

            if (this->ec_) return this->handle_response();

            authentication_ = std::make_unique<auth::Authentication>(this->ec_, *this->connection_);
            authentication_->update_username(std::bind(&leaper::auth::UsernameResource::handle_response, this), account, username);
        }
    });
}

void leaper::auth::UsernameResource::build_authenticated_post_response() {
    this->do_write(std::move(http::response::build_successful(this->version())));
}



