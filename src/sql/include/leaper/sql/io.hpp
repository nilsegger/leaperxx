//
// Created by nils on 20/05/2020.
//

#ifndef LEAPER_LIBS_SQL_INCLUDE_SQL_IO_HPP
#define LEAPER_LIBS_SQL_INCLUDE_SQL_IO_HPP

#include <ozo/result.h>
#include <ozo/io/recv.h>

#include "leaper/sql/sql_errors.hpp"

namespace leaper::sql {
    namespace io {

        template<typename TARGET_T, typename Result>
        void read(std::error_code &ec, ozo::value<Result> value, TARGET_T &target) {
            try {
                ozo::recv(value, ozo::empty_oid_map(), target);
            } catch(std::exception& exc) {
                fail(ec, SQLErrors::SQL_IO_ERROR);
            }
        }

        template<typename TARGET_T, typename Result>
        void read(std::error_code &ec, ozo::value<Result> &value, TARGET_T &target) {
            try {
                ozo::recv(value, ozo::empty_oid_map(), target);
            } catch(std::exception& exc) {
                fail(ec, SQLErrors::SQL_IO_ERROR);
            }
        }
   }
}; // namespace sql

#endif //LEAPER_LIBS_SQL_INCLUDE_SQL_IO_HPP
