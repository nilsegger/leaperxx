//
// Created by nils on 17/05/2020.
//

#ifndef LEAPER_LIBS_SQL_INCLUDE_SQL_TRANSACTION_HPP
#define LEAPER_LIBS_SQL_INCLUDE_SQL_TRANSACTION_HPP

#include <vector>

#include <boost/asio.hpp>

#include <ozo/transaction.h>
#include <ozo/impl/transaction.h>
#include <ozo/transaction_options.h>

#include "leaper/sql/sql_errors.hpp"
#include "leaper/sql/client.hpp"

namespace leaper::sql {

    namespace transaction {

        template<typename CONNECTION_T>
        void abort(std::error_code &ec, ozo::error_code &ozo_ec, CONNECTION_T &conn, SQLErrors error_code) {
            sql::handle_error(ozo_ec, conn);
            fail(ec, error_code);
        }

        template<typename CONNECTION_T, typename CALLBACK_T>
        void begin(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection) {
            ozo::begin(std::move(connection), [callback, &ec](ozo::error_code ozo_ec, auto &&conn) {
                if (ozo_ec) abort(ec, ozo_ec, conn, SQLErrors::SQL_TRANSACTION_BEGIN_ERROR);
                callback(conn);
            });
        }

        template<typename CONNECTION_T, typename CALLBACK_T>
        void commit(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection) {
            ozo::commit(std::move(connection), [&ec, callback](ozo::error_code ozo_ec, auto &&conn) {
                if (ozo_ec) abort(ec, ozo_ec, conn, SQLErrors::SQL_TRANSACTION_COMMIT_ERROR);
                callback();
            });
        }

        template<typename CALLBACK_T, typename CONNECTION_T>
        void rollback(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection) {
            ozo::rollback(std::move(connection), [&ec, callback](ozo::error_code ozo_ec, auto &&conn) {
                if (ozo_ec) abort(ec, ozo_ec, conn, SQLErrors::SQL_TRANSACTION_BEGIN_ERROR);
                callback();
            });
        }

        template<typename CALLBACK_T, typename CONNECTION_T, typename QUERY_T>
        void execute(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &conn, QUERY_T &&statement) {
            ozo::execute(conn, statement, [&ec, callback](ozo::error_code ozo_ec, auto &&conn) {
                if (ozo_ec) {
                    sql::handle_error(ozo_ec, conn, ec);
                }
                callback(conn);
            });
        }

        template<typename CALLBACK_T, typename CONNECTION_T, typename QUERY_T>
        void final_execute(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &conn, QUERY_T &&statement) {
            /*
             * Commit or executes rollback depending on ec
             */
            execute(ec, [&ec, callback](auto &&conn) {
                if (ec) return sql::transaction::rollback(ec, callback, conn);
                sql::transaction::commit(ec, callback, conn);
            }, conn, statement);
        }

        template<typename VALUE_T, typename CALLBACK_T, typename CONNECTION_T, typename QUERY_T>
        void fetch_value(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &conn, QUERY_T &&statement, ozo::result &buffer) {
            ozo::request(conn, statement, std::ref(buffer), [&buffer, callback, &ec](ozo::error_code ozo_ec, auto &&conn) {
                if (ozo_ec) {
                    sql::handle_error(ozo_ec, conn, ec);
                    callback(conn, std::nullopt);
                } else {
                    assert(buffer.size() <= 1); // Assert single row
                    if (buffer.size() == 1) {
                        assert(buffer[0].size() == 1); // Assert single column
                        std::optional<VALUE_T> result;
                        sql::io::read(ec, buffer[0][0], result);
                        callback(conn, result);
                    } else {
                        callback(conn, std::nullopt);
                    }
                }
            });
        }

        /*
        template<typename CALLBACK_T, typename CONNECTION_T, typename QUERY_T, typename RESULT_T>
        void fetch_row(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &conn, QUERY_T &&statement, std::vector<RESULT_T> &buffer) {
            ozo::request(conn, statement, ozo::into(buffer), [&ec, callback, &buffer](ozo::error_code ozo_ec, auto &&conn) {
                if (ozo_ec) {
                    sql::handle_error(ozo_ec, conn, ec);
                    callback(conn, std::nullopt);
                } else {
                    assert(buffer.size() <= 1);
                    if (buffer.size() == 1) {
                        callback(conn, std::optional<RESULT_T>(buffer[0]));
                    } else {
                        callback(conn, std::nullopt);
                    }
                }
            });
        }*/

    }; // namespace transaction

}; // namespace sql
#endif //LEAPER_LIBS_SQL_INCLUDE_SQL_TRANSACTION_HPP
