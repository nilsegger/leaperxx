#ifndef LEAPER_LIBS_ERRORS_INCLUDE_SQL_ERRORS_HPP
#define LEAPER_LIBS_ERRORS_INCLUDE_SQL_ERRORS_HPP

#include <system_error>

enum class SQLErrors {
    SUCCESS = 0,

    //  Internal Server Error
    SQL_ERROR,
    SQL_IO_ERROR,
    SQL_TRANSACTION_BEGIN_ERROR,
    SQL_TRANSACTION_COMMIT_ERROR,
    SQL_TRANSACTION_ROLLBACK_ERROR,
};

namespace std {
    template<>
    struct is_error_code_enum<SQLErrors> : true_type {};
}

namespace {
    struct SQLErrorsCategory : std::error_category {
        const char* name() const noexcept override {
            return "SQLErrors";
        }

        std::string message(int e) const override {
            switch (static_cast<SQLErrors>(e)) {
                case SQLErrors::SUCCESS:
                    return "There was no error but it was handled???";
               	case SQLErrors::SQL_ERROR:
                    return "SQL Error, see output for more information.";
                case SQLErrors::SQL_IO_ERROR:
                    return "SQL io error.";
                case SQLErrors::SQL_TRANSACTION_BEGIN_ERROR:
                    return "SQL transaction begin failed.";
                case SQLErrors::SQL_TRANSACTION_COMMIT_ERROR:
                    return "Failed to commit transaction.";
                case SQLErrors::SQL_TRANSACTION_ROLLBACK_ERROR:
                    return "Failed to rollback transaction.";
                default:
                    return "Unknown error code received. '" + std::to_string(e) + "'";
            }
        }
    };

    const SQLErrorsCategory sql_codes_categories {};

    void fail(std::error_code& e, SQLErrors c){
        e = c;
    }

}

inline std::error_code make_error_code(SQLErrors e) {
    return {static_cast<int>(e), sql_codes_categories};
}

#endif //LEAPER_LIBS_ERRORS_INCLUDE_SQL_ERRORS_HPP


