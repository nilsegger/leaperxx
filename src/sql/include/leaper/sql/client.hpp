//
// Created by nils on 14/04/2020.
//

#ifndef LEAPER_LIBS_SQL_INCLUDE_SQL_CLIENT_HPP
#define LEAPER_LIBS_SQL_INCLUDE_SQL_CLIENT_HPP

#include <boost/asio.hpp>
#include <boost/json.hpp>

#include <ozo/request.h>
#include <ozo/execute.h>
#include <ozo/connection_info.h>
#include <ozo/query_builder.h>
#include <ozo/shortcuts.h>
#include <ozo/connection_pool.h>
#include <ozo/transaction.h>
#include <ozo/result.h>

#include "leaper/util/queue.hpp"
#include "leaper/util/logger.hpp"
#include "leaper/definitions.hpp"
#include "leaper/sql/sql_errors.hpp"
#include "leaper/sql/io.hpp"

using namespace ozo::literals;

typedef ozo::connection_pool<ozo::connection_info<>> connection_pool_t;
typedef ozo::connection_provider<ozo::connection_pool<ozo::connection_info<ozo::oid_map_t<>>> &> connection_t;

OZO_PG_BIND_TYPE(string_t, "text")

namespace leaper::sql {

    template<typename Connection>
    void handle_error(ozo::error_code ec, Connection &connection) {
        ERROR << "Request failed with error: " << ec.message() << std::endl;
        // Here we should check if the connection is in null state to avoid UB.
        if (!ozo::is_null_recursive(connection)) {
            // Let's check libpq native error message and if so - print it out
            if (auto msg = ozo::error_message(connection); !msg.empty()) {
                ERROR << ", error message: " << msg << std::endl;
            }
            // Sometimes libpq native error message is not enough, so let's check
            // the additional error context from OZO
            if (const auto &ctx = ozo::get_error_context(connection); !ctx.empty()) {
                ERROR << ", additional error context: " << ctx << std::endl;
            }
        }
    };

    template<typename Connection>
    void handle_error(ozo::error_code ec, Connection &connection, std::error_code &custom_ec) {
        custom_ec = SQLErrors::SQL_ERROR;
        handle_error(ec, connection);
    };

    template<class CALLBACK_T, typename CONNECTION_T, typename QUERY_T>
    void request(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection, QUERY_T &&statement,
                 ozo::result &result) {
        ozo::request(connection, statement, std::ref(result),
                     [&ec, callback](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                         }
                         callback();
                     });
    }

    template<class CALLBACK_C, typename CONNECTION_T, typename QUERY_T>
    void request(std::error_code &ec, void(CALLBACK_C::* callback_member)(), CALLBACK_C *callback_instance,
                 CONNECTION_T &connection, QUERY_T &&statement, ozo::result &result) {
        ozo::request(connection, statement, std::ref(result),
                     [&ec, callback_member, callback_instance](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                         }
                         (callback_instance->*callback_member)();
                     });
    }

    template<typename CONNECTION_T, typename QUERY_T>
    void
    execute_queued(std::error_code &ec, leaper::util::queue::Queue &queue, CONNECTION_T &connection, QUERY_T &&statement) {
        ozo::execute(connection, statement, [&queue, &ec](ozo::error_code ozo_ec, auto connection) {
            if (ozo_ec) {
                handle_error(ozo_ec, connection, ec);
            }
            queue.done();
        });
    };

    template<typename CONNECTION_T, typename QUERY_T, typename CALLBACK_T>
    void execute(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection, QUERY_T &&statement) {
        ozo::execute(connection, statement, [callback, &ec](ozo::error_code ozo_ec, auto connection) {
            if (ozo_ec) {
                handle_error(ozo_ec, connection, ec);
            }
            callback();
        });
    };

    template<class CALLBACK_CLASS_T, typename CONNECTION_T, typename QUERY_T>
    void execute(std::error_code &ec, void(CALLBACK_CLASS_T::* callback)(), CALLBACK_CLASS_T *obj,
                 CONNECTION_T &connection, QUERY_T &&statement) {
        ozo::execute(connection, statement, [obj, callback, &ec](ozo::error_code ozo_ec, auto connection) {
            if (ozo_ec) {
                handle_error(ozo_ec, connection, ec);
            }
            (obj->*callback)();
        });
    }

    template<typename CONNECTION_T, typename QUERY_T, class CALLBACK_CLASS_T>
    void exists(std::error_code &ec, void(CALLBACK_CLASS_T::* callback)(bool), CALLBACK_CLASS_T *obj,
                CONNECTION_T &connection, QUERY_T &&statement, ozo::result &buffer) {
        ozo::request(connection, statement, std::ref(buffer),
                     [obj, callback, &ec, &buffer](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                             (obj->*callback)(false);
                         } else {
                             assert(buffer.size() == 1); // Assert single row
                             assert(buffer[0].size() == 1); // Assert single column
                             bool result;
                             sql::io::read(ec, buffer[0][0], result);
                             (obj->*callback)(result);
                         }
                     });
    };

    template<typename CONNECTION_T, typename QUERY_T, class CALLBACK_T>
    void exists(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection, QUERY_T &&statement, ozo::result &buffer) {
        ozo::request(connection, statement, std::ref(buffer),
                     [callback, &ec, &buffer](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                             callback(false);
                         } else {
                             assert(buffer.size() == 1); // Assert single row
                             assert(buffer[0].size() == 1); // Assert single column
                             bool result;
                             sql::io::read(ec, buffer[0][0], result);
                             callback(result);
                         }
                     });
    };

    template<typename VALUE_T, typename CONNECTION_T, typename QUERY_T, class CALLBACK_CLASS_T>
    void
    fetch_value(std::error_code &ec, void(CALLBACK_CLASS_T::* callback)(std::optional<VALUE_T>), CALLBACK_CLASS_T *obj,
                CONNECTION_T &connection, QUERY_T &&statement, ozo::result &buffer) {
        ozo::request(connection, statement, std::ref(buffer),
                     [obj, callback, &ec, &buffer](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                             (obj->*callback)(std::nullopt);
                         } else {
                             assert(buffer.size() <= 1); // Assert single row
                             if (buffer.size() == 1) {
                                 assert(buffer[0].size() == 1); // Assert single column
                                 std::optional<VALUE_T> result;
                                 sql::io::read(ec, buffer[0][0], result);
                                 (obj->*callback)(result);
                             } else {
                                 (obj->*callback)(std::nullopt);
                             }
                         }
                     });
    };

    template<typename VALUE_T, typename CONNECTION_T, typename QUERY_T, class CALLBACK_T>
    void
    fetch_value(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection, QUERY_T &&statement, ozo::result &buffer) {
        ozo::request(connection, statement, std::ref(buffer),
                     [callback, &ec, &buffer](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                             callback(std::nullopt);
                         } else {
                             assert(buffer.size() <= 1); // Assert single row
                             if (buffer.size() == 1) {
                                 assert(buffer[0].size() == 1); // Assert single column
                                 std::optional<VALUE_T> result;
                                 sql::io::read(ec, buffer[0][0], result);
                                 callback(result);
                             } else {
                                 callback(std::nullopt);
                             }
                         }
                     });
    };

    template<typename CALLBACK_T, typename CONNECTION_T, typename QUERY_T, typename RESULT_T>
    void fetch_row(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &conn, QUERY_T &&statement, std::vector<RESULT_T> &buffer) {
        ozo::request(conn, statement, ozo::into(buffer), [&ec, callback, &buffer](ozo::error_code ozo_ec, auto &&conn) {
            if (ozo_ec) {
                sql::handle_error(ozo_ec, conn, ec);
                callback(std::nullopt);
            } else {
                assert(buffer.size() <= 1);
                if (buffer.size() == 1) {
                    callback(std::optional<RESULT_T>(buffer[0]));
                } else {
                    callback(std::nullopt);
                }
            }
        });
    }

    template<typename RESULT_T, typename CONNECTION_T, typename QUERY_T, class CALLBACK_CLASS_T>
    void fetch_rows(std::error_code &ec, void(CALLBACK_CLASS_T::* callback)(), CALLBACK_CLASS_T *obj,
                    CONNECTION_T &connection, QUERY_T &&statement, std::vector<RESULT_T> &result) {
        ozo::request(connection, statement, ozo::into(result),
                     [obj, callback, &ec](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                         }
                         (obj->*callback)();
                     });
    };

    template<typename RESULT_T, typename CONNECTION_T, typename QUERY_T, typename CALLBACK_T>
    void fetch_rows(std::error_code &ec, CALLBACK_T callback, CONNECTION_T &connection, QUERY_T &&statement,
                    std::vector<RESULT_T> &result) {
        ozo::request(connection, statement, ozo::into(result),
                     [callback, &ec](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                         }
                         callback();
                     });
    };

    template<typename RESULT_T, typename CONNECTION_T, typename QUERY_T>
    void
    fetch_rows_queued(std::error_code &ec, util::queue::Queue &queue, CONNECTION_T &connection, QUERY_T &&statement,
                      std::vector<RESULT_T> &result) {
        ozo::request(connection, statement, ozo::into(result),
                     [&queue, &ec](ozo::error_code ozo_ec, auto connection) {
                         if (ozo_ec) {
                             handle_error(ozo_ec, connection, ec);
                         }
                         queue.done();
                     });
    };

}; // namespace sql

#endif //LEAPER_LIBS_SQL_INCLUDE_SQL_CLIENT_HPP
