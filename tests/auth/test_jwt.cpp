//
// Created by nils on 12/05/2020.
//

#include <gtest/gtest.h>
#include <boost/json.hpp>
#include <thread>
#include "auth/jwt.hpp"

namespace json = boost::json;

TEST(JWT, Integration) {
    json::object claims = {{"role", "admin"}};
    string_t issuer = "test.com";
    string_t audience = "test2.com";
    unsigned int expire_in_s = 3600;

    string_t token;
    std::error_code ec;

    auth::JSONWebToken jwt_;
    jwt_.create_access_token(claims, issuer, audience, expire_in_s, token, ec);

    ASSERT_FALSE(ec);
    ASSERT_FALSE(token.empty());

    claims.clear();
    jwt_.verify_access_token(token, issuer, audience, claims, ec);

    ASSERT_FALSE(ec);
    auto iter = claims.find("role");
    ASSERT_NE(iter, claims.end());
    EXPECT_EQ(iter->value().as_string(), "admin");

    issuer = "wrong";
    jwt_.verify_access_token(token, issuer, audience, claims, ec);
    ASSERT_TRUE(ec);
    ec.clear();

    expire_in_s = 0;
    jwt_.create_access_token(claims, issuer, audience, expire_in_s, token, ec);
    std::this_thread::sleep_for(std::chrono::seconds(1));
    jwt_.verify_access_token(token, issuer, audience, claims, ec);
    ASSERT_TRUE(ec);
}

