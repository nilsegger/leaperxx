//
// Created by nils on 12/05/2020.
//

#include <gtest/gtest.h>
#include "auth/hasher.hpp"

TEST(Hasher, Integration) {
    string_t pwd = "This is my secret.";
    string_t hash;
    std::error_code ec;
    auth::Hasher hasher_;
    hasher_.hash_pwd(ec, hash, pwd.c_str(), pwd.size());
    ASSERT_FALSE(ec);
    bool authenticated = false;
    hasher_.verify_pwd(ec, authenticated, pwd.c_str(), pwd.size(), hash.c_str());
    ASSERT_FALSE(ec);
    ASSERT_TRUE(authenticated);
    string_t not_pwd = "This is not my secret.";
    hasher_.verify_pwd(ec, authenticated, not_pwd.c_str(), not_pwd.size(), hash.c_str());
    ASSERT_FALSE(ec);
    ASSERT_FALSE(authenticated);
}

