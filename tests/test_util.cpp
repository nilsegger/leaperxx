//
// Created by nils on 10/05/2020.
//

#include "util/queue.hpp"
#include "util/base64.hpp"
#include <gtest/gtest.h>

TEST(UtilQueue, SharedQueue) {
    util::queue::Queue queue(2);
    queue.done();
    ASSERT_FALSE(queue.is_finalized());
    queue.done();
    EXPECT_TRUE(queue.is_finalized());
}

TEST(Base64, Encode) {
    std::error_code ec;
    const char * input_raw = "Hello World";
    auto * input = (unsigned char *) input_raw;
    char * buffer;
    size_t buffer_length;
    util::base64::encode(ec, input, strlen(input_raw), &buffer, &buffer_length);
    buffer[buffer_length] = '\0';
    const char * expected = "SGVsbG8gV29ybGQ=";
    ASSERT_FALSE(ec);
    EXPECT_STREQ(buffer, expected);
}

TEST(Base64, Decode) {
    std::error_code ec;
    const char * input = "SGVsbG8gV29ybGQ=";
    unsigned char * buffer;
    size_t buffer_length;
    util::base64::decode(ec, input, &buffer, &buffer_length);
    auto * expected = (unsigned char *) "Hello World";
    size_t expected_length = 11;

    ASSERT_FALSE(ec);
    ASSERT_EQ(buffer_length, expected_length);
    for(size_t i = 0; i < expected_length; i++, buffer++, expected++) {
        EXPECT_EQ(*buffer, *expected);
    }
}

TEST(Base64, EncodeUrlSafe) {
    std::error_code ec;
    const char * input_raw = "Hello World";
    auto * input = (unsigned char *) input_raw;
    string_t encoded = util::base64::encode_url_safe(ec, input, strlen(input_raw));
    string_t expected = "SGVsbG8gV29ybGQ";
    ASSERT_FALSE(ec);
    EXPECT_EQ(encoded, expected);
}

TEST(Base64, DecodeUrlSafe) {
    std::error_code ec;
    const char * input = "SGVsbG8gV29ybGQ";
    string_t decoded = util::base64::decode_url_safe(ec, input, strlen(input));
    string_t expected("Hello World");
    ASSERT_FALSE(ec);
    EXPECT_EQ(decoded, expected);
}

TEST(Replace, Replace) {
    string_t input = "Hello World!";
    util::string::replace_all(input, 'l', "x");
    util::string::replace_all(input, '!', "?");
    string_t expected("Hexxo Worxd?");
    ASSERT_EQ(input, expected);
}
