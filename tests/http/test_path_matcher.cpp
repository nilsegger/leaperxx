//
// Created by nils on 04/06/2020.
//

#include <gtest/gtest.h>

#include "http/path_matcher.hpp"

TEST(PathMatcher, pathMatcher) {
    PathMatcher<int> matcher(0);

    matcher.add_path("/users", 1);
    matcher.add_path("/users/{uuid}", 2);
    matcher.add_path("/users/{uuid}/friends", 3);
    matcher.add_path("/users/{uuid}/friends/{friend}", 4);
    matcher.add_path("/users/top", 5);
    matcher.add_path("/users/top/{uuid}", 6);
    matcher.add_path("/{path}/{path1}/{path2}", 7);
    matcher.add_path("/parties/{uuid}", 8);

    Parameters pathParameters;
    EXPECT_EQ(matcher.match("/users", pathParameters), 1);

    EXPECT_EQ(matcher.match("/users/useruuid", pathParameters), 2);
    EXPECT_EQ(pathParameters.find("uuid"), "useruuid");
    pathParameters.clear();

    EXPECT_EQ(matcher.match("/users/useruuid/friends", pathParameters), 3);
    EXPECT_EQ(pathParameters.find("uuid"), "useruuid");
    pathParameters.clear();

    EXPECT_EQ(matcher.match("/users/useruuid/friends/frienduuid", pathParameters), 4);
    EXPECT_EQ(pathParameters.find("uuid"), "useruuid");
    EXPECT_EQ(pathParameters.find("friend"), "frienduuid");
    pathParameters.clear();

    EXPECT_EQ(matcher.match("/users/top", pathParameters), 5);

    EXPECT_EQ(matcher.match("/users/top/topuuid", pathParameters), 6);
    EXPECT_EQ(pathParameters.find("uuid"), "topuuid");
    pathParameters.clear();

    EXPECT_EQ(matcher.match("/step/step/step", pathParameters), 7);
    EXPECT_EQ(pathParameters.find("path"), "step");
    EXPECT_EQ(pathParameters.find("path1"), "step");
    EXPECT_EQ(pathParameters.find("path2"), "step");
    pathParameters.clear();

    EXPECT_EQ(matcher.match("/parties/partyuuid", pathParameters), 8);
    EXPECT_EQ(pathParameters.find("uuid"), "partyuuid");
    pathParameters.clear();

    EXPECT_EQ(matcher.match("/wp_admin.php", pathParameters), 0);
    EXPECT_EQ(matcher.match("/parties", pathParameters), 0);
    EXPECT_EQ(matcher.match("/nothing", pathParameters), 0);
    EXPECT_EQ(matcher.match("/parties/partyuuid/guests/top", pathParameters), 0);
    EXPECT_EQ(matcher.match("/users/useruuid/friends/frienduuid/profile", pathParameters), 0);
}
