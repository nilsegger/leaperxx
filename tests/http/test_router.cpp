//
// Created by nils on 04/06/2020.
//

#include <gtest/gtest.h>

#include "http/router.hpp"

struct MockCallback {

};

class MockRouter : public Router<MockCallback> {

public:

    std::pair<string_t, Parameters> test(string_t url) {
        return this->parse_url(url);
    }

};

TEST(Router, parse_url) {
    MockRouter router;

    auto result = router.test("/user/uuid?test=hello&test2=world");

    EXPECT_EQ(result.first, "/user/uuid");

    EXPECT_EQ(result.second.find("test"), "hello");
    EXPECT_EQ(result.second.find("test2"), "world");
}

