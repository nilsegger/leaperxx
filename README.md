# Leaper++

This repository contains the complete code for the Leaper server. This includes the auth, config, definitions, errors, http, mail, sql, util modules and the bussiness logic code.

## Dependencies

- [Argon2](https://github.com/p-h-c/phc-winner-argon2)
- [boost](https://www.boost.org/users/download/)
- [ozo](https://github.com/yandex/ozo)
- [RSA](https://gist.github.com/irbull/08339ddcd5686f509e9826964b17bb59)
- [JSON](https://github.com/CPPAlliance/json)
- [PostgreSQL]()

## Installation

- git clone --recurse-submodules repo
- Build boost (./bootstrap.sh && ./b2 && ./b2 headers)
- Build argon2
- Create configuration file
- mkdir build && cd build
- cmake -DBOOST_ROOT=/path/third_party/boost ..
- cmake --build . -j8
- ./leaper++ <path to configuration file>

### Configuration
Name	| Description
------- | -----------
projectPath | Path to folder of project, this is used for the debug logger. 
address | Listening ip address of server
port | Listening port of server
threads | Number of threads to be used
self | URL of the server
fqdn | FQDN of server
rsaKeys | Path to directory containing a minimum of one pair of rsa keys to be used by the authentication module for access keys
sslCertificate | Path to .crt file
sslPassword | Password used for creating the certificate.
sslKey | Path to .key file
sslDh | Path to .pem file
audience | Audience for the access key
issuer | Issuer of the access keys
imagePath | Path to directory in which images and files will be stored
mailServer | SMTP Url to the mail server. (stmp://smtp.gmail.com:587)
mailUsername | Mail login
mailPassword | Mail password
mailSenderEmail | Alias to be used for sending mails
mailSenderName | Name of sender
mailSSL | Path to SSL file to be used for encryption of data

### Format

Keys are separated by space to their value followed by a newline.

### SSL Keys for https

To generate a SSL certificate for https connections use the following commands:

```
	openssl dhparam -out dh.pem 2048
	openssl req -newkey rsa:2048 -keyout key.pem -x509 -days 1000 -out cert.crt
```

Make sure to add an exception for localhost self signed certificate and import the cert.crt file into the browser if it is not done automatically upon adding the exception.

### RSA Keys for access tokens

Keep in mind that the current version of the server only accepts a single pair of keys, which must be named key.private.pem and key.public.pem.
Furthermore, the key must have a length of 4906.

- openssl genrsa -out key.private.pem 4096
- openssl rsa -in key.private.pem -out key.public.pem -pubout -outform PEM


### PostgreSQL setup User password authentication

- First locate the pg_hba.conf file with psql. Use the command `show hba_file`;
- Open the pg_hba.conf file and add md5 authentication for local Unix socket connections
- Change postgres user password. psql -> \password
- Restart the postgresql service with `sudo systemctl restart postgresql`

### Code Checklist 
#### Resources
- [ ] Make sure that atleast `Resources::prepare_queue` has been called before calling async functions of different classes.

#### Controllers
- [ ] Receive only pointers to models, not std::unique_ptr.

## Tests

TODO
